<?php

/**

 * The base configuration for WordPress

 *

 * The wp-config.php creation script uses this file during the

 * installation. You don't have to use the web site, you can

 * copy this file to "wp-config.php" and fill in the values.

 *

 * This file contains the following configurations:

 *

 * * MySQL settings

 * * Secret keys

 * * Database table prefix

 * * ABSPATH

 *

 * @link https://codex.wordpress.org/Editing_wp-config.php

 *

 * @package WordPress

 */


// ** MySQL settings - You can get this info from your web host ** //

/** The name of the database for WordPress */

define( 'DB_NAME', "netrootstech" );


/** MySQL database username */

define( 'DB_USER', "root" );


/** MySQL database password */

define( 'DB_PASSWORD', "" );


/** MySQL hostname */

define( 'DB_HOST', "localhost" );


/** Database Charset to use in creating database tables. */

define( 'DB_CHARSET', 'utf8mb4' );


/** The Database Collate type. Don't change this if in doubt. */

define( 'DB_COLLATE', '' );


/**#@+

 * Authentication Unique Keys and Salts.

 *

 * Change these to different unique phrases!

 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}

 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.

 *

 * @since 2.6.0

 */

define( 'AUTH_KEY',         ' T%MP/xSk<Q`r1UNY4.}%?5uV;vwI&L,-{/9Sdeak]:x/l6*1M;n#!uV$%YCRAsx' );

define( 'SECURE_AUTH_KEY',  'd+scp}N_Jj4@ hJFoJ^Jtd[L*aZM0Mhv@]JGJ;feCD2-2NW-}0]gKrzf?IXHp9P,' );

define( 'LOGGED_IN_KEY',    'Bpz)rg$IMT:tQ&sT|$GEj+Vfykvp_=:VBhAJ`aB#?0q?Klf@*!u4 pq~TBZ-s}xN' );

define( 'NONCE_KEY',        '5Qo:IdWX`Tpwb-RdnkQf6%[K{m/ h6hZ8}3e6Vkl4vPRE|@*AFjcpHbaCfm_1J^L' );

define( 'AUTH_SALT',        'gx`kvhlhC@z_+~6On4}~ ;q8X|z~@dKULn/=~!lsu]pu[6dRsk`O#Mv2AFneK!,x' );

define( 'SECURE_AUTH_SALT', '!5`!|pmAT=G7rUKWxqacmoQss`l^=lh!.ZGdAy9jy,_z^H+@WrOE03paLCxL^sFR' );

define( 'LOGGED_IN_SALT',   '/|k,LP+F?d5aB8$3{o^2:y{uD-0>,`Z<ez7gTCs-.[}G#|`js?mZPN&/_+.zY<]k' );

define( 'NONCE_SALT',       'dhWwAOUK,08(P>oYGG1VMe+|n+1&x[WKU4X&;6E<s01)g)6^INclhlIJK B`mcrF' );


/**#@-*/


/**

 * WordPress Database Table prefix.

 *

 * You can have multiple installations in one database if you give each

 * a unique prefix. Only numbers, letters, and underscores please!

 */

$table_prefix = 'NRT_';


/**

 * For developers: WordPress debugging mode.

 *

 * Change this to true to enable the display of notices during development.

 * It is strongly recommended that plugin and theme developers use WP_DEBUG

 * in their development environments.

 *

 * For information on other constants that can be used for debugging,

 * visit the Codex.

 *

 * @link https://codex.wordpress.org/Debugging_in_WordPress

 */

define( 'WP_DEBUG', false );


/* That's all, stop editing! Happy publishing. */


/** Absolute path to the WordPress directory. */

if ( ! defined( 'ABSPATH' ) ) {

	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

}


/** Sets up WordPress vars and included files. */

require_once( ABSPATH . 'wp-settings.php' );

