<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>


<!------------------

          Fotter Area

          ------------------>
<?php
require 'inc/config.php'; ?>

<div class="fotter_con wf">
    <div class="footer_1">


        <div class="fotter_seprator_line wf">

            <!--fotter_seprator_line-->

        </div>


        <div class="container no_paddind"
             style="border-bottom: 2px solid #c3c0c0; margin-bottom: 15px; padding-bottom: 15px;">


            <div class="fotter_content wf">

                <style>
                    .office_numder a {
                        color: black !important;
                    }

                    .office_email a {
                        color: black !important;
                    }

                    .office_numder2 a {
                        color: black !important;
                    }
                </style>

                <div class="col-lg-4">

                    <div class="footer_links">

                        <h5 class="medium_font__Montserrat color-black sub-heading"
                            style="margin-top: 41px; font-weight: bold;"><?php echo $sticky_number['IL']['officename'] ?></h5>
                        <ul>
                            <li class="office_location" style="margin-top: 5px; color: black;"><span><i
                                            class="fas fa-map-marker-alt"></i></span><?php echo $sticky_number['IL']['address'] ?>
                            </li>
                            <li class="office_numder"><span><i class="fas fa-phone-square"></i></span><a
                                        href="tel:<?php echo $sticky_number ['IL']['dialnumber']; ?>"
                                        target="_blank"><?php echo $sticky_number ['IL']['number']; ?></a></li>
                            <li class="office_email"><span><i class="far fa-envelope"></i></span><a
                                        href="mailto:<?php echo $sticky_number ['IL']['email']; ?>"><?php echo $sticky_number ['IL']['email']; ?></a>
                            </li>

                        </ul>


                    </div>


                </div>


            </div>

            <div class="fotter_content col-lg-8">

                <div class="fotter_content_heading">


                    <div class="footer_office_detail wf">

                        <ul class="color-black regular_font__Montserrat" style="color:#000;">

                            <li class="regular_font__Montserrat sub-heading color-black"
                                style=" font-weight: bold;"><?php echo $sticky_number['USA']['officename'] ?></li>
                            <li class="office_location" style="margin-top: 5px;"><span><i
                                            class="fas fa-map-marker-alt"></i></span><?php echo $sticky_number['USA']['address'] ?>
                            </li>
                            <li class="office_numder"><span><i class="fas fa-phone-square"></i></span><a
                                        href="tel:<?php echo $sticky_number ['USA']['dialnumber']; ?>"
                                        target="_blank"><?php echo $sticky_number ['USA']['number']; ?></a></li>
                            <li class="office_numder2"><span><i class="fas fa-phone-square"></i></span><a
                                        href="tel:<?php echo $sticky_number ['USA']['dialnummber2']; ?>"
                                        target="_blank"><?php echo $sticky_number ['USA']['number2']; ?></a></li>
                            <li class="office_email"><span><i class="far fa-envelope"></i></span><a
                                        href="mailto:<?php echo $sticky_number ['IL']['email']; ?>"><?php echo $sticky_number ['IL']['email']; ?></a>
                            </li>

                        </ul>


                        <ul class="canada-office color-black regular_font__Montserrat">

                            <li class="regular_font__Montserrat sub-heading color-black"
                                style=" font-weight: bold;"><?php echo $sticky_number['CA']['officename'] ?></li>

                            <li class="office_location" style="margin-top: 5px;"><span><i
                                            class="fas fa-map-marker-alt"></i></span><?php echo $sticky_number['CA']['address'] ?>

                            </li>

                            <li class="office_numder"><span><i class="fas fa-phone-square"></i></span> <a
                                        href="tel:<?php echo $sticky_number ['CA']['dialnumber']; ?>"
                                        target="_blank"><?php echo $sticky_number ['CA']['number']; ?></a></li>

                            <li class="office_email"><span><i class="far fa-envelope"></i></span><a
                                        href="mailto:<?php echo $sticky_number ['IL']['email']; ?>"><?php echo $sticky_number ['IL']['email']; ?></a>
                            </li>

                        </ul>


                        <!--footer_office_detail wf-->

                    </div>

                    <!--fotter_content_heading-->

                </div>

                <!--col-lg-7-->

            </div>

            <!--fotter_content-->


            <!--container-->

        </div>


        <div class="footer_logo_box">

            <div class="container">

                <div class="row">


                    <div class="col-lg-4 no_margin">
                        <div class="footer_logo_img">


                            <img class="img-responsive" style=""
                                 src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-2.png" alt="LOGO"/>


                        </div>
                    </div>

                    <div class="col-lg-8">

                        <div class="footer_logo_text" style="text-align: justify;"><p>NetRoots Technologies is a modern
                                Information Technology services and consultancy firm, designed especially to help
                                businesses succeed in the increasingly digital times. From application development to
                                knowledge and business process management, we address your IT needs with care and
                                precision.</p></div>
                    </div>

                </div>


            </div>


        </div>
    </div>
</div>
<!--fotter_con-->
<div class="fotter_copyright_sec_con wf">

    <div class="no_paddind container">

        <div class="fotter_copyright_sec_inner">

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">

                <small class=" color-white regular_font__Montserrat paragraph-text">© copyright 2019 <a
                            class="color-white regular_font__Montserrat paragraph-text" href="#">netrootstech</a>. All
                    rights reserved.
                </small>

                <!--col-lg-6-->

            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">

                <small class=" color-white regular_font__Montserrat paragraph-text"><a
                            class="color-white regular_font__Montserrat paragraph-text" 
						 href="<?php echo site_url();?>"
                            style="margin-right: 10px; color: white;">Home</a>
                    <a class="color-white regular_font__Montserrat paragraph-text" href="<?php echo get_page_link(44) ?>"
                       style="margin-right: 10px; color: white;">About us</a>

                    <a class="color-white regular_font__Montserrat paragraph-text" href="<?php echo get_page_link(47);?>" style="margin-right: 10px; color: white;">Contact</a>
                </small>

                <!--col-lg-6-->

            </div>

            <style>


                .return-to-top:hover i {
                    color: #fff;
                    top: 5px;
                }
            </style>
            <div class="social col-lg-4 col-md-4 col-sm-4 col-xs-12 ">

                <ul class="social-icons">


                    <li class="fb"><a href="https://www.facebook.com/netrootstech/"><img
                                    src="<?php echo get_template_directory_uri(); ?>/assets/images/sm_icon_1.png" alt="FaceBook"/></a></li>

                    <li class="twiter"><a href="https://twitter.com/Netrootstech7"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/sm_icon_2.png" alt="twiter"/></a></li>

                    <li class="pintress"><a href="https://www.pinterest.com/netrootsfb/netroots-technologies/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/sm_icon_3.png" alt="pintress"/></a></a></li>

                    <li class="linked-in"><a href="https://www.linkedin.com/company/13277143/admin/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/sm_icon_4.png" alt="linked-in"/></a></li>

                    <li class="linked-in" style=" background: #d6249f;
  background: radial-gradient(circle at 30% 107%, #fdf497 0%, #fdf497 5%, #fd5949 45%,#d6249f 60%,#285AEB 90%);"><a
                                href="https://www.instagram.com/netrootstech7/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/insta.png"
                            alt="linked-in" width="21"
                            height="21"/></a></li>

                </ul>

                <!--col-lg-6-->

            </div>


            <!--fotter_copyright_sec_inner-->

        </div>

        <!--no_paddind container-->

    </div>

    <!--fotter_copyright_sec_con-->

</div>

<div class="sticky-container" >

    <?php
        require 'inc/config.php';
        require 'inc/sticky.php';
 ?>

</div>
<!--ip-container-->
</div>

<!--Talk-US Widget Call-->

<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5cc2d770ee912b07bec4f7fa/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->


<!--WhatsApp Widget Call-->
<script async data-id="9498" src="https://cdn.widgetwhats.com/script.min.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-137707608-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-137707608-1');
</script>

<?php wp_footer(); ?>

</body>
</html>
