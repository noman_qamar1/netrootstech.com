jQuery(document).ready(function (jQuery) {
    function morphDropdown(element) {
        this.element = element;
        this.mainNavigation = this.element.find('.main-nav');
        this.mainNavigationItems = this.mainNavigation.find('.has-dropdown');
        this.dropdownList = this.element.find('.dropdown-list');
        this.dropdownWrappers = this.dropdownList.find('.dropdown');
        this.dropdownItems = this.dropdownList.find('.content');
        this.dropdownBg = this.dropdownList.find('.bg-layer');
        this.mq = this.checkMq();
        this.bindEvents()
    }

    morphDropdown.prototype.checkMq = function () {
        var self = this;
        return window.getComputedStyle(self.element.get(0), '::before').getPropertyValue('content').replace(/'/g, "").replace(/"/g, "").split(', ')
    };
    morphDropdown.prototype.bindEvents = function () {
        var self = this;
        this.mainNavigationItems.mouseenter(function (event) {
            self.showDropdown(jQuery(this))
        }).mouseleave(function () {
            setTimeout(function () {
                if (self.mainNavigation.find('.has-dropdown:hover').length == 0 && self.element.find('.dropdown-list:hover').length == 0) self.hideDropdown()
            }, 50)
        });
        this.dropdownList.mouseleave(function () {
            setTimeout(function () {
                (self.mainNavigation.find('.has-dropdown:hover').length == 0 && self.element.find('.dropdown-list:hover').length == 0) && self.hideDropdown()
            }, 50)
        });
        this.mainNavigationItems.on('touchstart', function (event) {
            var selectedDropdown = self.dropdownList.find('#' + jQuery(this).data('content'));
            if (!self.element.hasClass('is-dropdown-visible') || !selectedDropdown.hasClass('active')) {
                event.preventDefault();
                self.showDropdown(jQuery(this))
            }
        });
        this.element.on('click', '.nav-trigger', function (event) {
            event.preventDefault();
            self.element.toggleClass('nav-open')
        })
    };
    morphDropdown.prototype.showDropdown = function (item) {
        this.mq = this.checkMq();
        if (this.mq == 'desktop') {
            var self = this;
            var selectedDropdown = this.dropdownList.find('#' + item.data('content')),
                selectedDropdownHeight = selectedDropdown.innerHeight(),
                selectedDropdownWidth = selectedDropdown.children('.content').innerWidth(),
                selectedDropdownLeft = item.offset().left + item.innerWidth() / 2 - selectedDropdownWidth / 2;
            this.updateDropdown(selectedDropdown, parseInt(selectedDropdownHeight), selectedDropdownWidth, parseInt(selectedDropdownLeft));
            this.element.find('.active').removeClass('active');
            selectedDropdown.addClass('active').removeClass('move-left move-right').prevAll().addClass('move-left').end().nextAll().addClass('move-right');
            item.addClass('active');
            if (!this.element.hasClass('is-dropdown-visible')) {
                setTimeout(function () {
                    self.element.addClass('is-dropdown-visible')
                }, 10)
            }
        }
    };
    morphDropdown.prototype.updateDropdown = function (dropdownItem, height, width, left) {
        this.dropdownList.css({
            '-moz-transform': 'translateX(' + left + 'px)',
            '-webkit-transform': 'translateX(' + left + 'px)',
            '-ms-transform': 'translateX(' + left + 'px)',
            '-o-transform': 'translateX(' + left + 'px)',
            'transform': 'translateX(' + left + 'px)',
            'width': width + 'px',
            'height': height + 'px'
        });
        this.dropdownBg.css({
            '-moz-transform': 'scaleX(' + width + ') scaleY(' + height + ')',
            '-webkit-transform': 'scaleX(' + width + ') scaleY(' + height + ')',
            '-ms-transform': 'scaleX(' + width + ') scaleY(' + height + ')',
            '-o-transform': 'scaleX(' + width + ') scaleY(' + height + ')',
            'transform': 'scaleX(' + width + ') scaleY(' + height + ')'
        })
    };
    morphDropdown.prototype.hideDropdown = function () {
        this.mq = this.checkMq();
        if (this.mq == 'desktop') {
            this.element.removeClass('is-dropdown-visible').find('.active').removeClass('active').end().find('.move-left').removeClass('move-left').end().find('.move-right').removeClass('move-right')
        }
    };
    morphDropdown.prototype.resetDropdown = function () {
        this.mq = this.checkMq();
        if (this.mq == 'mobile') {
            this.dropdownList.removeAttr('style')
        }
    };
    var morphDropdowns = [];
    if (jQuery('.cd-morph-dropdown').length > 0) {
        jQuery('.cd-morph-dropdown').each(function () {
            morphDropdowns.push(new morphDropdown(jQuery(this)))
        });
        var resizing = !1;
        updateDropdownPosition();
        jQuery(window).on('resize', function () {
            if (!resizing) {
                resizing = !0;
                (!window.requestAnimationFrame) ? setTimeout(updateDropdownPosition, 300) : window.requestAnimationFrame(updateDropdownPosition)
            }
        });
        function updateDropdownPosition() {
            morphDropdowns.forEach(function (element) {
                element.resetDropdown()
            });
            resizing = !1
        }
    }
    (function (jQuery) {
        'use strict';
        var owl = jQuery('.owl-carousel');
        var owlLoader = jQuery('.loader-bar');
        var owlDuration = 3200;
        var loaderDuration = owlDuration / 1000 + 's';
        var owlSlides = owl.children('div');
        var owlAutoPlay = owlSlides.length > 1;
        var resetLoaderStyles = {'width': '0', 'transition-duration': '0s'};
        var loaderStyles = {'width': '100%', 'transition-duration': loaderDuration};
        jQuery(document).ready(function () {
            if (owlAutoPlay == !0) {
                owl.owlCarousel({
                    loop: !0,
                    autoplay: owlAutoPlay,
                    autoplaytimeout: owlDuration,
                    autoplayHoverPause: !0,
                    margin: 0,
                    responsiveClass: !0,
                    navText: ["<div id='myCarouselControl'><i class='fas fa-arrow-left'></i></div>",
                        "<div id='myCarouselControl'><i class='fas fa-arrow-right'></i></div>"],
                    responsive: {0: {items: 1, nav: !0}}
                })
            } else {
                owl.show()
            }
            window.requestAnimationFrame(function () {
                owlLoader.css(loaderStyles)
            });
            owl.on('changed.owl.carousel', function () {
                owlLoader.css(resetLoaderStyles);
                window.requestAnimationFrame(function () {
                    owlLoader.css(loaderStyles)
                })
            })
        })
    })(jQuery);


    var MqL = 1170;
    moveNavigation();
    jQuery(window).on('resize', function () {
        (!window.requestAnimationFrame) ? setTimeout(moveNavigation, 300) : window.requestAnimationFrame(moveNavigation)
    });
    jQuery('.cd-nav-trigger').on('click', function (event) {
        event.preventDefault();
        if (jQuery('.cd-main-content').hasClass('nav-is-visible')) {
            closeNav();
            jQuery('.cd-overlay').removeClass('is-visible')
        } else {
            jQuery(this).addClass('nav-is-visible');
            jQuery('.cd-primary-nav').addClass('nav-is-visible');
            jQuery('.cd-main-header').addClass('nav-is-visible');
            jQuery('.cd-main-content').addClass('nav-is-visible').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function () {
                jQuery('body').addClass('overflow-hidden')
            });
            toggleSearch('close');
            jQuery('.cd-overlay').addClass('is-visible')
        }
    });
    jQuery('.cd-search-trigger').on('click', function (event) {
        event.preventDefault();
        toggleSearch();
        closeNav()
    });
    jQuery('.cd-overlay').on('swiperight', function () {
        if (jQuery('.cd-primary-nav').hasClass('nav-is-visible')) {
            closeNav();
            jQuery('.cd-overlay').removeClass('is-visible')
        }
    });
    jQuery('.nav-on-left .cd-overlay').on('swipeleft', function () {
        if (jQuery('.cd-primary-nav').hasClass('nav-is-visible')) {
            closeNav();
            jQuery('.cd-overlay').removeClass('is-visible')
        }
    });
    jQuery('.cd-overlay').on('click', function () {
        closeNav();
        toggleSearch('close')
        jQuery('.cd-overlay').removeClass('is-visible')
    });
    jQuery('.cd-primary-nav').children('.has-children').children('a').on('click', function (event) {
        event.preventDefault()
    });
    jQuery('.has-children').children('a').on('click', function (event) {
        if (!checkWindowWidth()) event.preventDefault();
        var selected = jQuery(this);
        if (selected.next('ul').hasClass('is-hidden')) {
            selected.addClass('selected').next('ul').removeClass('is-hidden').end().parent('.has-children').parent('ul').addClass('moves-out');
            selected.parent('.has-children').siblings('.has-children').children('ul').addClass('is-hidden').end().children('a').removeClass('selected');
            jQuery('.cd-overlay').addClass('is-visible')
        } else {
            selected.removeClass('selected').next('ul').addClass('is-hidden').end().parent('.has-children').parent('ul').removeClass('moves-out');
            jQuery('.cd-overlay').removeClass('is-visible')
        }
        toggleSearch('close')
    });
    var windowsize = jQuery(window).width();

    function hover_dropdown() {
        windowsize = jQuery(window).width();
        if (windowsize > 900) {
            jQuery('.has-children').children('a').mouseenter(function (event) {
                if (!checkWindowWidth()) event.preventDefault();
                var selected = jQuery(this);
                if (selected.next('ul').hasClass('is-hidden')) {
                    selected.addClass('selected').next('ul').removeClass('is-hidden').end().parent('.has-children').parent('ul').addClass('moves-out');
                    selected.parent('.has-children').siblings('.has-children').children('ul').addClass('is-hidden').end().children('a').removeClass('selected');
                    jQuery('.cd-overlay').addClass('is-visible')
                } else {
                    selected.removeClass('selected').next('ul').addClass('is-hidden').end().parent('.has-children').parent('ul').removeClass('moves-out');
                    jQuery('.cd-overlay').removeClass('is-visible')
                }
                toggleSearch('close')
            });
            jQuery('li:not(.has-children)').children('a').mouseenter(function (event) {
                var selected = jQuery('.has-children a.selected');
                selected.removeClass('selected').next('ul').addClass('is-hidden').end().parent('.has-children').parent('ul').removeClass('moves-out');
                jQuery('.cd-overlay').removeClass('is-visible')
            });
            jQuery('.has-children .cd-secondary-nav, .has-children .cd-nav-icons').mouseleave(function (event) {
                var selected = jQuery('.has-children a.selected');
                selected.removeClass('selected').next('ul').addClass('is-hidden').end().parent('.has-children').parent('ul').removeClass('moves-out');
                jQuery('.cd-overlay').removeClass('is-visible')
            })
        }
    }

    hover_dropdown();
    jQuery(window).resize(function () {
        hover_dropdown()
    });
    jQuery('.go-back').on('click', function () {
        jQuery(this).parent('ul').addClass('is-hidden').parent('.has-children').parent('ul').removeClass('moves-out')
    });
    function closeNav() {
        jQuery('.cd-nav-trigger').removeClass('nav-is-visible');
        jQuery('.cd-main-header').removeClass('nav-is-visible');
        jQuery('.cd-primary-nav').removeClass('nav-is-visible');
        jQuery('.has-children ul').addClass('is-hidden');
        jQuery('.has-children a').removeClass('selected');
        jQuery('.moves-out').removeClass('moves-out');
        jQuery('.cd-main-content').removeClass('nav-is-visible').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function () {
            jQuery('body').removeClass('overflow-hidden')
        })
    }

    function toggleSearch(type) {
        if (type == "close") {
            jQuery('.cd-search').removeClass('is-visible');
            jQuery('.cd-search-trigger').removeClass('search-is-visible');
            jQuery('.cd-overlay').removeClass('search-is-visible')
        } else {
            jQuery('.cd-search').toggleClass('is-visible');
            jQuery('.cd-search-trigger').toggleClass('search-is-visible');
            jQuery('.cd-overlay').toggleClass('search-is-visible');
            if (jQuery(window).width() > MqL && jQuery('.cd-search').hasClass('is-visible')) jQuery('.cd-search').find('input[type="search"]').focus();
            (jQuery('.cd-search').hasClass('is-visible')) ? jQuery('.cd-overlay').addClass('is-visible') : jQuery('.cd-overlay').removeClass('is-visible')
        }
    }

    function checkWindowWidth() {
        var e = window, a = 'inner';
        if (!('innerWidth' in window)) {
            a = 'client';
            e = document.documentElement || document.body
        }
        if (e[a + 'Width'] >= MqL) {
            return !0
        } else {
            return !1
        }
    }

    function moveNavigation() {
        var navigation = jQuery('.cd-nav');
        var desktop = checkWindowWidth();
        if (desktop) {
            navigation.detach();
            navigation.insertBefore('.cd-header-buttons')
        } else {
            navigation.detach();
            navigation.insertAfter('.cd-main-content')
        }
    }
});
jQuery(document).ready(function (jQuery) {
    var path = window.location.pathname.split("/").pop();
    var actualpath = path.split("/").pop();
    var target = jQuery('.right-side a[href="' + actualpath + '"]');
    target.addClass('service_active')
});
jQuery(document).ready(function () {
    jQuery('#cd-primary-nav li.has-children ul.cd-secondary-nav li.has-children a').click(function (event) {
        event.preventDefault();
        var link = jQuery(this).attr('href');
        window.location.href = link;
        return !1
    });


})

jQuery(document).ready(function () {

    var mockupSlider = jQuery("#mockup-slider");

    var mockupSliderTitles = jQuery("#mockup-slider-titles");



    mockupSlider.owlCarousel({

        items:1,

    })

    mockupSliderTitles.owlCarousel({

        items:1,

    })



    jQuery(".next").click(function(){

        mockupSlider.trigger('next.owl.carousel');

        mockupSliderTitles.trigger('next.owl.carousel');

    });



    jQuery(".prev").click(function(){

        mockupSlider.trigger('prev.owl.carousel');

        mockupSliderTitles.trigger('prev.owl.carousel');

    });


    jQuery('.owl-carousel34').owlCarousel({

        loop: true,

        margin: 10,

        responsiveClass: true,

        responsive: {

            0: {

                items: 1,

                nav: true

            },

            600: {

                items: 3,

                nav: false

            },

            1000: {

                items: 2,

                nav: true,

                loop: false,

                margin: 20

            }

        }

    });

    jQuery('.owl-carousel-logo-slider').owlCarousel({

        loop: true,

        margin: 10,

        responsiveClass: true,

        margin:10,

        autoplay:true,

        autoplayTimeout:2000,

        autoplayHoverPause:true,

        responsive: {

            0: {

                items: 1,

                nav: true

            },

            600: {

                items: 3,

                nav: false

            },

            1000: {

                items: 5,

                nav: true,

                loop: false,

                margin: 20

            }

        }

    });









    // tabbed content

    // http://www.entheosweb.com/tutorials/css/tabs.asp

    jQuery(".tab_content").hide();


    jQuery(".tab_content:first").show();



    /* if in tab mode */

    jQuery("ul.tabs li").click(function() {



        jQuery(".tab_content").hide();

        var activeTab = jQuery(this).attr("rel");

        jQuery("#"+activeTab).fadeIn();



        jQuery("ul.tabs li").removeClass("active");

        jQuery(this).addClass("active");



        jQuery(".tab_drawer_heading").removeClass("d_active");

        jQuery(".tab_drawer_heading[rel^='"+activeTab+"']").addClass("d_active");



    });

    /* if in drawer mode */

    jQuery(".tab_drawer_heading").click(function() {



        jQuery(".tab_content").hide();

        var d_activeTab = jQuery(this).attr("rel");

        jQuery("#"+d_activeTab).fadeIn();



        jQuery(".tab_drawer_heading").removeClass("d_active");

        jQuery(this).addClass("d_active");



        jQuery("ul.tabs li").removeClass("active");

        jQuery("ul.tabs li[rel^='"+d_activeTab+"']").addClass("active");

    });





    /* Extra class "tab_last"

     to add border to right side

     of last tab */

    jQuery('ul.tabs li').last().addClass("tab_last");


})


