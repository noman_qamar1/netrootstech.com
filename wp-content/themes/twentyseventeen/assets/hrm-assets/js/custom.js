/* ---------------------
	CUSTOM JS DOCUMENT 
--------------------- */
var appMaster = {
	/* ---------------------
		Page Loader
	--------------------- */
	pageLoader: function(){
		jQuery(".loader-item").delay(700).fadeOut();
		jQuery("#pageloader").delay(800).fadeOut("slow");
	},
	/* ---------------------
		Navigation Menu
	--------------------- */
	navBar: function(){
		/* ---------------------
			Sticky
		--------------------- */
		 if (jQuery('#sticker').length) {
			jQuery("#sticker").sticky({topSpacing:0});
		 }
		/* --------------------------
		Home Background Super Slider
		-------------------------- */
		 if (jQuery('#slides').length) {
			jQuery('#slides').superslides({
			});
		 }

	},

	/* --------------------------
	HeaderSearch, Phone & Social Icons Toggle
	-------------------------- */
	toggleNav: function(){

		/* header Contact (Phone) */
	   jQuery( ".header-contact" ).click(function() {
		jQuery( ".header-contact-content" ).show( "fast", function() {});
		jQuery('.transparent-header .navbar').fadeIn().addClass('top-search-open');
		jQuery(".close").click(function() {
		  jQuery(".header-contact-content").hide("fast", function() {});
		  jQuery('.transparent-header .navbar').fadeIn().removeClass('top-search-open');
		})
	   });


	   /* header Search (Search Form) */
	   jQuery( ".header-search" ).click(function() {
		jQuery( ".header-search-content" ).show( "fast", function() {});
		jQuery('.transparent-header .navbar').fadeIn().addClass('top-search-open');

		jQuery(".close").click(function() {
		  jQuery(".header-search-content").hide("fast", function() {});
		  jQuery('.transparent-header .navbar').fadeIn().removeClass('top-search-open');
		})
	   });


		/* header Share (Search Form) */
	   jQuery( ".header-share" ).click(function() {
		jQuery( ".header-share-content" ).show( "fast", function() {});
		jQuery('.transparent-header .navbar').fadeIn().addClass('top-search-open');

		jQuery(".close").click(function() {
		  jQuery(".header-share-content").hide("fast", function() {});
		  jQuery('.transparent-header .navbar').fadeIn().removeClass('top-search-open');
		})
	   });
	},
	/* ---------------------
		Owl Slider
	/* --------------------- */
	owlCarousel: function(){
		(function(jQuery) {
			"use strict";
			if (jQuery('.owl-carousel').length) {
				  jQuery(".owl-carousel").each(function (index) {
					var effect_mode = jQuery(this).data('effect');
					var autoplay = jQuery(this).data('autoplay');
					var navigation = jQuery(this).data('navigation');
					var pagination = jQuery(this).data('pagination');
					var singleitem = jQuery(this).data('singleitem');
					var items = jQuery(this).data('items');
					var itemsdesktop = jQuery(this).data('desktop');
					var itemsdesktopsmall = jQuery(this).data('desktopsmall');
					var itemstablet = jQuery(this).data('tablet');
					var itemsmobile = jQuery(this).data('mobile');
					if( itemsdesktop > 0 )
					{
						itemsdesktop = [1199, itemsdesktop];
					}
					if( itemsdesktopsmall > 0 )
					{
						itemsdesktopsmall = [979, itemsdesktopsmall];
					}
					if( itemstablet > 0 )
					{
						itemstablet = [479, itemstablet];
					}
					if( itemsmobile > 0 )
					{
						itemsmobile = [479, itemsmobile];
					}
					jQuery(this).owlCarousel({
						transitionStyle: effect_mode,
						autoPlay : autoplay,
						navigation : navigation,
						pagination : pagination,
						singleItem : singleitem,
						items : items,
						itemsDesktop : itemsdesktop,
						itemsDesktopSmall : itemsdesktopsmall,
						itemsTablet : itemstablet,
						itemsMobile : itemsmobile,
						navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
					});
				});
			}
		 })(jQuery);
	},
	/* ---------------------
		Animation
	/* --------------------- */
	dataAnimations: function() {
	  jQuery('[data-animation]').each(function() {
			var element = jQuery(this);
			element.addClass('animated');
			element.appear(function() {

				var delay = ( element.data('delay') ? element.data('delay') : 1 );
				if( delay > 1 ) element.css('animation-delay', delay + 'ms');
				element.addClass( element.data('animation') );
				setTimeout(function() {
					element.addClass('visible');
				}, delay);

			});
	  });
	},
	/* ---------------------
		Owl Slider
	/* --------------------- */
	progressBar: function(){
		if (jQuery('.progress-bar').length) {
		 jQuery('.progress-bar').each(function() {
			jQuery(this).appear(function(){
			 var datavl = jQuery(this).attr('data-percentage');
			 jQuery(this).animate({ "width" : datavl + "%"}, '1200');
			});
		  });
		}
	},
	/* ---------------------
		Background Image Attribute
	/* --------------------- */
	bgImage: function(){
		var pageSection = jQuery(".image-bg, .parallax-bg");
		pageSection.each(function(indx){
			if (jQuery(this).attr("data-background")){
				jQuery(this).css("background-image", "url(" + jQuery(this).data("background") + ")");
			}
		});
	},
	/* ---------------------
		Fun Factor / Counter
	/* --------------------- */
	funFactor: function(){
		(function(jQuery){
			jQuery(".count-number").appear(function(){
				jQuery(this).each(function(){
					datacount = jQuery(this).attr('data-count');
					jQuery(this).find('.counter').delay(6000).countTo({
						from: 10,
						to: datacount,
						speed: 3000,
						refreshInterval: 50,
					});
				});
			});
		})(jQuery);
	},
	/* ---------------------
		Parallax BG
	/* --------------------- */
	parallaxBg: function(){
		if(jQuery('.image-bg').length != 0 && !navigator.userAgent.match(/iPad|iPhone|Android/i)){
			jQuery.stellar({
			horizontalScrolling: false,
			verticalOffset: 0,
			horizontalOffset: 0,
			responsive: true,
			scrollProperty: 'scroll',
			parallaxElements: false,
		  });
		}
    },
	/* ---------------------
		Portfolio
	/* --------------------- */
	portfolioFilter: function(){
	if(jQuery('#mix-container').length != 0){
		jQuery('#mix-container').mixItUp();
	}
	},
	/* ---------------------
		Image Popup
	/* --------------------- */
	prettyPhoto: function(){
		(function(jQuery) {
			"use strict";
			if( jQuery("a[rel^='prettyPhoto'], a[data-rel^='prettyPhoto']").length != 0 ) {
			 jQuery("a[rel^='prettyPhoto'], a[data-rel^='prettyPhoto']").prettyPhoto({hook: 'data-rel', theme: "dark_square", social_tools: false, deeplinking: false});
			}
		 })(jQuery);
	},
	/* ---------------------
		Background Video
	/* --------------------- */
	backgroundVideo: function(){
        if (typeof jQuery.fn.mb_YTPlayer != 'undefined' && jQuery.isFunction(jQuery.fn
            .mb_YTPlayer)) {
            var m = false;
            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(
                navigator.userAgent)) {
                m = true
            }
            var v = jQuery('.player');
            if (m == false) {
                v.mb_YTPlayer();
                jQuery('#video-controls a')
                    .each(function() {
                        var t = jQuery(this);
                        t.on('click', (function(e) {
                            e.preventDefault();
                            if (t.hasClass(
                                'fa-volume-off')) {
                                t.removeClass(
                                        'fa-volume-off'
                                    )
                                    .addClass(
                                        'fa-volume-down'
                                    );
                                v.unmuteYTPVolume();
                                return false
                            }
                            if (t.hasClass(
                                'fa-volume-down')) {
                                t.removeClass(
                                        'fa-volume-down'
                                    )
                                    .addClass(
                                        'fa-volume-off'
                                    );
                                v.muteYTPVolume();
                                return false
                            }
                            if (t.hasClass('fa-pause')) {
                                t.removeClass(
                                        'fa-pause')
                                    .addClass('fa-play');
                                v.pauseYTP();
                                return false
                            }
                            if (t.hasClass('fa-play')) {
                                t.removeClass('fa-play')
                                    .addClass(
                                        'fa-pause');
                                v.playYTP();
                                return false
                            }
                        }));
                    });
                jQuery('#video-controls')
                    .show();
            }
        }
    },
	/* ---------------------
		Contact Form
	/* --------------------- */
	simplecontactForm: function(){
		if ( jQuery( "#contactform" ).length !== 0 ) {
		jQuery('#contactform').bootstrapValidator({
				container: 'tooltip',
				feedbackIcons: {
					valid: 'fa fa-check',
					warning: 'fa fa-user',
					invalid: 'fa fa-times',
					validating: 'fa fa-refresh'
				},
				fields: {
					contact_name: {
                        validators: {
                            notEmpty: {
                                message: ''
                            }
                        }
                    },
                    contact_email: {
                        validators: {
                            notEmpty: {
                                message: ''
                            },
                            emailAddress: {
                                message: ''
                            }
                        }
                    },
					contact_phone: {
                        validators: {
                            notEmpty: {
                                message: ''
                            }
                        }
                    },
                    contact_message: {
                        validators: {
                            notEmpty: {
                                message: ''
                            }
                        }
                    },
				}
			})
			.on('success.form.bv', function(e) {
				e.preventDefault();
				var jQueryform        = jQuery(e.target),
				validator    = jQueryform.data('bootstrapValidator'),
				submitButton = validator.getSubmitButton();
				var form_data = jQuery('#contactform').serialize();
				jQuery.ajax({
						type: "POST",
						dataType: 'json',
						url: "php/contact-form.php",
						data: form_data,
						success: function(msg){
							jQuery('.form-message').html(msg.data);
							jQuery('.form-message').show();
							submitButton.removeAttr("disabled");
							resetForm(jQuery('#contactform'));
						},
						error: function(msg){}
				 });
				return false;
			});
		}
		function resetForm(jQueryform) {

            jQueryform.find(
                    'input:text, input:password, input, input:file, select, textarea'
                )
                .val('');

            jQueryform.find('input:radio, input:checkbox')
                .removeAttr('checked')
                .removeAttr('selected');
			jQueryform.find('button[type=submit]')
                .attr("disabled", "disabled");

        }
	},
	/* --------------------------------------------
	Career Form
	-------------------------------------------- */
	careersform: function(){
		if ( jQuery( "#careerform" ).length !== 0 ) {
		jQuery('#careerform').bootstrapValidator({
				container: 'tooltip',
				feedbackIcons: {
					valid: 'fa fa-check',
					warning: 'fa fa-user',
					invalid: 'fa fa-times',
					validating: 'fa fa-refresh'
				},
				fields: {
                    career_name: {
                        validators: {
                            notEmpty: {
                                message: ''
                            }
                        }
                    },
                    career_email: {
                        validators: {
                            notEmpty: {
                                message: ''
                            },
                            emailAddress: {
                                message: ''
                            }
                        }
                    },
					career_phone: {
                        validators: {
                            notEmpty: {
                                message: ''
                            }
                        }
                    },
                    career_file: {
						validators: {
							notEmpty: {
                                message: 'Please Upload pdf or doc or docx file'
                            },
							file: {
								extension: 'pdf,doc,docx',
								type: 'application/pdf,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword',
								message: 'The selected file is not valid!'
							}
						}
					},
                }
			})
			.on('success.form.bv', function(e) {
				e.preventDefault();
				var jQueryform        = jQuery(e.target),
				validator    = jQueryform.data('bootstrapValidator'),
				submitButton = validator.getSubmitButton();
				var form_data = jQuery('#careerform').serialize();
				jQuery.ajax({
						type: "POST",
						dataType: 'json',
						url: "php/career.php",
						data: form_data,
						success: function(msg){
							jQuery('.form-message2').html(msg.data);
							jQuery('.form-message2').show();
							submitButton.removeAttr("disabled");
							resetForm(jQuery('#careerform'));
						},
						error: function(msg){}
				 });
				return false;
			});
		}
		function resetForm(jQueryform) {

            jQueryform.find(
                    'input:text, input:password, input, input:file, select, textarea'
                )
                .val('');

            jQueryform.find('input:radio, input:checkbox')
                .removeAttr('checked')
                .removeAttr('selected');
			jQueryform.find('button[type=submit]')
                .attr("disabled", "disabled");


        }
	},
	/* ---------------------
		Subscribe Form
	/* --------------------- */
	subscribeForm: function(){
		if ( jQuery( "#subscribe_form" ).length !== 0 ) {
		jQuery('#subscribe_form').bootstrapValidator({
				container: 'tooltip',
				feedbackIcons: {
					valid: 'fa fa-check',
					warning: 'fa fa-user',
					invalid: 'fa fa-times',
					validating: 'fa fa-refresh'
				},
				fields: {
					subscribe_email: {
						validators: {
							notEmpty: {
								message: 'Email is required. Please enter email.'
							},
							emailAddress: {
								message: 'Please enter a correct email address.'
							}
						}
					},
				}
			})
			.on('success.form.bv', function(e) {
				e.preventDefault();
				var jQueryform        = jQuery(e.target),
				validator    = jQueryform.data('bootstrapValidator'),
				submitButton = validator.getSubmitButton();
				var form_data = jQuery('#subscribe_form').serialize();
				jQuery.ajax({
						type: "POST",
						dataType: 'json',
						url: "php/subscription.php",
						data: form_data,
						success: function(msg){
							jQuery('.form-message1').html(msg.data);
							jQuery('.form-message1').show();
							submitButton.removeAttr("disabled");
							resetForm(jQuery('#subscribe_form'));
						},
						error: function(msg){}
				 });
				return false;
			});
		}
		function resetForm(jQueryform) {

            jQueryform.find(
                    'input:text, input:password, input, input:file, select, textarea'
                )
                .val('');

            jQueryform.find('input:radio, input:checkbox')
                .removeAttr('checked')
                .removeAttr('selected');
			jQueryform.find('button[type=submit]')
                .attr("disabled", "disabled");

        }
	},
	/* --------------------------------------------
	 Scroll Navigation
	-------------------------------------------- */

	navMenu: function(){
		jQuery('.scroll').bind('click', function(event) {
		var jQueryanchor = jQuery(this);
		var headerH = jQuery('#navigation').outerHeight();
			jQuery('html, body').stop().animate({
				scrollTop : jQuery(jQueryanchor.attr('href')).offset().top  - 60 + "px"
			}, 1200, 'easeInOutExpo');
		event.preventDefault();
	});
		/* Active When Scroll */
		jQuery('body').scrollspy({
			target: '#topnav',
			offset: 95
		})
		/* Responsive Auto Close */
		jQuery('.one-page .nav li a').click(function () {
			 jQuery('.navbar-collapse').removeClass('in');
		});
		/* Smooth Scroll Links */
		jQuery('.page-scroll a')
            .bind('click', function(event) {
                var jQueryanchor = jQuery(this);
                jQuery('html, body')
                    .stop()
                    .animate({
                        scrollTop: jQuery(jQueryanchor.attr('href'))
                            .offset()
                            .top
                    }, 1500, 'easeInOutExpo');
                event.preventDefault();
            });
	},
	/* --------------------------------------------
	 Scroll Navigation
	-------------------------------------------- */	
	/* Text Tying Slider */
	typedSlider: function() {
		jQuery(".element").each(function(){
			var jQuerythis = jQuery(this);
			jQuerythis.typed({
			strings: jQuerythis.attr('data-elements').split(','),
			typeSpeed: 100, 
			backDelay: 3000 
			});
		});
	},
	/* --------------------------------------------
	 Video Slider 
	-------------------------------------------- */	
	videotextSlider: function() {
	jQuery(function() {
		"use strict";
		if ( jQuery( ".video-slider-text" ).length !== 0 ) {
			jQuery('.video-slider-text').easyTicker({
				direction: 'up',  
				speed: 'slow',
				interval: 4000,
				height: 'auto',
				visible: 1,
				mousePause: 0,
			});
		}
	});
	},
	/* --------------------------------------------
	 Day Counter
	-------------------------------------------- */	
	countDown: function() {
	  jQuery('.daycounter').each(function(){
	   var counter_id = jQuery(this).attr('id');
	   var counter_type = jQuery(this).data('counter');
	   var year = jQuery(this).data('year');
	   var month = jQuery(this).data('month');
	   var date = jQuery(this).data('date');
	   var countDay = new Date();
	   countDay = new Date(year, month - 1, date);
	   if( counter_type == "down" ) {
		jQuery("#"+counter_id).countdown({
		 labels: ['Years', 'Months', 'Weeks', 'Days', 'Hours', 'Mins', 'Secs'],
		 labels1: ['Year', 'Month', 'Week', 'Day', 'Hour', 'Min', 'Sec'],
		 until: countDay
		});
	   } else if( counter_type == "up" ) {
		jQuery("#"+counter_id).countdown({
		 labels: ['Years', 'Months', 'Weeks', 'Days', 'Hours', 'Mins', 'Secs'],
		 labels1: ['Year', 'Month', 'Week', 'Day', 'Hour', 'Min', 'Sec'],
		 since: countDay
		});
	   }
	  });
	 },
	 

	 /* --------------------------------------------
	Social Feeds
	-------------------------------------------- */	
	 socialPhotostream: function() {
		 
		if( jQuery(".my-feeds").length != 0 ) {
			/* ================ FLICKR FEED ================ */
			jQuery('.flickr-feed').socialstream({
				socialnetwork: 'flickr',
				limit: 12,
				username: 'google'
			})
			/* ================ PINTEREST FEED ================ */
			jQuery('.pinterest-feed').socialstream({
				socialnetwork: 'pinterest',
				limit: 12,
				username: 'vmrkela'
				})
			/* ================ INSTAGRAM FEED ================ */
			jQuery('.instagram-feed').socialstream({
				socialnetwork: 'instagram',
				limit: 12,
				username: 'google'
			})
			/* ================ INSTAGRAM FOOTER FEED ================ */
			jQuery('.instagram-footer-feed').socialstream({
				socialnetwork: 'instagram',
				limit: 10,
				username: 'google'
			})
			 /* ================ DRIBBBLE FEED ================ */
			jQuery('.dribbble-feed').socialstream({
				socialnetwork: 'dribbble',
				limit: 12,
				username: 'envato'
			})
			/* ================ NEWSFEED ================ */
			jQuery('.instagram-footer-feed').socialstream({
				socialnetwork: 'newsfeed',
				limit: 10,
				username: '#'
			}) 
			/* ================ PICASA FEED ================ */
			jQuery('.picasa-feed').socialstream({
				socialnetwork: 'picasa',
				limit: 12,
				username: 'envato'
			});
			/* ================ YOUTUBE FEED ================ */
			jQuery('.youtube-feed').socialstream({
				socialnetwork: 'youtube',
				limit: 12,
				username: 'Envato'
			})
		
		}
	},
	/* --------------------------------------------
	Price Range Slier 
	-------------------------------------------- */	
	 priceRange: function() {
		if( jQuery(".range").length != 0 ) { 
			jQuery('.range').nstSlider({
				"left_grip_selector": ".leftGrip",
				"right_grip_selector": ".rightGrip",
				"value_bar_selector": ".bar",
				"value_changed_callback": function(cause, leftValue, rightValue) {
					var jQuerycontainer = jQuery(this).parent();
					jQuerycontainer.find('.leftLabel').text(leftValue);
					jQuerycontainer.find('.rightLabel').text(rightValue);
				},
				"highlight": {
					"grip_class": "gripHighlighted",
					"panel_selector": ".highlightPanel"
				}
			});
			jQuery('#highlightRangeButton').click(function() {
				var highlightMin = Math.random() * 20,
					highlightMax = highlightMin + Math.random() * 80;
				jQuery('.nstSlider').nstSlider('highlight_range', highlightMin, highlightMax);
			});
		}
	},
	/*==========Navigation Menu============*/	
	sideNav: function() {	
		jQuery("#navigation-menu").click(function(e) {
			e.preventDefault();		
			jQuery("#wrapper .toggle-menu").animate({ right: '0px' }, "slow");		
			return false;
		});					   
		jQuery("#navigation-close").click(function(e) {
			e.preventDefault();		
			jQuery("#wrapper .toggle-menu").animate({ right: '-50%' }, "slow");		
			return false;
		});	
		
		/* ----------- Menus hide after click -- mobile devices ----------- */	
	
		jQuery('#wrapper .nav li a').click(function () {
			jQuery("#wrapper .toggle-menu").animate({ right: '-50%' }, "slow");		
			return false;
		});
		
		jQuery('.scroll-2').bind('click', function(event) {
			var jQueryanchor = jQuery(this);
			var headerH = jQuery('#navigation-menu').outerHeight();
				jQuery('html, body').stop().animate({					
					scrollTop : jQuery(jQueryanchor.attr('href')).offset().top  + 1 + "px"
				}, 1200, 'easeInOutExpo');		
			event.preventDefault();
		});
	},	
	
	/*  Background image height equal to the browser height. */
		fullScreen: function() {
			jQuery('.full-screen').css({ 'height': jQuery(window).height() });
				 jQuery(window).on('resize', function() {
					jQuery('.full-screen').css({ 'height': jQuery(window).height() });
			   });
		},
		
	fancySelect: function() {
		"use strict";
		if ( jQuery( ".fancy-select" ).length !== 0 ) {
			jQuery('.fancy-select').fancySelect();
		}
	},	
	
	
	hiddenFooter: function() {
		var footer_height = jQuery(".hidden-footer").height();
		jQuery('#page').css({ 'margin-bottom': footer_height + "px" });
		jQuery('.hidden-footer').css({ 'height': footer_height + "px" });
		jQuery('.hidden-footer').css({ 'max-height': footer_height + "px" });
		jQuery(window).on('resize', function() {
			jQuery('#page').css({ 'margin-bottom': footer_height + "px" });
			jQuery('.hidden-footer').css({ 'height': footer_height + "px" });
			jQuery('.hidden-footer').css({ 'max-height': footer_height + "px" });
		});	
	},
		
	/* --------------------------------------------
	Charts
	-------------------------------------------- */	
	allCharts: function() {
		
	jQuery(window).load( function(){
			var lineChartData = {
				labels : ["January","February","March","April","May","June","July"],
				datasets : [
					{
						fillColor : "rgba(220,220,220,.5)",
						strokeColor : "rgba(220,220,220,1)",
						pointColor : "rgba(220,220,220,1)",
						pointStrokeColor : "#fff",
						data : [10,20,40,70,100,90,40]
					},
					{
						fillColor : "rgba(151,187,205,0.5)",
						strokeColor : "rgba(151,187,205,1)",
						pointColor : "rgba(151,187,205,1)",
						pointStrokeColor : "#fff",
						data : [70,30,60,40,50,30,60]
					},
					{
						fillColor : "rgba(255,196,0,0.5)",
						strokeColor : "rgba(151,187,205,1)",
						pointColor : "rgba(151,187,205,1)",
						pointStrokeColor : "#fff",
						data : [10,40,100,70,30,80,50]
					}
				]
			};

			var barChartData = {
				labels : ["January","February","March","April","May","June","July"],
				datasets : [
					{
						fillColor : "rgba(255,196,0,0.5)",
						strokeColor : "rgba(220,220,220,1)",
						data : [50,70,90,60,70,40,50]
					}
				]

			};

			var radarChartData = {
				labels : ["Html5","Css3","Jquery","Wordpress","Joomla","Drupal","Design"],
				datasets : [
					{
						fillColor : "rgba(220,220,220,0.5)",
						strokeColor : "rgba(220,220,220,1)",
						pointColor : "rgba(220,220,220,1)",
						pointStrokeColor : "#fff",
						data : [65,59,90,81,56,55,40]
					},
					{
						fillColor : "rgba(255,196,0,0.5)",
						strokeColor : "rgba(255,196,0,1)",
						pointColor : "rgba(255,196,0,1)",
						pointStrokeColor : "#fff",
						data : [28,48,40,19,96,27,100]
					}
				]

			};

			var pieChartData = [
				{
					value: 90,
					color:"#ffc400"
				},
				{
					value : 30,
					color : "#229e05"
				},
				{
					value : 60,
					color : "#171717"
				},
				{
					value : 100,
					color : "#004eff"
				},
				{
					value : 20,
					color : "#584A5E"
				}

			];

			var polarAreaChartData = [
				{
					value : 60,
					color: "#ffc400"
				},
				{
					value : 70,
					color: "#cccccc"
				},
				{
					value : 60,
					color: "#171717"
				},
				{
					value : 30,
					color: "#229e05"
				},
				{
					value : 50,
					color: "#004eff"
				},
				{
					value : 20,
					color: "#584A5E"
				}
			];

			var doughnutChartData = [
				{
					value: 30,
					color:"#ffc400"
				},
				{
					value : 50,
					color : "#cccccc"
				},
				{
					value : 100,
					color : "#171717"
				},
				{
					value : 40,
					color : "#004eff"
				},
				{
					value : 120,
					color : "#4D5360"
				}
			];

			function showLineChart(){
				var ctx = document.getElementById("lineChartmist").getContext("2d");
				 new Chart(ctx).Line(lineChartData, {	responsive: true	});
			}

			function showBarChart(){
				var ctx = document.getElementById("barChartmist").getContext("2d");
				new Chart(ctx).Bar(barChartData, {	responsive: true	});
			}

			function showRadarChart(){
				var ctx = document.getElementById("radarChartmist").getContext("2d");
				new Chart(ctx).Radar(radarChartData, {	responsive: true	});
			}

			function showPolarAreaChart(){
				var ctx = document.getElementById("polarAreaChartmist").getContext("2d");
				new Chart(ctx).PolarArea(polarAreaChartData, {	responsive: true	});
			}

			function showPieChart(){
				var ctx = document.getElementById("pieChartmist").getContext("2d");
				new Chart(ctx).Pie(pieChartData,{	responsive: true	});
			}
			function showDoughnutChart(){
				var ctx = document.getElementById("doughnutChartmist").getContext("2d");
				new Chart(ctx).Doughnut(doughnutChartData,{	responsive: true	});
			}

			jQuery('#lineChart').appear( function(){ jQuery(this).css({ opacity: 1 }); setTimeout(showLineChart,300); },{accX: 0, accY: -155},'easeInCubic');

			jQuery('#barChart').appear( function(){ jQuery(this).css({ opacity: 1 }); setTimeout(showBarChart,300); },{accX: 0, accY: -155},'easeInCubic');

			jQuery('#radarChart').appear( function(){ jQuery(this).css({ opacity: 1 }); setTimeout(showRadarChart,300); },{accX: 0, accY: -155},'easeInCubic');

			jQuery('#polarAreaChart').appear( function(){ jQuery(this).css({ opacity: 1 }); setTimeout(showPolarAreaChart,300); },{accX: 0, accY: -155},'easeInCubic');

			jQuery('#pieChart').appear( function(){ jQuery(this).css({ opacity: 1 }); setTimeout(showPieChart,300); },{accX: 0, accY: -155},'easeInCubic');

			jQuery('#doughnutChart').appear( function(){ jQuery(this).css({ opacity: 1 }); setTimeout(showDoughnutChart,300); },{accX: 0, accY: -155},'easeInCubic');

		});

	},
	/* --------------------------------------------
	Masonry Grid
	-------------------------------------------- */	
	masonryGrid: function(){
	
			 jQuery('.masonry-grid').each(function(){  
			  var jQueryport_container = jQuery(this);  	  
			  
			  var filter_selector = jQueryport_container.parent().find('.works-filters li.active').data('filter');  
			  
				jQueryport_container.imagesLoaded(function(){  
					jQueryport_container.isotope({   
					   itemSelector: '.grid-item',
					   filter: filter_selector,
					   animationEngine: "css",
					   masonry: {
						columnWidth: '.grid-sizer'
					   }
					  });
				});
			  
			 /*  Portfolio Filter Items */
			 jQuery('.works-filters li').click(function(){			  
				jQuery(this).parents().find('.works-filters li.active').removeClass('active');    
				jQuery(this).addClass('active');
				var selector = jQuery(this).parents().find('.works-filters li.active').attr('data-filter');  
				jQuery(this).parents().find('.masonry-grid').isotope({ filter: selector, animationEngine : "css" });

				return false; 
				});
			});
				 
	},
	/* --------------------------------------------
	Product Zoom
	-------------------------------------------- */	
	productZoom: function(){	
		if ( jQuery( ".single-product" ).length !== 0 ) {
			var zoomWindowWidth;
			var zoomWindowHeight;
			
			 zoomWindowWidth    : 400;
			 zoomWindowHeight   : 470;	
			 zoomType   = 'window';
			 
			if (jQuery(window).width() < 992) {
				 zoomWindowWidth    : 0;
				 zoomWindowHeight   : 0;	
				 zoomType   = 'inner';
			}
			
			jQuery("#zoom-product").elevateZoom({				
			gallery:'zoom-product-thumb', 
			cursor: 'pointer', 
			galleryActiveClass: 'active', 
			imageCrossfade: true,
			responsive: true,
			scrollZoom: false,
			zoomWindowWidth    : zoomWindowWidth,
			zoomWindowHeight   : zoomWindowHeight,
			zoomType		: zoomType	
			}); 

			jQuery("#zoom-product").bind("click", function(e) {  
			  var ez =   jQuery('#zoom-product').data('elevateZoom');	
				jQuery.fancybox(ez.getGalleryList());
			  return false;
			});
			jQuery('#plus').click(function() {
				jQuery('#output').html(function(i, val) { return val*1+1 });
			});
			jQuery('#minus').click(function() {
				jQuery('#output').html(function(i, val) { return val*1-1 });
			});
		}	
	},
	
	/* --------------------------------------------
	Local Hosted Video Player
	-------------------------------------------- */	
	
	hostedVideo: function(){
		if ( jQuery( ".video-section" ).length !== 0 ) {
			var player = videojs('really-cool-video', { /* Options */ }, function() {
			  console.log('Good to go!');

			  this.play(); 
			 
			  this.on('ended', function() {
				console.log('awww...over so soon?');
			  });
			});
		}
	},	
	
	/* --------------------------------------------
	Local Hosted Video Player
	-------------------------------------------- */	
	gridRotator: function(){
		if ( jQuery( "#ri-grid" ).length !== 0 ) {
			jQuery( '#ri-grid' ).gridrotator( {
				   rows : 3,
				   columns : 8,
				   interval : 2000,
				   animType : 'random',
				   animSpeed : 1000,
				   step  : 2,
				   w1024 : { rows : 4, columns : 6 },
				   w768 : {rows : 4,columns : 5 },
				   w480 : {rows : 4,columns : 3 },
				   w320 : {rows : 4,columns : 2 },
				   w240 : {rows : 4,columns : 2 },
			});
		}
	},
	
	/* --------------------------------------------
	Local Hosted Video Player
	-------------------------------------------- */	
	datePicker: function(){
		if ( jQuery( ".date-picker" ).length !== 0 ) {
			jQuery(function () {
				jQuery('.date-picker').datetimepicker({
					format: 'DD/MM/YYYY'
				});
			});
		}	
	},
	timePicker: function(){
		if ( jQuery( ".time-picker" ).length !== 0 ) {
			jQuery(function () {
				jQuery('.time-picker').datetimepicker({
					format: 'LT'
				});
			});
		}	
	},
	
	/* --------------------------------------------
	 Sticky Sidebar
	-------------------------------------------- */	
	
	sidebarSticky: function(){
		if( jQuery("#sidebar").length != 0 ) { 
			jQuery('#sidebar').stickit();
		}
	}
	
}; 

jQuery(document).ready(function() {
	appMaster.pageLoader();
	appMaster.navBar();
	appMaster.toggleNav();
	appMaster.owlCarousel();
	appMaster.progressBar();
	appMaster.dataAnimations();
	appMaster.bgImage();
	appMaster.funFactor();
	appMaster.parallaxBg();
	appMaster.portfolioFilter();
	appMaster.prettyPhoto();
	appMaster.backgroundVideo();
	appMaster.simplecontactForm();
	appMaster.careersform();
	appMaster.subscribeForm();
	appMaster.navMenu();
	appMaster.typedSlider();
	appMaster.videotextSlider();
	appMaster.countDown();	
	appMaster.socialPhotostream();	
	appMaster.priceRange();	
	appMaster.allCharts();	
	appMaster.masonryGrid();
	appMaster.productZoom();
	appMaster.sideNav();
	appMaster.fullScreen();
	appMaster.fancySelect();
	appMaster.hostedVideo();
	appMaster.gridRotator();
	appMaster.datePicker();
	appMaster.timePicker();
	appMaster.sidebarSticky();
	
});	

/* --------------------------------------------
	
	Placeholder for Image
	-------------------------------------------- */	
jQuery(window).load(function(){
		
	jQuery('img:not(".site_logo")').each(function() {
		if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)){
			var ieversion=new Number(RegExp.jQuery1)
			if (ieversion>=9)
			if (typeof this.naturalWidth === "undefined" || this.naturalWidth === 0) {
			  this.src = "http://placehold.it/" + (jQuery(this).attr('width') || this.width || jQuery(this).naturalWidth()) + "x" + (this.naturalHeight || jQuery(this).attr('height') || jQuery(this).height());
			}
		} else {
			if (!this.complete || typeof this.naturalWidth === "undefined" || this.naturalWidth === 0) {
				this.src = "http://placehold.it/" + (jQuery(this).attr('width') || this.width) + "x" + (jQuery(this).attr('height') || jQuery(this).height());
			}
		}
	});
	
	jQuery('.image-bg').each(function() {
		var imageSrc = jQuery(this).data('background');
		if( imageSrc !== undefined ) {
			var newSrc = imageSrc.replace(/url\((['"])?(.*?)\1\)/gi, 'jQuery2').split(',')[0];
		}

		/*  I just broke it up on newlines for readability        */ 
		var image = new Image();
		image.src = newSrc;

		var width = image.width,
			height = image.height;
		
		if( width === 0 || height === 0 ) {
			jQuery(this).attr('data-background', "http://placehold.it/" + ('1900') + "x" + ('700') + "/2e2e2e/666.jpg" );
			
			jQuery(this).removeAttr('style');
			jQuery(this).css("background-image", "url(" + "http://placehold.it/1900x700/2e2e2e/666.jpg" + ")");
		}
	});
});

/* --------------------------------------------
Google Map
-------------------------------------------- */	
window.onload = MapLoadScript;
function GmapInit() {
	  Gmap = jQuery('.map-canvas');
	  Gmap.each(function() {
		var jQuerythis           = jQuery(this),
			lat             = -35.2835,
			lng             = 149.128,
			zoom            = 12,
			scrollwheel     = false,
			zoomcontrol 	= true,
			draggable       = true,
			mapType         = google.maps.MapTypeId.ROADMAP,
			title           = '',
			contentString   = '',
			dataLat         = jQuerythis.data('lat'),
			dataLng         = jQuerythis.data('lng'),
			dataZoom        = jQuerythis.data('zoom'),
			dataType        = jQuerythis.data('type'),
			dataScrollwheel = jQuerythis.data('scrollwheel'),
			dataZoomcontrol = jQuerythis.data('zoomcontrol'),
			dataHue         = jQuerythis.data('hue'),
			dataTitle       = jQuerythis.data('title'),
			dataContent     = jQuerythis.data('content');
			
		if( dataZoom !== undefined && dataZoom !== false ) {
			zoom = parseFloat(dataZoom);
		}
		if( dataLat !== undefined && dataLat !== false ) {
			lat = parseFloat(dataLat);
		}
		if( dataLng !== undefined && dataLng !== false ) {
			lng = parseFloat(dataLng);
		}
		if( dataScrollwheel !== undefined && dataScrollwheel !== null ) {
			scrollwheel = dataScrollwheel;
		}
		if( dataZoomcontrol !== undefined && dataZoomcontrol !== null ) {
			zoomcontrol = dataZoomcontrol;
		}
		if( dataType !== undefined && dataType !== false ) {
			if( dataType == 'satellite' ) {
				mapType = google.maps.MapTypeId.SATELLITE;
			} else if( dataType == 'hybrid' ) {
				mapType = google.maps.MapTypeId.HYBRID;
			} else if( dataType == 'terrain' ) {
				mapType = google.maps.MapTypeId.TERRAIN;
			}		  	
		}
		if( dataTitle !== undefined && dataTitle !== false ) {
			title = dataTitle;
		}
		if( navigator.userAgent.match(/iPad|iPhone|Android/i) ) {
			draggable = false;
		}
		
		var mapOptions = {
		  zoom        : zoom,
		  scrollwheel : scrollwheel,
		  zoomControl : zoomcontrol,
		  draggable   : draggable,
		  center      : new google.maps.LatLng(lat, lng),
		  mapTypeId   : mapType
		};		
		var map = new google.maps.Map(jQuerythis[0], mapOptions);
		
		var image = 'img/map-marker.png';
		if( dataContent !== undefined && dataContent !== false ) {
			contentString = '<div class="map-data">' + '<h6>' + title + '</h6>' + '<div class="map-content">' + dataContent + '</div>' + '</div>';
		}
		var infowindow = new google.maps.InfoWindow({
			content: contentString
		});
		
		var marker = new google.maps.Marker({
		  position : new google.maps.LatLng(lat, lng),
		  map      : map,
		  icon     : image,
		  title    : title
		});
		if( dataContent !== undefined && dataContent !== false ) {
			google.maps.event.addListener(marker, 'click', function() {
				infowindow.open(map,marker);
			});
		}
		
		if( dataHue !== undefined && dataHue !== false ) {
		  var styles = [
			{
			  stylers : [
				{ hue : dataHue },
				{ saturation: 80 },
				{ lightness: -10 }
			  ]
			}
		  ];
		  map.setOptions({styles: styles});
		}
	 });
}
	
function MapLoadScript() {
	var script = document.createElement('script');
	script.type = 'text/javascript';
	script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&' + 'callback=GmapInit';
	document.body.appendChild(script);
}



/*---------------STICKY NAVIGATION------------------*/
var header = jQuery('#slider-section'),
      headerPos = header.offset();
        
  jQuery(window).scroll(function() {
	  if( jQuery(".side-nav").length != 0 ) {
      if( jQuery(this).scrollTop() > headerPos.top+header.height() ) {
          jQuery('#sticky').addClass('nav-fixed').fadeIn('medium');
      } else {
          jQuery('#sticky').removeClass('nav-fixed').fadeIn('medium');
	  }
      }
});

jQuery(window).load(function() {
	appMaster.hiddenFooter();	
	appMaster.masonryGrid();
});