<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg no_margin">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

	

<script type="text/javascript">
document.addEventListener( 'wpcf7mailsent', function( event ) {
    location = 'https://www.netrootstech.com/thankyou/';
}, false );
</script>
	
<?php wp_head(); ?>
</head>

<body <?php body_class('demo-2'  ); ?> id="page">
<?php wp_body_open(); ?>

<?php require 'inc/config.php'?>

<body  class="demo-2" id="page">



<div id="ip-container" class="ip-container">



        </div>






        <header class="cd-main-header">



            <a class="cd-logo" href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/header-logo.png" alt="Logo" style="margin-left:2px"></a>



            <ul class="cd-header-buttons" style="font-size: 14px;">


                <li>Talk To Our Experts<br>
                    <i class="fas fa-mobile-alt"></i><a style="color: black" href="tel:<?php echo $sticky_number ['USA']['dialnumber']; ?>" target="_blank"><?php echo $sticky_number ['USA']['number']; ?></a>
                </li>

                <li><a class="cd-nav-trigger" href="#cd-primary-nav"><span></span></a></li>



            </ul> <!-- cd-header-buttons -->



        </header>


            <nav class="cd-nav" >

                <ul id="cd-primary-nav" class="cd-primary-nav is-fixed">

                    <li class="has-children has-children-custom view">

                        <a href="">Products</a>



                        <ul class="cd-secondary-nav is-hidden" style="overflow-x:hidden;">

                            <h2 style="font-size: 26px; margin: 0; margin-bottom: 22px; color: #007488; font-weight: bold;">Products</h2>

                            <hr>

                            <li class="go-back"><a href="#0">Menu</a></li>


							 <!-- ERP -->

                            <li class="has-children">

                                <a href="<?php echo get_page_link(528); ?>"><img
                                                src="<?php echo get_template_directory_uri(); ?>/assets/img/forsaerp-logo.png" width="40%" height="40%" alt=""></a>
                                <p>Integrate and automate your core business <br>processes</p>



                            </li>
							
							 <li class="has-children">

                                <a href="<?php echo get_page_link(441); ?>"><img
                                                src="<?php echo get_template_directory_uri(); ?>/assets/img/forsahr-logo-1.png" width="40%" height="40%" alt=""></a>
                                <p>Consolidate your HR processes within a simple,<br> yet thorough, HR system</p>



                            </li>


							

                            <li class="has-children" >

                                <a href="http://dentomate.com/"><img
                                                src="<?php echo get_template_directory_uri(); ?>/assets/img/nrt-dentomate-1.png" width="40%" height="40%" alt=""></a>
                                <p>Step into the digital age with our EHR <br>system for dental clinics</p>




                            </li>
                           



                            <li class="has-children" style="border: none">

                                <a href="<?php echo get_page_link(119); ?>"><img
                                                src="<?php echo get_template_directory_uri(); ?>/assets/img/nrt-crm-1.png" width="45%" height="40%" alt=""></a>
                                <p>Build better relationships with <br>your customers and boost sales</p>


                            </li>

                            <li class="has-children" style="border: none">

                                <a href="<?php echo get_page_link(189); ?>"><img
                                                src="<?php echo get_template_directory_uri(); ?>/assets/img/nrt-school-1.png" width="50%" height="40%" alt=""></a>
                                <p>Manage student information and the school’s <br>administrative functions </p>



                            </li>





                           



                            <li class="has-children" style="border: none;">

                                <a href="<?php echo get_page_link(268); ?>"><img
                                                src="<?php echo get_template_directory_uri(); ?>/assets/img/nrt-property-1.png" width="60%" height="40%" alt=""></a>
                                <p>Simplify residential and commercial property <br>management by automating processes</p>


                            </li>



                        </ul>

                    </li>





                    <li class="has-children has-children-custom">







                        <a href="#">Services</a>





                        <ul class="cd-secondary-nav cd-secondary-nav-1 is-hidden" style="overflow-x:hidden;">

                            <h2 style="font-size: 26px; margin: 0; margin-bottom: 22px; color: #007488; font-weight: bold;">Services</h2>

                            <hr>



                            <li class="go-back"><a href="#0">Menu</a></li>



                            <li class="has-children">

                                <a href="<?php echo get_page_link(6); ?>">Digital Marketing & SEO</a>
                                <p>Increase your visibility
                                    <a href="#" class="sbtn">Read more</a></p>

                            </li>







                            <!-- ERP -->

                            <li class="has-children">

                                <a href="<?php echo get_page_link(9); ?>">BI & Big Data Analytics</a>
                                <p>Use information to achieve more
                                    <a href="#" class="sbtn">Read more</a></p>

                            </li>





                            <li class="has-children">

                                <a href="<?php echo get_page_link(11); ?>">E-Commerce</a>
                                <p>Sell more
                                    <a href="#" class="sbtn">Read more</a></p>
                            </li>

                            <li class="has-children">

                                <a href="<?php echo get_page_link(54); ?>">Web Development</a>
                                <p>Take your business into the cyber world
                                    <a href="#" class="sbtn">Read more</a></p>
                            </li>





                            <li class="has-children">

                                <a href="<?php echo get_page_link(15); ?>">Medical Billing</a>
                                <p>Get paid for your healthcare services
                                    <a href="#" class="sbtn">Read more</a></p>

                            </li>



                            <li class="has-children">

                                <a href="<?php echo get_page_link(17); ?>">Blockchain</a>
                                <p>Perform free, secure transactions
                                    <a href="#" class="sbtn">Read more</a></p>

                            </li>

                            <li class="has-children">

                                <a href="<?php echo get_page_link(19); ?>">E-Wallet</a>
                                <p>Make sending payments more convenient
                                    <a href="#" class="sbtn">Read more</a></p>

                            </li>

                            <li class="has-children">

                                <a href="<?php echo get_page_link(21); ?>">Mobile App Development</a>
                                <p>Fit your business in the palm of your hand
                                    <a href="#" class="sbtn">Read more</a></p>



                            </li>



                            <li class="has-children" style="border: none;">

                                <a href="<?php echo get_page_link(24); ?>"> UI-UX Design</a>
                                <p>Don’t give up beauty for utility
                                    <a href="#" class="sbtn">Read more</a></p>


                            </li>

                            <li class="has-children" style="border: none;">

                                <a href="<?php echo get_page_link(26); ?>"> Support and Maintenance</a>
                                <p>Don’t give up beauty for utility
                                    <a href="#" class="sbtn">Read more</a></p>


                            </li>





                        </ul>



                    </li>



                    <li><a href="<?php echo get_page_link(44); ?>">About us</a></li>

                    <li><a href="<?php echo get_page_link(47); ?>">Contact us</a></li>

                    <li><a href="<?php echo get_page_link(49); ?>">Blog</a></li>

                    <a href="<?php echo get_page_link(52); ?>" class="btn btn-info" style=" padding-left: 15px;  padding-right: 15px; margin-top: 16px; background-color: #3ca3bf; border-bottom: none; display: inline-block; margin-left: 30px;">BUSINESS PROFILE </a>

                </ul> <!-- primary-nav -->



            </nav> <!-- cd-nav -->


