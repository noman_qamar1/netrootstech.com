<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_template_part( 'template-parts/header/header', 'hrm' ); ?>

<!-- ------------------------------------------------template body start from here -->

<div class="top-banner">
        <div class="container">
            <div class="banner-content">
                <h1>
                    Payroll Management
                </h1>
                <p>
                   You don’t need to break the bank for the best  payroll
                </p>
                <p>
                  software to record and manage your finances!
                </p>


            </div>




        </div>



    </div>

    <!--banner ends-->


    <!--module-area start-->

    <div class="module-area">

        <div class="container">
            <h2>
                You don’t need to break the bank for the best payroll software to record and manage your finances!
            </h2>
            <p>
               With our practical yet affordable payroll software, you get the best of both worlds! Fórsa HR software enables you to manage employee payrolls, calculate taxes, EOBI, and gratuity, generate salary slips, carry out deductions, and manage loans. In addition, it stores bank account details and salary reports and receipts. 
            </p>
            <div class="module-chart">
                <div class="row">
                    <div class="col-md-4">

                        <div class="branches-payroll" data-aos="flip-up">

                            <a href="#">
                                <h4>
                                    Payroll Generation
                                </h4>
                                <p>
                                    The Payroll Generation sub-module allows you to record the salaries of all your employees, including the amounts paid along with the dates. Through this sub-module, you can also print salary slips.
                                </p>
                            </a>

                        </div>

                        <div class="branches-payroll" data-aos="flip-up">
                            <a href="#">

                                <h4>
                                    Payroll Reports
                                </h4>
                                <p>
                                    The Payroll Reports sub-module of the payroll management software of Fórsa HRM is used to create monthly payroll reports, so that you can get an insight into the finances of your company. 
                                </p>
                            </a>

                        </div>



                    </div>
                    <div class="col-md-4">

                        <figure class="module-tablet"  data-aos="zoom-in">

                            <a href="#">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/images/payroll-tab.png" alt="dashboard" width="100%">
                            </a>

                        </figure>

                    </div>
                    <div class="col-md-4">

                        <div class="branches-right-payroll" data-aos="flip-up">
                            <a href="#">

                                <h4>
                                  Advances and Loan Management
                                </h4>
                                <p>
                                    This sub-module keeps track of the advances or loans requested by and granted to employees. The payroll software records the name of the employee, the scheme under which the money is loaned, the total amount loaned, the loan term, how many instalments that have to be paid back, and the remaining amount to be paid. 
                                </p>
                            </a>

                        </div>

                       


                    </div>


                </div>



            </div>



        </div>

    </div>

    <!--module-area ends-->


    <!--content-wraper start-->

    <div class="content-wraper">

        <div class="container">

            <div class="row">



                <div class="col-md-6">

                    <div class="wraper-text"data-aos="fade-right"
                         data-aos-offset="300"
                         data-aos-easing="ease-in-sine">

                        <h2>
                            Integrated, Fast, Efficient
                        </h2>
                        <p>
                            Fórsa payroll management software is highly integrated with many of the other modules of the HR software. Not only does the software provide fast communication among the modules for an accurate calculation of payroll, but also manages the company’s loans and advances, thus keeping track of all money flow. Finally, monthly reports generated by the payroll software allow you to view the data in a clear and concise form, allowing for efficient analysis of your company’s finances. 

                        </p>



                    </div>



                </div>
                <div class="col-md-6">
                    <figure class="wraper-image" data-aos="zoom-in-up">

                        <a href="#">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/images/payroll-laptop.png" alt="dashboard" width="100%">
                        </a>

                    </figure>

                </div>

            </div>



        </div>

    </div>

    <!--content-wraper end-->

    <!--text area-->

    <div class="text-area" data-aos="zoom-in-down">

        <div class="container">

            <h2>
                Why use Fórsa’s Payroll Management Software?
            </h2>

            <p>

              Our payroll software is cloud-based, allowing for the recording of real time data, accurate reporting, and quick updates to software for compliance with legal changes and updates. Moreover, using our software, you can generate updated reports to keep track of your finances. Our payroll management software also allows employees to create and access their own accounts for self-management, taking pressure off of your company’s HR staff. Most importantly, our payroll software integrates all payroll functions into one system, simplifying financial management and calculations for your business.

            </p>


        </div>



    </div>


<!-- ----------------------------------------------template end here -->

<script>
	AOS.init();
	</script>
<?php
get_footer();