<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>


    <main class="cd-main-content">

        <!-- your content here -->


        <div class="wf">

            <!--------------------------------

Banner Area Start

--------------------------------->


            <div class="banner-area wf">

                <div class="carousel-wrapper owl-carousel-banner " id="owlcarousel">


                    <div class="owl-carousel owl-theme" id="owl-carousel-banner">
						
						

                        <div class="item" style="margin-top: 88px;">

                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/Visionary2.jpg"
                                 alt="Alt text"
                                 longdesc="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam, eum? Deserunt, quas.">


                        </div>


                        <div class="item" style="margin-top: 88px;">

                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/ERP-Banner-Back-Image.jpg"
                                 alt="Alt text"
                                 longdesc="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt, nemo nisi velit?">

                            <div class="caption hh">

                                <h3>NetRoots ERP</h3>

                                <p>Integrate the core processes of your business into a</p>

                                <p>single, customizable system to boost productivity,</p>

                                <p> save time, and increase profits</p>


                                <button title="Corusom ponent alur" class="btn btn-light "><a href="<?php echo get_page_link(47); ?>">GET A
                                        QUOTE</a></button>
                                <button title="Corusom ponent alur" class="btn btn-light rbtn"
                                        style="background-color: #ffffff0f;"><a href="<?php echo get_page_link(116); ?>">LEARN MORE</a>
                                </button>

                            </div>

                        </div>

                        <div class="item" style="margin-top: 88px;">

                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/HRM-Banner-Back-Image.jpg"
                                 alt="Alt text"
                                 longdesc="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium, iure error voluptatem.">

                            <div class="caption hh">

                                <h3>NetRoots HRM</h3>

                                <p>Organize and consolidate your HR processes to save time,</p>

                                <p>lower operational costs, and revolutionize employee</p>

                                <p> management</p>


                                <button title="Corusom ponent alur" class="btn btn-light "><a href="<?php echo get_page_link(47); ?>">GET A
                                        QUOTE</a></button>
                                <button title="Corusom ponent alur" class="btn btn-light rbtn"
                                        style="background-color: #ffffff0f;"><a href="http://www.forsahr.com/">LEARN MORE</a>
                                </button>

                            </div>

                        </div>

                        <div class="item" style="margin-top: 88px;">

                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/CRM-Banner-Back-Image.jpg"
                                 alt="Alt text"
                                 longdesc="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium, iure error voluptatem.">

                            <div class="caption hh">

                                <h3>NetRoots CRM</h3>

                                <p>Record, manage, and analyze interactions with leads and</p>

                                <p>customers to improve relationships and increase sales.</p>


                                <button title="Corusom ponent alur" class="btn btn-light "><a href="<?php echo get_page_link(47); ?>">GET A
                                        QUOTE</a></button>
                                <button title="Corusom ponent alur" class="btn btn-light rbtn"
                                        style="background-color: #ffffff0f;"><a href="<?php echo get_page_link(119); ?>">LEARN MORE</a>
                                </button>

                            </div>

                        </div>

                        <div class="item" style="margin-top: 88px;">

                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/AI.jpg" alt="Alt text"
                                 longdesc="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium, iure error voluptatem.">

                            <div class="caption hh">

                                <h3 style="font-size: 48px;">ARTIFICIAL INELLIGENCE</h3>

                                <p>Use intelligent software capable of interpreting its </p>

                                <p>environment and taking complex actions to fit your</p>

                                <p>business needs</p>


                            </div>

                        </div>

                        <div class="item" style="margin-top: 88px;">

                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/Dentomate.jpg"
                                 alt="Alt text"
                                 longdesc="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam, eum? Deserunt, quas.">

                            <div class="caption hh">

                                <h3 style="color: black;">DentoMate</h3>

                                <p style="color: black;">Expedite and automate administrative tasks to </p>

                                <p style="color: black;">increase efficiency in your dental practice with our</p>

                                <p style="color: black;"> electronic health record (EHR) system</p>


                                <button title="Corusom ponent alur" class="btn btn-light "><a href="<?php echo get_page_link(47); ?>">GET A
                                        QUOTE</a></button>
                                <button title="Corusom ponent alur" class="btn btn-light rbtn"
                                        style="background-color: black;"><a href="http://dentomate.com/">LEARN MORE</a>
                                </button>

                            </div>

                        </div>
                        <div class="item" style="margin-top: 88px;">

                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/Banner-02-SEO-1-1024x410.png"
                                 alt="Alt text">
                            <!--form starts-->

                            <div class="se0-caption caption" style="left: 5%; color: white; margin-top:34px; ">
                                <h3 style="text-align: center; margin-top: -120px; font-size: 48px; margin-bottom: 20px;">
                                    Increase Your <br>Social Media Presence</h3>
                                <p style="text-align:center; margin-bottom: 20px;">A digital marketing and social media
                                    management agency</p>
                                <?php echo do_shortcode('[contact-form-7 id="440" title="Seo Form Banner"]'); ?>
                            </div>

                            <!--form ends-->


                        </div>
                    </div>
                </div>


            </div>


        </div>

        <!--

                <svg class="progress-circle" width="100" height="100">

                    <circle cx="50" cy="50" r="40"></circle>

                </svg>

                -->

        <div class="carousel-loader">

            <div class="loader-bar"></div>

        </div>

        </div>


        </div>


        <!--------------------------------

        Our Products

        --------------------------------->


        <div class="our-product-con wf no_paddind text-center">

            <div class="container">

                <div class="our-product-inner">

                    <h4 class="medium_font__Montserrat main-heading color-blue text-uppercase ">our products</h4>

                    <p class="paragraph_text color-black">Our state-of-the-art software is curated to fit your business
                        needs</p>

                    <p class="paragraph_text color-black regular_font__Montserrat" style="text-align: justify">Our team
                        of experienced professionals assesses your requirements to create a number of products to meet
                        your business needs, including ERP, HRMS, and CRM software. Each of these products is customized
                        to solve specific business challenges and to help you grow your business.


                </div>


            </div>


            <div class="wf our-product-bg">

                <div class="container">


                    <div class="prouduct-icon-box-con">


                        <div class="product-icon-con">


                            <a href="./Products/erp">

                                <div class="product-icon-box">

                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/product-icon-erp.png"/>

                                    <h2 class="change1 paragraph_text color-black">Enterprise Resource

                                        Planning</h2>

                                </div>

                            </a>


                            <a href="./Products/crm">

                                <div class=" product-icon-box">

                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/product-icon-crm.png"/>

                                    <h2 class="change2 paragraph_text color-black">Customer Relationship

                                        Management</h2>

                                </div>

                            </a>


                            <a href="./Products/hrm/">

                                <div class="product-icon-box">

                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/product-icon-hrm.png"/>

                                    <h2 class="change3 paragraph_text color-black">Human Relationship

                                        Management</h2>

                                </div>

                            </a>

                        </div>


                        <div class="product-icon-con">


                            <a href="./Products/pms">

                                <div class="change product-icon-box">

                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/product-icon-property.png"/>

                                    <h2 class="change4 paragraph_text color-black">Property Manegment</h2>

                                </div>

                            </a>

                            <a href="https://dentomate.com/">

                                <div class="change product-icon-box">

                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/product-icon-dentomate.png"/>

                                    <h2 class="change4 paragraph_text color-black">Dentomate</h2>

                                </div>

                            </a>


                            <a href="./Products/smcs/">

                                <div class="product-icon-box">

                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/Student-Consulting.png">

                                    <h2 class="change5 paragraph_text color-black">School Manegment System</h2>

                                </div>

                            </a>


                        </div>


                    </div>

                </div>

            </div>

        </div>


        <!--------------------------

       services & About Section

       <!-- -------------------- -->

        <div class="services_about_con no_paddind col-lg-12 wf">

            <div class="services_about_inner  no_paddind col-lg-12 wf">

                <div class="container">


                    <div class="our_services_con wf" style="padding-top: 70px;">


                        <div class="case_study_head text-center wf">

                            <h4 class="medium_font__Montserrat main-heading color-black text-uppercase ">Our
                                Services</h4>

                            <p class="paragraph_text color-black">Our competent, experienced professionals will help
                                transform the way you do business</p>

                            <!--case_study-->

                        </div>
                        
                
                <style>
                        <?php
            // the query
                $count=1;
            $wpb_all_query = new WP_Query(array('post_type'=>'post', 'cat'=> 21 , 'post_status'=>'publish', 'posts_per_page'=>5)); ?>
            <?php if ( $wpb_all_query->have_posts() ) : ?>
                <!-- the loop -->
                <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
                    .service_icon_0<?php echo $count; ?>{
                        background: url(<?php echo get_field('blue-image'); ?>) no-repeat center center;
                    }
                    <?php if($count == 1){?>
                     .service_icon_0<?php echo $count; ?>{
                        background: url(<?php echo get_field('blue-image'); ?>) no-repeat center center;
                    }
                    .tabs li:first-child:hover .service_icon_0<?php echo $count; ?> {
                        background: url(<?php echo get_field('hover-image'); ?>) no-repeat center center;
                    }
                    .tabs li:first-child.active .service_icon_0<?php echo $count; ?>{
                        background: url(<?php echo get_field('hover-image'); ?>) no-repeat center center;
                    }
                        
                   <?php } else { ?>
                    .tabs li:nth-child(<?php echo $count; ?>):hover .service_icon_0<?php echo $count; ?> {
                        background: url(<?php echo get_field('hover-image'); ?>) no-repeat center center;
                    }
                    .tabs li:nth-child(<?php echo $count; ?>).active .service_icon_0<?php echo $count; ?>{
                        background: url(<?php echo get_field('hover-image'); ?>) no-repeat center center;
                    }
                    <?php } ?>
                
                
                
                <?php $count++; endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>

                </style>

                        <ul class="tabs">

                            <?php
            // the query
   $count=1;
            $wpb_all_query = new WP_Query(array('post_type'=>'post', 'cat'=> 21 , 'post_status'=>'publish', 'posts_per_page'=>6)); ?>
            <?php if ( $wpb_all_query->have_posts() ) : ?>
                <!-- the loop -->
                <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
                     
                            <li class=" paragraph-text color-black medium_font__Montserrat" rel="tab<?php echo $count ?>">
                                <div class="tabs_head_inner  paragraph-text color-black medium_font__Montserrat">
                                    <div class="service_icon_0<?php echo $count ?>"><span>
                                      </span></div>
                                    <?php echo the_title(); ?>
                                    <!--tabs_head_inner-->
                                </div>
                            </li>
                            
<?php $count++; endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>


                        </ul>


                        <div class="tab_container" style=" margin-bottom: 50px;">

                             <?php
            // the query
   $count=1;
            $wpb_all_query = new WP_Query(array('post_type'=>'post', 'cat'=> 21 , 'post_status'=>'publish', 'posts_per_page'=>6, 'order'=>'DESC')); ?>
            <?php if ( $wpb_all_query->have_posts() ) : ?>
                <!-- the loop -->
                <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
							<div>
						
					<h3 class="d_active tab_drawer_heading" rel="tab<?php echo $count ?>"><?php echo the_title(); ?></h3>
                            <div id="tab<?php echo $count ?>" class="tab_content">
                                <div class="col-lg-12 no_paddind">
                                    <div class="col-sm-12 col-md-5  service_img">
                                        <?php the_post_thumbnail();?>
                                        <!--col-lg-6-->
                                    </div>

                                    <div class="col-sm-12 col-md-7   service_txt">

                <h3 class="color-blue medium_font__Montserrat sub-heading text-uppercase"><?php echo the_title(); ?></h3>
 <p class="paragraph-text color-black medium_font__Montserrat"><?php echo get_field('sub-heading'); ?></p>

     <p class="paragraph-text color-black" style="text-align: justify;"><?php echo the_content(); ?> </p>
<a href="<?php echo get_field('links') ?>" class="btn-info" style="    padding: 10px 15px; margin-top: 10px; display: inline-block; font-size: 16px; font-weight: bold;">Read More</a>
                                        <!--col-lg-6-->
                                    </div>
                                    <!--col-lg-12-->
                                </div>
                            </div>
							</div>
	 
<?php $count++; endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>

                          

                        </div>

                        <!-- .tab_container -->

                    </div>


                    <!--container-->

                </div>


                <div class="about_top_icon_con wf">

                    <div class="about_top_icon">

                        <div class="about_icon_bullet wf">

                            <div class="col-md-4" style="margin-top: 15px;">

                                <div class="about_icon_inner text-center">

                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/about_icon_1.png"
                                         alt="about_icon_1"/>

                                    <p class="paragraph-text color-black medium_font__Montserrat"
                                       style="margin-top: 10px;">Customer Satisfaction

                                        Guarantee!</p>

                                    <!--about_icon_inner-->

                                </div>

                                <!--col-md-4-->

                            </div>


                            <div class="col-md-4" style="margin-top: 15px;">

                                <div class="about_icon_inner text-center">

                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/about_icon_2.png"
                                         alt="about_icon_1"/>

                                    <p class="paragraph-text color-black medium_font__Montserrat">Super Fast Turnaround

                                        Time Guarantee!</p>

                                    <!--about_icon_inner-->

                                </div>

                                <!--col-md-4-->

                            </div>


                            <div class="col-md-4" style="margin-top: 15px;">

                                <div class="about_icon_inner text-center">

                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/about_icon_3.png"
                                         alt="about_icon_1"/>

                                    <p class="paragraph-text color-black medium_font__Montserrat">IT Support 24/7, 365

                                        Days a Year</p>

                                    <!--about_icon_inner-->

                                </div>

                                <!--col-md-4-->

                            </div>

                        </div>


                        <!--about_icon_bullet-->

                    </div>

                    <div class="about_left no_paddind col-lg-12"
                         style="text-align: center; background-color: #e8e8ea;   ">

                        <div class="case_study_head wf">

                            <h4 class="medium_font__Montserrat main-heading color-black text-uppercase "
                                style="padding-top: 20px;">About Us</h4>

                            <p class="paragraph_text color-black">At NetRoots, we ensure that your business finds
                                exactly what it needs to grow.<br/>


                                <!--case_study-->

                        </div>

                        <div class="about_text">

                            <p class="paragraph_text color-black regular_font__Montserrat" style="text-align: justify;">
                                NetRoots Technologies is a modern Information Technology services and consultancy firm,
                                designed especially to help businesses succeed in the increasingly digital times. From
                                application development to knowledge and business process management, we address your IT
                                needs with care and precision. We understand that in this day and age, IT is
                                instrumental to the growth and success of any business. Thus, our experienced
                                professionals strive to help SMEs develop unique, sustainable, and innovative solutions
                                to complex challenges.</p>


                            <!--                                            <div class="readmore_btn wf">-->

                            <!--                                                <a href="#" class="color-blue medium_font__Montserrat sub-heading text-uppercase">Read More <img src="<?php echo get_template_directory_uri(); ?>/assets/images/readmore_btn.png" alt="" /></a>-->

                            <!--                                               -->

                            <!--                                            </div>-->

                            <!--about_text-->

                        </div>


                        <!--no_paddind col-lg-7-->

                    </div>

                    <!--about_us_con wf-->


                </div>


                <!--services_about_inner-->

            </div>

            <!--services_about_con-->

        </div>


       


        <!---------------------->

        <!--Case Study Section-->

        <!---------------------->

        <div class="case_study_con no_paddind col-lg-12 text-center">

            <div class="case_study_head wf">

                <h4 class="medium_font__Montserrat main-heading color-black text-uppercase ">Success Stories</h4>

                <div class="container">

                    <p class="paragraph_text color-black">We have provided tailor-made solutions to help grow many
                        businesses. <br/>

                        Take a look at what we’ve done!</p>

                </div>

                <!--case_study-->

            </div>


            <div class="col-lg-12 no_paddind">

                <div class="container">


                    <div class="mockups">


                        <div class="col-lg-6">

                            <div class="css-mb with-glare" style="margin-top: 10px;">

                                <div class="mb-display-position">

                                    <div class="mb-display">

                                        <div class="mb-screen-position">

                                            <div class="mb-screen">

                                                <section id="mockup-slider" class="owl-carousel">

                                                    <div>
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/aog.jpg"
                                                             alt=""/></div>

                                                    <div>
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/TUV1.jpg"
                                                             alt=""/></div>

                                                    <div>
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/nkhan.png"
                                                             alt=""/></div>

                                                    <div>
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/mtel.png"
                                                             alt=""/></div>

                                                    <div>
                                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/sufi.jpg"
                                                             alt=""/></div>

                                                </section>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                                <div class="mb-body"></div>

                                <div class="mb-bottom-cover"></div>

                            </div>

                            <!--col-lg-6-->

                        </div>


                        <div class="col-lg-6">


                            <div id="mockup-slider-titles" class="owl-carousel">

                                <div>

                                    <div class="case_study_text_con text-left">

                                        <h5 class="text-uppercase color-blue medium_font__Montserrat sub-heading">Office
                                            Automation Group (OAG)</h5>


                                        <div class="case_study_text_list wf text-left">

                                            <ul>

                                                <li>

                                                    <span class="challange wf color-blue medium_font__Montserrat">Challenge:</span>

                                                    <p class="light_font__Montserrat color-black paragraph-text"
                                                       style="text-align: justify;">Since its inception in 1989, OAG
                                                        managed its imports manually. As a distributor of brands such as
                                                        Panasonic, Konica Minolta, and Pantum, OAG had a large quantity
                                                        of imports, and keeping track of them manually was tedious,
                                                        time-consuming, and labour-intensive. </p>

                                                </li>

                                                <li>

                                                    <span class="solution  wf  color-blue medium_font__Montserrat">Solution:</span>

                                                    <p class="light_font__Montserrat color-black paragraph-text"
                                                       style="text-align: justify;">NetRoots crafted a customized ERP
                                                        system for OAG to automate its imports, significantly
                                                        simplifying the import process and the paperwork associated with
                                                        it. Our ERP for OAG also includes, in addition to the core
                                                        modules, features including image tracking of products and an
                                                        option to account for exchange rate fluctuations. </p>

                                                </li>

                                                <li></li>

                                                <li></li>

                                                <li></li>

                                            </ul>


                                            <!--case_study_text_list-->

                                        </div>

                                        <!--case_study_text_con-->

                                    </div>


                                </div>


                                <div>

                                    <div class="case_study_text_con text-left">

                                        <h5 class="text-uppercase color-blue medium_font__Montserrat sub-heading">TUV
                                            Austria</h5>


                                        <div class="case_study_text_list wf text-left">

                                            <ul>

                                                <li>

                                                    <span class="challange wf color-blue medium_font__Montserrat">Challenge:</span>

                                                    <p class="light_font__Montserrat color-black paragraph-text"
                                                       style="text-align: justify;">TUV Austria, a globally accredited
                                                        testing, inspection and certification company, regularly hires
                                                        contractual employees. Before acquiring our HRMS, TUV explored
                                                        the market but could not find an affordable HR system to
                                                        generate customized payrolls for the different kinds of
                                                        employees. </p>

                                                </li>

                                                <li>

                                                    <span class="solution  wf  color-blue medium_font__Montserrat">Solution:</span>

                                                    <p class="light_font__Montserrat color-black paragraph-text"
                                                       style="text-align: justify;">NetRoots built an HR system for TUV
                                                        that not only creates unique payrolls for each employee, but
                                                        also incorporates a number of other features that are helpful
                                                        for a company that has a high recruitment rate. TUV’s HRMS
                                                        contains a new employee induction module, a probation evaluation
                                                        module, and the option to provide feedback on training</p>

                                                </li>

                                                <li></li>

                                                <li></li>

                                                <li></li>

                                            </ul>


                                            <!--case_study_text_list-->

                                        </div>

                                        <!--case_study_text_con-->

                                    </div>


                                </div>

                                <div>

                                    <div class="case_study_text_con text-left">

                                        <h5 class="text-uppercase color-blue medium_font__Montserrat sub-heading">NK
                                            Consultancy</h5>


                                        <div class="case_study_text_list wf text-left">

                                            <ul>

                                                <li>

                                                    <span class="challange wf color-blue medium_font__Montserrat">Challenge:</span>

                                                    <p class="light_font__Montserrat color-black paragraph-text"
                                                       style="text-align: justify;">Naseem Khan Consultancy provides
                                                        educational consulting services to students applying to
                                                        universities in the UK. Due to a high volume of applicants, Mrs.
                                                        Khan needed help keeping track of the students, their documents,
                                                        and their applications. </p>

                                                </li>

                                                <li>

                                                    <span class="solution  wf  color-blue medium_font__Montserrat">Solution:</span>

                                                    <p class="light_font__Montserrat color-black paragraph-text"
                                                       style="text-align: justify;">NetRoots created for Naseem Khan
                                                        Consultancy a student management system that records and stores
                                                        all student information and documents, and tracks student
                                                        applications and visa processes. Applicants can also upload
                                                        their documents directly to the system and view their
                                                        application status, simplifying the whole application
                                                        process. </p>

                                                </li>

                                                <li></li>

                                                <li></li>

                                                <li></li>

                                            </ul>


                                            <!--case_study_text_list-->

                                        </div>

                                        <!--case_study_text_con-->

                                    </div>


                                </div>


                                <div>

                                    <div class="case_study_text_con text-left">

                                        <h5 class="text-uppercase color-blue medium_font__Montserrat sub-heading">Emtel
                                            Communications</h5>


                                        <div class="case_study_text_list wf text-left">

                                            <ul>

                                                <li>

                                                    <span class="challange wf color-blue medium_font__Montserrat">Challenge:</span>

                                                    <p class="light_font__Montserrat color-black paragraph-text"
                                                       style="text-align: justify;"> Emtel is a communications company
                                                        that provides telecommunications services as well as IT
                                                        solutions. Emtel was unable to find an affordable ERP system
                                                        that could account for its inventory as the good were owned both
                                                        by the company and by other parties. </p>

                                                </li>

                                                <li>

                                                    <span class="solution  wf  color-blue medium_font__Montserrat">Solution:</span>

                                                    <p class="light_font__Montserrat color-black paragraph-text"
                                                       style="text-align: justify;">NetRoots took into consideration the
                                                        company’s unique needs and crafted ERP software with an
                                                        inventory module that helped track goods with multiple
                                                        ownerships. We also added a feature to facilitate the ‘request
                                                        lifecycle’ for office and site expenses, greatly easing their
                                                        business processes. </p>

                                                </li>

                                                <li></li>

                                                <li></li>

                                                <li></li>

                                            </ul>


                                            <!--case_study_text_list-->

                                        </div>

                                        <!--case_study_text_con-->

                                    </div>


                                </div>

                                <div>

                                    <div class="case_study_text_con text-left">

                                        <h5 class="text-uppercase color-blue medium_font__Montserrat sub-heading">Sufi
                                            Flour Mills</h5>


                                        <div class="case_study_text_list wf text-left">

                                            <ul>

                                                <li>

                                                    <span class="challange wf color-blue medium_font__Montserrat">Challenge:</span>

                                                    <p class="light_font__Montserrat color-black paragraph-text"
                                                       style="text-align: justify;">For over four decades, Sufi Flour
                                                        Mills used manual records. Being one of the largest flour mill
                                                        companies in Pakistan, Sufi was spending expensive resources and
                                                        time and encountering issues in manufacturing management,
                                                        production tracking, and accounts. </p>

                                                </li>

                                                <li>

                                                    <span class="solution  wf  color-blue medium_font__Montserrat">Solution:</span>

                                                    <p class="light_font__Montserrat color-black paragraph-text"
                                                       style="text-align: justify;">NetRoots’ customized ERP system
                                                        automated manufacturing management and accounts, and enabled
                                                        production tracking for Sufi. The software includes a feature
                                                        issuing ‘gate passes’ to regulate the distribution and inventory
                                                        of flour.</p>

                                                </li>

                                                <li></li>

                                                <li></li>

                                                <li></li>

                                            </ul>


                                            <!--case_study_text_list-->

                                        </div>

                                        <!--case_study_text_con-->

                                    </div>


                                </div>

                            </div>

                            <!--col-lg-6-->

                        </div>


                        <!-- Add or remove "with-glare" class. Removing the glare makes the screen interactive -->


                        <div class="controls">

                            <a href="javascript:void(0);" class="prev">Prev</a>

                            <a href="javascript:void(0);" class="next">Next</a>

                        </div>


                    </div>


                    <!--container-->

                </div>

                <!--col-lg-12-->

            </div>


            <!--case_study_con no_paddind col-lg-12-->

        </div>

        <!---------------------->

        <!--Case Study Section-->

        <!---------------------->


        <!---------------------->

        <!--testimonial Section-->

        <!---------------------->

        <div class="testimonial_con no_paddind col-lg-12 text-center">

            <div class="container">

                <div class="testimonial_inner wf">

                    <div class="testimonial_head wf">

                        <h4 style="font-size: 40px"
                            class="medium_font__Montserrat main-heading color-white text-uppercase ">Happy Clients</h4>

                        <p style="font-size: 21px" class="paragraph_text color-white"> We've helped over 25 companies
                            across 7 different industries grow their businesses with technology.

                        </p>

                        <!--testimonial_head-->

                    </div>

                    <!--testimonial_inner-->

                </div>


                <div class="testimonial_content wf">

                    <div class="large-12 columns col-lg-12">

                        <div class="owl-carousel owl-carousel34 owl-theme">

   <?php
            // the query
            $wpb_all_query = new WP_Query(array('post_type'=>'post', 'cat'=> 7 , 'post_status'=>'publish', 'posts_per_page'=>6)); ?>
            <?php if ( $wpb_all_query->have_posts() ) : ?>
                <!-- the loop -->
                <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
                            <div class="item">
                                <div class="client_word_con wf">
                                    <div class="client_img_con wf text-center">
                                        <div class="client_img_holder">
                                            <?php the_post_thumbnail( array(80, 80), array('class' => 'img-circle'));?>
                                            <!--client_img_holder-->
                                        </div>
                                        <!--client_img-->
                                    </div>
                                    <div class="client_word wf">
                                        <h4 class="medium_font__Montserrat sub-heading color-black"><?php the_title();?></h4>
                                        <div class="rating">
                                            <input type="radio" id="star10" name="rating" value="10"/><label
                                                    for="star10" title="Rocks!">5 stars</label>
                                            <input type="radio" id="star8" name="rating" value="8"/><label for="star8"  title="Pretty good">3
                                                stars</label>
                                            <input type="radio" id="star7" name="rating" value="7"/><label for="star7" title="Pretty good">2
                                                stars</label>
                                            <input type="radio" id="star3" name="rating" value="3"/><label for="star3" title="Kinda bad">3
                                                stars</label>
                                            <input type="radio" id="star1" name="rating" value="1"/><label for="star1"title="Sucks big time">1
                                                star</label>
                                        </div>
                                        <p class="paragraph-text color-black regular_font__Montserrat"
                                           style="text-align: justify;">
                                            <?php echo the_excerpt() ?>
                                        </p>
                                        <!--
										<div class="clients-detail">
                                            <a href="Happyclients.php" class="tbtn"
                                               style=" color: #0d768b; text-align: center;">read more</a>
                                        </div> 
										-->
                                        <!--client_word-->
                                    </div>
                                    <!--client_word_con wf-->
                                </div>
                            </div>
 <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>
                            

                        </div>

                    </div>

                    <!--testimonial_content-->

                </div>

                <!--container-->

            </div>


            <!--testimonial_con no_paddind col-lg-12-->

        </div>

        <!---------------------->

        <!--testimonialSection-->

        <!---------------------->


      <!------------------

        Logo Slider

        ------------------>

        <div class="no_padding logo_slider_con col-lg-12">

            <div class="container">

                <div class="wf logo_slider_inner">


                    <div class="large-12 columns col-lg-12">

                        <div class="owl-carousel owl-carousel-logo-slider owl-theme">



                  <?php
                // the query
                $wpb_all_query = new WP_Query(array('post_type'=>'post', 'cat'=> 8 , 'post_status'=>'publish', 'posts_per_page'=>10)); ?>
                <?php if ( $wpb_all_query->have_posts() ) : ?>
                    <!-- the loop -->
                    <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
                            <div class="item">

                                <div class="client_logo_con">

                                    <div class="client_logo_inner">



                                    <span>

                                    <?php the_post_thumbnail( array(150, 150));?>

                                    </span>

                                       



                                    </div>

                                    <!--client_logo_con-->

                                </div>

                                <!--item-->

                            </div>

            <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>     




                        </div>

                    </div>


                    <!--wf logo_slider_inner-->

                </div>

                <!--container-->

            </div>

            <!--col-lg-12-->

        </div>

        <!------------------

        Logo Slider

        ------------------>


    </main>


<?php
get_footer();
