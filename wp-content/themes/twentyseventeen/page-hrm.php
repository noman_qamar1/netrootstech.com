<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_template_part( 'template-parts/header/header', 'hrm' ); ?>

    <section class="slider" id="home" style="margin-top: 40px;">
        <div class="tp-banner">
            <ul>
                <li data-delay="7000" data-transition="fade" data-slotamount="7" data-masterspeed="2000">
                    <div class="elements">
                        <div class="tp-caption tp-resizeme lfb" data-x="0" data-y="65" data-speed="1000" data-start="2040" data-easing="Power4.easeOut" data-endspeed="500" data-endeasing="Power1.easeIn" style="z-index: 4">
                             <img src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/img/sections/slider/rs-slider1-img2.png" width="500" height="600" alt="" /> </div>

                        <div><h2 class="tp-caption tp-resizeme lft skewtotop title " data-x="502" data-y="161" data-speed="1000" data-start="1700" data-easing="Power4.easeOut" data-endspeed="500" data-endeasing="Power1.easeIn"><strong>Do Business, <br> Leave the HR to us </strong></h2></div>

                        <div class="tp-caption tp-resizeme lfr skewtoright description black" data-x="502" data-y="300" data-speed="1000" data-start="1500" data-easing="Power4.easeOut" data-endspeed="500" data-endeasing="Power1.easeIn" style="max-width: 600px">

                            <h4>Freeing You Up for What Matters – Fórsa HR Software for Human Resource Management </h4>
                        </div>
                        <div class="tp-caption tp-resizeme lfr skewtoright description black" data-x="502" data-y="385" data-speed="1000" data-start="1500" data-easing="Power4.easeOut" data-endspeed="500" data-endeasing="Power1.easeIn" style="max-width: 600px">

                            <p>Every penny counts, and so does every minute. Allocate less time and fewer resources to HR, and instead spend it on growing your business. With our revolutionary HR software, you get more for less. </p>

                        </div>
                    </div> 
                     <img src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/img/cropped-Savin-NY-Website-Background-Web.jpg" alt="" data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat" />
                 </li>
                <!-- Slide -->
                <!-- Slide -->

                <li data-delay="10300" data-transition="fade" data-slotamount="7" data-masterspeed="2000" class="slid2">
                    <div class="elements">

                        <h4 class="tp-caption tp-resizeme sft skewtotop title " data-x="15" data-y="191" data-speed="1000" data-start="1100" data-easing="Power4.easeOut" data-endspeed="400" data-endeasing="Power1.easeIn">Improve Employee Performance &                            Retention</h4>

                        <div class="tp-caption tp-resizeme lfl skewtoleft description col-xs-5" data-x="0" data-y="250" data-speed="1000" data-start="1000" data-easing="Power4.easeOut" data-endspeed="400" data-endeasing="Power1.easeIn" style="max-width: 520px;">

                            <p>From their first day to their last, Fórsa gives you the HRMS solutions you need to cultivate trust and give consistent feedback, ensuring that your employees are performing to their greatest potential.</p>

                        </div>

                        <div class="tp-caption tp-resizeme lfb skewtobottom customin" data-x="600" data-hoffset="150" data-y="170" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="1000" data-start="1000" data-easing="Power4.easeOut" data-end="14000" data-endspeed="500" data-endeasing="Power1.easeIn" style="z-index: 3">

                         <img src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/img/sections/slider/rs-slider2-img.png" width="500" height="315" alt="" />
                     </div>

                        <div class="tp-caption tp-resizeme customout" data-x="665" data-hoffset="-208" data-y="205" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:1;transformPerspective:600;transformOrigin:50% 50%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="1" data-start="1000" data-easing="Power0.easeOut" data-end="3800" data-endspeed="1" data-endeasing="Power0.easeIn" data-captionhidden="on" style="z-index: 4">

                         <img src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/img/Web-Development-Banner-1.jpg" width="372" height="232" alt="" />
                     </div>

                        <div class="tp-caption tp-resizeme customin customout" data-x="665" data-hoffset="-208" data-y="205" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:1;transformPerspective:600;transformOrigin:50% 50%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="1" data-start="3800" data-easing="Power0.easeOut" data-end="5300" data-endspeed="1" data-endeasing="Power0.easeIn" data-captionhidden="on" style="z-index: 4"> 
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/img/webdev.jpg" width="372" height="232" alt="" />
                        </div>

                        <div class="tp-caption tp-resizeme customin customout" data-x="665" data-hoffset="-208" data-y="205" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:1;transformPerspective:600;transformOrigin:50% 50%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="1" data-start="5300" data-easing="Power0.easeOut" data-end="6800" data-endspeed="1" data-endeasing="Power0.easeIn" data-captionhidden="on" style="z-index: 4">
                         <img src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/img/multi-platform-hr-software.jpg" width="372" height="232" alt="" /></div>

                        <div class="tp-caption tp-resizeme customin customout" data-x="665" data-hoffset="-208" data-y="205" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:1;transformPerspective:600;transformOrigin:50% 50%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="1" data-start="6800" data-easing="Power0.easeOut" data-end="14000" data-endspeed="1" data-endeasing="Power0.easeIn" data-captionhidden="on" style="z-index: 4">
                         <img src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/img/Web-Development-Banner-1.jpg" width="372" height="232" alt="" />
                     </div>

                    </div> 
                     <img src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/img/cropped-Savin-NY-Website-Background-Web.jpg" alt="" data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat" />
                 </li>

                <li data-delay="10300" data-transition="fade" data-slotamount="7" data-masterspeed="2000" class="slid2">
                    <div class="elements">

                        <h2 class="tp-caption tp-resizeme sft skewtotop title " data-x="50" data-y="100" data-speed="1000" data-start="1100" data-easing="Power4.easeOut" data-endspeed="400" data-endeasing="Power1.easeIn">360-Degree Overview Of Your Employees Their Performance</h2>

                        <div class="tp-caption tp-resizeme lfl skewtoleft description col-xs-5" data-x="100" data-y="285" data-speed="1000" data-start="1000" data-easing="Power4.easeOut" data-endspeed="400" data-endeasing="Power1.easeIn" style="max-width: 520px;">

                            <p>With a centralized database of employee information and performance evaluation module, know which employee is best suited to a particular task, due for a raise, and deserving of a promotion. As one of the best HRIS systems on the market, our HR software helps you capitalize on the strengths of your workforce.</p>

                        </div>

                        <div class="tp-caption tp-resizeme lfb skewtobottom customin" data-x="630" data-hoffset="150" data-y="170" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="1000" data-start="1000" data-easing="Power4.easeOut" data-end="14000" data-endspeed="500" data-endeasing="Power1.easeIn" style="z-index: 3">
                         <img src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/img/sections/slider/rs-slider2-img.png" width="500" height="315" alt="" />
                     </div>

                        <div class="tp-caption tp-resizeme customout" data-x="695" data-hoffset="-208" data-y="205" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:1;transformPerspective:600;transformOrigin:50% 50%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="1" data-start="1000" data-easing="Power0.easeOut" data-end="3800" data-endspeed="1" data-endeasing="Power0.easeIn" data-captionhidden="on" style="z-index: 4">

                         <img src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/img/HRM-WEB.png" width="372" height="500" alt="" />
                     </div>

                        <div class="tp-caption tp-resizeme customin customout" data-x="695" data-hoffset="-208" data-y="205" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:1;transformPerspective:600;transformOrigin:50% 50%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="1" data-start="3800" data-easing="Power0.easeOut" data-end="5300" data-endspeed="1" data-endeasing="Power0.easeIn" data-captionhidden="on" style="z-index: 4">
                         <img src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/img/2019-04-01_1518.png" width="372" height="500" alt="" />
                          </div>

                        <div class="tp-caption tp-resizeme customin customout" data-x="695" data-hoffset="-208" data-y="205" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:1;transformPerspective:600;transformOrigin:50% 50%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="1" data-start="5300" data-easing="Power0.easeOut" data-end="6800" data-endspeed="1" data-endeasing="Power0.easeIn" data-captionhidden="on" style="z-index: 4"> 
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/img/multi-platform-hr-software.jpg" width="372" height="232" alt="" />
                    </div>

                        <div class="tp-caption tp-resizeme customin customout" data-x="695" data-hoffset="-208" data-y="205" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:1;transformPerspective:600;transformOrigin:50% 50%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="1" data-start="6800" data-easing="Power0.easeOut" data-end="14000" data-endspeed="1" data-endeasing="Power0.easeIn" data-captionhidden="on" style="z-index: 4">
                         <img src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/img/Web-Development-Banner-1.jpg" width="372" height="232" alt="" />
                     </div>

                    </div>  
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/img/cropped-Savin-NY-Website-Background-Web.jpg" alt="" data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat" />
                </li>
                <li data-delay="7000" data-transition="fade" data-slotamount="7" data-masterspeed="2000">

                    <div class="elements">

                        <h2 class="tp-caption tp-resizeme sft skewtotop title " data-x="650" data-y="150" data-speed="1000" data-start="500" data-easing="Power4.easeOut" data-endspeed="400" data-endeasing="Power1.easeIn">Take a look at our dashboard</h2>

                        <div class="tp-caption tp-resizeme lfr skewtoright description text-center" data-x="650" data-y="280" data-speed="1000" data-start="800" data-easing="Power4.easeOut" data-endspeed="500" data-endeasing="Power1.easeIn" style="max-width: 600px">

                            <p>If you’re wondering why Fórsa HR software ranks among the top HRIS systems, it’s because of our dashboard!</p>

                        </div>

                        <div class="tp-caption tp-resizeme customin customout" data-x="0" data-y="10" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="1200" data-start="1000" data-easing="Power3.easeInOut" data-endspeed="300" style="z-index: 5">
                         <img src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/img/Mockup.png" width="700" height="300" alt="" />
                     </div>

                    </div> 
                     <img src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/img/cropped-Savin-NY-Website-Background-Web.jpg" alt="" data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat" />
                 </li>

                <!-- Slide -->
            </ul>
            <div class="tp-bannertimer"></div>
        </div>
    </section>
    <!-- slider -->
    
        <section id="Modules" class="page-section">
        <div class="container">
            <div class="section-title" data-animation="fadeInUp">
                <h1 class="title">Fórsa HR Software: <small style="color: #4b98a7;">Freeing you up for what matters</small></h1>
                <p align="center"></p>
            </div>
            <div class="row">
                <div class="col-md-12 text-center" data-animation="fadeInUp">
                    <!-- Text -->
                    <p class="title-description" style="color: black">Save time, lower operational costs, and revolutionize employee management with Fórsa HRMS solutions. Our HR software enables businesses to organize and consolidate their HR processes within a simple, yet thorough, HR system. Among other modules, our HR software comprises one of the best HRIS systems, or centralized database of employee information. While Fórsa HRMS automates and streamlines virtually all HR processes, what distinguishes our HR software as being among the most effective HRIS solutions is its simple, accessible, and comprehensive Dashboard. Ranking among the top HRIS systems, Fórsa HRMS solutions promise to not only fulfill, but exceed your expectations. </p>
                </div>
            </div>
            <div class="row special-feature row-centered">
                <!-- Special Feature Box 1 -->
                <div class="col-md-3 col-sm-6" data-animation="fadeInUp">
                    <div class="s-feature-box text-center">
                        <div class="mask-top">
                            <!-- Icon --><i class="fas fa-database" aria-hidden="true"></i>
                            <!-- Title -->
                            <h4>Employee Database</h4></div>
                        <a href="<?php echo get_page_link(449); ?>">
                            <div class="mask-bottom">
                                <!-- Icon --><i class="fas fa-database" aria-hidden="true"></i>
                                <!-- Title -->
                                <h4>Employee Database</h4>
                                <!-- Text -->
                                <p>For recording all employee information. </p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 " data-animation="fadeInDown">
                    <div class="s-feature-box text-center">
                        <div class="mask-top">
                            <!-- Icon --><i class="fas fa-refresh" aria-hidden="true"></i>
                            <!-- Title -->
                            <h4>Recruitment</h4></div>
                        <a href="<?php echo get_page_link(457); ?>">
                            <div class="mask-bottom">
                                <!-- Icon --><i class="fas fa-refresh" aria-hidden="true"></i>
                                <!-- Title -->
                                <h4>Recruitment</h4>
                                <!-- Text -->
                                <p>For recruiting and on-boarding new employees</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 " data-animation="fadeInUp">
                    <div class="s-feature-box text-center">
                        <div class="mask-top">
                            <!-- Icon --><i class="far fa-file" aria-hidden="true"></i>
                            <!-- Title -->
                            <h4>Documents Manager</h4></div>
                        <a href="<?php echo get_page_link(447); ?>">
                            <div class="mask-bottom">
                                <!-- Icon --><i class="far fa-file" aria-hidden="true"></i>
                                <!-- Title -->
                                <h4>Documents Manager </h4>
                                <!-- Text -->
                                <p>For storing and retrieving HR documents</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6" data-animation="fadeInUp">
                    <div class="s-feature-box text-center">
                        <div class="mask-top">
                            <!-- Icon --><i class="fas fa-calendar" aria-hidden="true"></i>
                            <!-- Title -->
                            <h4>Time & Attendance</h4></div>
                        <a href="<?php echo get_page_link(444); ?>">
                            <div class="mask-bottom">
                                <!-- Icon --><i class="fas fa-calendar" aria-hidden="true"></i>
                                <!-- Title -->
                                <h4>Time & Attendance</h4>
                                <!-- Text -->
                                <p> For tracking employee attendance and days off </p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6" data-animation="fadeInRight">
                    <div class="s-feature-box text-center">
                        <div class="mask-top">
                            <!-- Icon --><i class="fas fa-money" aria-hidden="true"></i>
                            <!-- Title -->
                            <h4>Payroll Management </h4></div>
                        <a href="<?php echo get_page_link(453); ?>">
                            <div class="mask-bottom">
                                <!-- Icon --><i class="fas fa-money" aria-hidden="true"></i>
                                <!-- Title -->
                                <h4>Payroll Manager</h4>
                                <!-- Text -->
                                <p> All payroll functions rolled into a single module</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 " data-animation="fadeInUp">
                    <div class="s-feature-box text-center">
                        <div class="mask-top">
                            <!-- Icon --><i class="far fa-window-maximize" aria-hidden="true"></i>
                            <!-- Title -->
                            <h4>Training & Development </h4></div>
                        <a href="<?php echo get_page_link(459); ?>">
                            <div class="mask-bottom">
                                <!-- Icon --><i class="far fa-window-maximize" aria-hidden="true"></i>
                                <!-- Title -->
                                <h4>Training & Development </h4>
                                <!-- Text -->
                                <p>For simplifying and enhancing training and development functions</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 " data-animation="fadeInLeft">
                    <div class="s-feature-box text-center">
                        <div class="mask-top">
                            <!-- Icon --><i class="fas fa-pie-chart" aria-hidden="true"></i>
                            <!-- Title -->
                            <h4>Performance Evaluation</h4></div>
                        <a href="<?php echo get_page_link(455); ?>">
                            <div class="mask-bottom">
                                <!-- Icon --><i class="fas fa-pie-chart" aria-hidden="true"></i>
                                <!-- Title -->
                                <h4>Performance Evaluation</h4>
                                <!-- Text -->
                                <p> For evaluating and recording employee performance</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6" data-animation="fadeInUp">
                    <div class="s-feature-box text-center">
                        <div class="mask-top">
                            <!-- Icon --><i class="fas fa-door-open" aria-hidden="true"></i>
                            <!-- Title -->
                            <h4>Exit Management </h4></div>
                        <a href="<?php echo get_page_link(451); ?>">
                            <div class="mask-bottom">
                                <!-- Icon --><i class="fas fa-door-open" aria-hidden="true"></i>
                                <!-- Title -->
                                <h4>Exit Management</h4>
                                <!-- Text -->
                                <p> For processing and recording employee resignations .</p>
                            </div>
                        </a>
                    </div>
                </div>
                <!-- Special Feature Box 3 -->
            </div>
        </div>
    </section>
    <!-- about-us -->
    <section id="Ourphilosophy" class="page-section no-pad light-bg border-tb">
        <div class="container who-we-are">
            <div class="row">
                <div class="col-md-6" data-animation="fadeInLeft">
                    <div class="section-title text-left" data-animation="fadeInUp">
                        <!-- Title -->
                        <h1 class="title">Who We Are</h1></div>
                    <div class="owl-carousel pagination-1 dark-switch" data-pagination="true" data-autoplay="true" data-navigation="false" data-singleitem="true" data-animation="fadeInUp">
                        <div class="item">
                            <!-- Heading -->
                            <h class="entry-title"> Our Philosophy</h>
                            <!-- Content -->
                            <div class="entry-content">
                                <p>Hello from the NetRoots family!
                                    <br>Here at NetRoots, we believe in creating simple but innovative human-centered technological solutions, with a focus on ease of use. We believe that maximizing utility and simplicity do not necessitate compromising on design and beauty. Thus we have worked to create HR software that is simple, practical, and beautiful. </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 no-pad text-center" data-animation="fadeInRight"> <img src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/img/hrm-(2).png" alt="" class="img-fluid" style="width:434px  ; padding: 20px; margin-top: 20px"> </div>
            </div>
        </div>
    </section>
    <section id="whyus" class="page-section">
        <div class="container">
            <div class="section-title" data-animation="fadeInUp">
                <!-- Heading -->
                <h1 class="title">Why Choose Us</h1></div>
            <div class="row">
                <div class="item-box icons-color col-sm-6 col-md-4 hvr-glow" data-animation="fadeInDown">
                    <a>
                        <!-- Icon --><i class="fas fa-magic fa-2x" aria-hidden="true" ></i>
                        <!-- Title -->
                        <h5 class="title">Affordable</h5>
                        <!-- Text -->
                        <div>Get more for less <span style="color: #d8d8d852;visibility: hidden">Get more for less Get more for less Get more for less Get more for less Get more for less</span> </div>
                    </a>
                </div>
                <div class="item-box icons-color col-sm-6 col-md-4 hvr-glow" data-animation="fadeInRight">
                    <a>
                        <!-- Icon --><i class="fas fa-history fa-2x"></i>
                        <h5 class="title">                            Round-the-clock support</h5>
                        <!-- Text -->
                        <div>Call us any time for help with our HR system.<span style="color: #d8d8d852;visibility: hidden">Get more for less Get more for less Get more for less Get more</span> </div>
                    </a>
                </div>
                <div class="item-box icons-color col-sm-6 col-md-4 hvr-glow" data-animation="fadeInLeft">
                    <a>
                        <!-- Icon --><i class="fas fa-mobile fa-2x"></i>
                        <!-- Title -->
                        <h5 class="title">Responsive</h5>
                        <!-- Text -->
                        <div>Use it on your cellphone, tablet, or computer . <span style="color: #d8d8d852;visibility: hidden">Get more for less Get more for less Get more for less Get more for</span> </div>
                    </a>
                </div>
                <div class="item-box icons-color col-sm-6 col-md-4 hvr-glow" data-animation="fadeInLeft">
                    <a>
                        <i class="fas fa-user-cog fa-2x"></i>
                        <!-- Title -->
                        <h5 class="title">Ease of use</h5>
                        <!-- Text -->
                        <div>Make your life easy with the system’s simple interface.<span style="color: #d8d8d852;visibility: hidden">Get more for less Get more for less Get more for less</span> </div>
                    </a>
                </div>
                <div class="item-box icons-color col-sm-6 col-md-4 hvr-glow" data-animation="fadeInUp">
                    <a>
                        <!-- Icon --><i class="fab fa-intercom fa-2x"></i>
                        <!-- Title -->
                        <h5 class="title">Customizable</h5>
                        <!-- Text -->
                        <div>Add features/modules to meet your specific business needs.
							<span style="color:#d8d8d852;visibility: hidden">Get more for less Get more for less Get more for less</span> </div>
                    </a>
                </div>
                <div class="item-box icons-color col-sm-6 col-md-4 hvr-glow" data-animation="fadeInRight">
                    <a>
                        <!-- Icon --><i class="fas fa-tachometer-alt fa-2x"></i>
                        <!-- Title -->
                        <h5 class="title">Increased collaboration</h5>
                        <!-- Text -->
                        <div>Collaborate with your team members with a centralized HRMS .
							<span style="color: #d8d8d852;visibility: hidden">Get more for less Get more for less Get more for less</span> </div>
                    </a>
                </div>
                <div class="item-box icons-color col-sm-6 col-md-4 hvr-glow" data-animation="fadeInLeft">
                    <a>
                        <!-- Icon --><i class="fas fa-plug fa-2x"></i>
                        <!-- Title -->
                        <h5 class="title">Overview of employee qualities </h5>
                        <!-- Text -->
                        <div>Make better decisions about promotions and work assignment through an overview of employee skills and performance. </div>
                    </a>
                </div>
                <div class="item-box icons-color col-sm-6 col-md-4 hvr-glow" data-animation="fadeInUp">
                    <a> <i class="fas fa-adjust fa-2x"></i>
                        <h5 class="title">Multi Dashboard</h5>
                        <div style="margin-bottom: 45px;">Choose from among a range of colors to configure your system’s aesthetics according to your preferences. </div>
                    </a>
                </div>
                <div class="item-box icons-color col-sm-6 col-md-4 hvr-glow" data-animation="fadeInRight">
                    <a> <i class="fas fa-refresh text-color fa-2x" aria-hidden="true"></i>
                        <h5 class="title">                            updates</h5>
                        <div id="fellowus" style="margin-bottom: 23px;"> Receive frequent updates so that your HR system keeps running smoothly and improves consistently to remain its best version possible. </div>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section class="try-it page-section light-bg" style="padding: 0!important;">
        <div class="image-bg content-in fixed" data-background="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/img/CH-Email-footer-image-final.jpg">
            <div class="overlay-half-dark"></div>
        </div>
        <div class="twitter-feed">
            <div class="container" data-animation="fadeInUp">
                <div class="row">
                    <div class="col-md-12 text-center icons-circle icons-bg-color fa-1x">
                        <h3 style="color: white" class="text-capitalize inline-block tb-margin-20 black" data-animation="fadeInUp">                            Its <span style="color: black;">simplicity & adaptability</span> make Fórsa HR software                            the best HRMS system out there.</h3>
                        <div class="inline-block lr-pad-20"><a style="color: black" href="<?php echo get_page_link(47); ?>" class="btn btn-transparent-black btn-lg" data-animation="fadeInDown">Try it now</a></div>
                    </div>
                </div>
            </div>
        </div></section>


<!-- ****************************************************************** -->


    <section class="page-section testimonails" style="background-color: #f2f2f2;">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <div class="section-title text-left" data-animation="fadeInLeft">
                        <!-- Heading --> <h2 class="title">Latest Posts</h2></div>
                    <ul class="latest-posts">

            <?php
            // the query
            $wpb_all_query = new WP_Query(array('post_type'=>'post', 'cat'=> 23 , 'post_status'=>'publish', 'posts_per_page'=>5)); ?>
            <?php if ( $wpb_all_query->have_posts() ) : ?>
                <!-- the loop -->
                <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>

                        <li data-animation="fadeInLeft">
                            <div class="post-thumb">
                             
                             <?php the_post_thumbnail( array(85,85) );?>

                            </div>

                            <style>
                                .description a {text-decoration: underline; }
                                .description a:hover {text-decoration: underline;color: #00094d;}
                            </style>

                            <div class="post-details">
                                <div class="description"><a href="<?php echo get_page_link(488); ?>">
                                <?php the_title();?>

                                </a>
                                    </div>

                                <div class="meta">
                                <span class="time">
                                    <i class="fa fa-calendar"></i>
                                    <?php the_date( 'd-m-Y'); ?>
                                </span></div>
                            </div>
                        </li>


            <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>

                    </ul>
                </div>

                <!-- testemonials start from here -->

                <div class="col-sm-12 col-md-6 testimonails">
                    <div class="section-title text-left" data-animation="fadeInRight">
                        <!-- Heading --> <h2 class="title">Testimonials</h2>
                    </div>
                    <div class="owl-carousel pagination-1 dark-switch" data-effect="backSlide" data-pagination="true"
                         data-autoplay="true" data-navigation="false" data-singleitem="true"
                         data-animation="fadeInRight">

            <?php
            // the query
            $wpb_all_query = new WP_Query(array('post_type'=>'post', 'cat'=> 7 , 'post_status'=>'publish','order'=>'asc', 'posts_per_page'=>3)); ?>
            <?php if ( $wpb_all_query->have_posts() ) : ?>
                <!-- the loop -->
                <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>

                        <div class="item">
                            <div class="desc-border bottom-arrow quote">
                                <blockquote class="small-text">
                                  <?php echo the_content(); ?>
                                </blockquote>
                                <div class="star-rating text-right"><i class="fa fa-star text-color"></i> <i
                                        class="fa fa-star text-color"></i> <i class="fa fa-star text-color"></i> <i
                                        class="fa fa-star text-color"></i> <i
                                        class="fa fa-star-half-o text-color"></i></div>
                            </div>
                            <div class="client-details text-center">
                                <div class="client-image">      
                             <div>
								 <?php the_post_thumbnail( array(80, 80), array('class' => 'img-circle'));?>
									</div>
                                    </div>

                                <div class="client-details">
                                    <strong class="text-color">
                                        <?php the_title();?>
                                        </strong>
                                </div>
                            </div>
                        </div>

            <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>

                        
                    </div>
                </div>
            </div>
        </div>
    </section>

       <section id="clients" class="page-section tb-pad-30">
        <div class="container">
            <div class="section-title text-center" data-animation="fadeInUp">
                <h1 class="title">Our Best Clients</h1></div>
            <div class="row">
                <div class="col-md-12 text-center" data-animation="fadeInDown">
                    <div class="owl-carousel navigation-1" data-pagination="false" data-items="6" data-autoplay="true"
                         data-navigation="true">


                         <?php
                // the query
                $wpb_all_query = new WP_Query(array('post_type'=>'post', 'cat'=> 8 , 'post_status'=>'publish', 'posts_per_page'=>10)); ?>
                <?php if ( $wpb_all_query->have_posts() ) : ?>
                    <!-- the loop -->
                    <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
             <a style="margin:10px;">
             <?php the_post_thumbnail( array(150, 150));?>
                           
                 </a> 

          <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?> 


                 </div>
                </div>
            </div>
        </div>
    </section>    <!--testimonials-->


<div id="get-quote" class="bg-color get-a-quote black text-center">
    <div class="container animated pulse visible" data-animation="pulse">
        <div class="row">
            <div class="col-md-12"><p>Get A Free Quote / Need a Help ? <a class="black" href="<?php echo get_page_link(47); ?>">Contact
                        Us</a></p></div>
        </div>
    </div>
</div>

<script>
	AOS.init();
	</script>
<?php 
get_footer();
