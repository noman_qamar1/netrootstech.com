<?php
/*
Template Name: erp inventory
Template Post Type:  page
*/
// Page code here...

get_template_part( 'template-parts/header/header', 'erp' ); ?>

    <head>

        <link href="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/css/responsive.css" rel="stylesheet" type="text/css">
        <link href="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/css/stylesheet.css" rel="stylesheet" type="text/css">
        <link href="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/css/erp-sales-module.css" rel="stylesheet" type="text/css">
        <link href="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/css/owl.carousel.min.css" rel="stylesheet">
        <link href="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/css/owl.theme.default.min.css" rel="stylesheet">
        <link href="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/animate.css-master/animate.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
              integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link href="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/js/bootstrap.min.js"></script>
        <style>
            .navbar-default {
                background-color: white;
                border-color: #1c7992;
            }
            .banner{
                padding: 100px 0;
            }
            .banner h1{
                font-size:80px;
            }
            .banner h2{
                font-size:70px;
            }
            .navbar-default .navbar-nav > li > a {
                color: black;
            }

            .navbar-nav > li > a {
                font-size: 16px;
            }

            .navbar {
                min-height: 50px;
                margin-bottom: 0;
                border: 1px solid transparent;
                padding-top: 10px;
                padding-bottom: 10px;
                top: 0;
                position: sticky;
                z-index: 1;
                box-shadow: 0px 1px 10px #999;
                border-radius: 0;
                height: 123px;
                border-bottom: 2px solid #007488;
                width: 100%;
            }

            header {
                width: 100%;
                padding: 0 0;
            }

            .btn-warning {
                color: #fff;
                background-color: #FF9800;
                border-color: #FF9800;
                margin-top: 8px;
            }

            .navbar-default .navbar-nav > li > a:hover {
                color: #007488;
            }    </style>
    </head>
    <!-- ------------------------------------------------template body start from here -->

    <div class="top_banner_inventory">
        <div class="container">




            <div class="row">
                <div class="col-md-12">

                    <div class="banner_content">
                        <h3>LOG STOCK INVENTORY AND GOODS ISSUED/RECEIVED</h3>
                        <p>Log stock inventory and goods issued and received to enable efficient</p>
                        <p>production planning through the inventory module.As purchases end up in</p>
                        <p>the inventory,and stock from the inventory is sold through the sales</p>
                        <p>module, this module is tightly integrated with other modules.</p>




                    </div>
                </div>

            </div>



        </div>





    </div>
    <!--banner end-->

    <!--flavour start-->
    <div class="flavour">
        <h3>Key Components</h3>
        <div class="container">
            <div class="row">


                <div class="tabs">

                    <input type="radio" id="tab1" name="tab-control" checked>
                    <input type="radio" id="tab2" name="tab-control">
                    <input type="radio" id="tab3" name="tab-control">
                    <input type="radio" id="tab4" name="tab-control">
                    <ul>
                        <li title="Features"><label for="tab1" role="button"><svg viewBox="0 0 24 24"><path d="M14,2A8,8 0 0,0 6,10A8,8 0 0,0 14,18A8,8 0 0,0 22,10H20C20,13.32 17.32,16 14,16A6,6 0 0,1 8,10A6,6 0 0,1 14,4C14.43,4 14.86,4.05 15.27,4.14L16.88,2.54C15.96,2.18 15,2 14,2M20.59,3.58L14,10.17L11.62,7.79L10.21,9.21L14,13L22,5M4.93,5.82C3.08,7.34 2,9.61 2,12A8,8 0 0,0 10,20C10.64,20 11.27,19.92 11.88,19.77C10.12,19.38 8.5,18.5 7.17,17.29C5.22,16.25 4,14.21 4,12C4,11.7 4.03,11.41 4.07,11.11C4.03,10.74 4,10.37 4,10C4,8.56 4.32,7.13 4.93,5.82Z"/>
                                </svg><br><span>Features</span></label></li>
                        <li title="Delivery Contents"><label for="tab2" role="button"><svg viewBox="0 0 24 24"><path d="M14,2A8,8 0 0,0 6,10A8,8 0 0,0 14,18A8,8 0 0,0 22,10H20C20,13.32 17.32,16 14,16A6,6 0 0,1 8,10A6,6 0 0,1 14,4C14.43,4 14.86,4.05 15.27,4.14L16.88,2.54C15.96,2.18 15,2 14,2M20.59,3.58L14,10.17L11.62,7.79L10.21,9.21L14,13L22,5M4.93,5.82C3.08,7.34 2,9.61 2,12A8,8 0 0,0 10,20C10.64,20 11.27,19.92 11.88,19.77C10.12,19.38 8.5,18.5 7.17,17.29C5.22,16.25 4,14.21 4,12C4,11.7 4.03,11.41 4.07,11.11C4.03,10.74 4,10.37 4,10C4,8.56 4.32,7.13 4.93,5.82Z"/>
                                </svg><br><span>Delivery Contents</span></label></li>
                        <li title="Shipping"><label for="tab3" role="button"><svg viewBox="0 0 24 24">
                                    <path d="M14,2A8,8 0 0,0 6,10A8,8 0 0,0 14,18A8,8 0 0,0 22,10H20C20,13.32 17.32,16 14,16A6,6 0 0,1 8,10A6,6 0 0,1 14,4C14.43,4 14.86,4.05 15.27,4.14L16.88,2.54C15.96,2.18 15,2 14,2M20.59,3.58L14,10.17L11.62,7.79L10.21,9.21L14,13L22,5M4.93,5.82C3.08,7.34 2,9.61 2,12A8,8 0 0,0 10,20C10.64,20 11.27,19.92 11.88,19.77C10.12,19.38 8.5,18.5 7.17,17.29C5.22,16.25 4,14.21 4,12C4,11.7 4.03,11.41 4.07,11.11C4.03,10.74 4,10.37 4,10C4,8.56 4.32,7.13 4.93,5.82Z"/>
                                </svg><br><span>Shipping</span></label></li>    <li title="Returns"><label for="tab4" role="button"><svg viewBox="0 0 24 24">
                                    <path d="M14,2A8,8 0 0,0 6,10A8,8 0 0,0 14,18A8,8 0 0,0 22,10H20C20,13.32 17.32,16 14,16A6,6 0 0,1 8,10A6,6 0 0,1 14,4C14.43,4 14.86,4.05 15.27,4.14L16.88,2.54C15.96,2.18 15,2 14,2M20.59,3.58L14,10.17L11.62,7.79L10.21,9.21L14,13L22,5M4.93,5.82C3.08,7.34 2,9.61 2,12A8,8 0 0,0 10,20C10.64,20 11.27,19.92 11.88,19.77C10.12,19.38 8.5,18.5 7.17,17.29C5.22,16.25 4,14.21 4,12C4,11.7 4.03,11.41 4.07,11.11C4.03,10.74 4,10.37 4,10C4,8.56 4.32,7.13 4.93,5.82Z"/>
                                </svg><br><span>Returns</span></label></li>
                    </ul>

                    <div class="slider_module"><div class="indicator"></div></div>
                    <div class="content">
                        <section>
                            <h2>Features</h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea dolorem sequi, quo tempore in eum obcaecati atque quibusdam officiis est dolorum minima deleniti ratione molestias numquam. Voluptas voluptates quibusdam cum?</section>
                        <section>
                            <h2>Delivery Contents</h2>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem quas adipisci a accusantium eius ut voluptatibus ad impedit nulla, ipsa qui. Quasi temporibus eos commodi aliquid impedit amet, similique nulla.</section>
                        <section>
                            <h2>Shipping</h2>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam nemo ducimus eius, magnam error quisquam sunt voluptate labore, excepturi numquam! Alias libero optio sed harum debitis! Veniam, quia in eum.</section>
                        <section>
                            <h2>Returns</h2>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa dicta vero rerum? Eaque repudiandae architecto libero reprehenderit aliquam magnam ratione quidem? Nobis doloribus molestiae enim deserunt necessitatibus eaque quidem incidunt.</section>
                    </div>
                </div>


            </div>

        </div>
    </div>

    <!--flavour end-->


















    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="assets/js/owl.carousel.min.js"></script>
    <script src="assets/js/owl.carousel.js"></script>






    <script>
        $(document).ready(function() {

            (function ($) {
                $('.tab ul.tabs').addClass('active').find('> li:eq(0)').addClass('current');

                $('.tab ul.tabs li a').click(function (g) {
                    var tab = $(this).closest('.tab'),
                        index = $(this).closest('li').index();

                    tab.find('ul.tabs > li').removeClass('current');
                    $(this).closest('li').addClass('current');

                    tab.find('.tab_content').find('div.tabs_item').not('div.tabs_item:eq(' + index + ')').slideUp();
                    tab.find('.tab_content').find('div.tabs_item:eq(' + index + ')').slideDown();

                    g.preventDefault();
                } );
            })(jQuery);

        });
    </script>




    <!-- ----------------------------------------------template end here -->

    <script>
        AOS.init();
    </script>
<?php
get_footer();