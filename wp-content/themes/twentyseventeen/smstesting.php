<body id="page" cz-shortcut-listen="true"><!---->


<div class="sticky-container">
    <div class="sticky-container">    <style>

    .okewa-style_3.okewa-text_3 #okewa-floating_cta{

        bottom: 55px;
        right: 13px;
    }
    .okewa-pulse_3{
        bottom: 55px !important;
    }
</style>
<ul class="sticky">

        <li>
            <img src="https://www.netrootstech.com///assets/<?php echo get_template_directory_uri(); ?>/assets/pms-assets/img/ireland.png" width="32" height="32">
            <p><a href="tel:+353578601255" target="_blank">+353 (57) 8601255</a></p>
</li>
<li>
    <img src="https://www.netrootstech.com///assets/<?php echo get_template_directory_uri(); ?>/assets/pms-assets/img/canada.png" width="32" height="32">
    <p><a href="tel:+16474906750" target="_blank">+1 (647) 490 6750</a></p>
</li>
<li>
    <img src="https://www.netrootstech.com///assets/<?php echo get_template_directory_uri(); ?>/assets/pms-assets/img/usa.png" width="32" height="32">
    <p><a href="tel:+13023001742" target="_blank">+1 (302) 300 1742</a></p>
</li> <li>
    <img src="https://www.netrootstech.com///assets/<?php echo get_template_directory_uri(); ?>/assets/pms-assets/img/pakistan.png" width="32" height="32">
    <p><a href="tel:+924236287770" target="_blank">+92 42 36287770</a></p>
</li>
</ul>
        <style>        .sticky-number {
                color: black;
                text-decoration: underline;
            }    </style>
    </div>
    <style>        .sticky-number {
            color: black;
            text-decoration: underline;
        }    </style>
</div>


<header>
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class=""></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2" class=""></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <!-- Slide One - Set the background image for this slide in the line below -->
                <div class="carousel-item" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/pms-assets/img/cropped-Savin-NY-Website-Background-Web.jpg');">
                    <div class="carousel-caption d-none d-md-block">
                        <div class="first"><h1 class="display-4" style="color: black;font-size: 60px;font-weight: bold">
                                NRT Property Management System</h1>
                            <h3 style="color: black">Automate and simplify residential and commercial property
                                management </h3></div>
                    </div>
                </div>            <!-- Slide Two - Set the background image for this slide in the line below -->
                <div class="carousel-item active" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/pms-assets/img/Property-Management2.jpg')">
                    <div class="carousel-caption d-none d-md-block"><h1 class="display-4 " style="text-align: left ;margin-left: 50px;color: black;font-size: 60px;">
                            Digitize your information</h1>                    <h4 style="text-align: left ;margin-left: 50px;color: black">Create a centralized database
                            of all your properties, property owners, and tenants </h4></div>
                </div>            <!-- Slide Three - Set the background image for this slide in the line below -->
                <div class="carousel-item" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/pms-assets/img/hero-accounting-hands.jpg')">
                    <div class="carousel-caption d-none d-md-block"><h1 class="display-4" style="text-align: left ;margin-left: 50px; color:black;font-size: 60px;font-weight: bold">
                            <span style="background-color: #ffffff4f">Manage your accounts</span></h1>
                        <h4 style="text-align: left ;margin-left: 50px;color: black;padding-bottom: 100px;"><span style="background-color: #ffffff4f">Keep track of your commissions and expenses</span>
                        </h4></div>
                </div>
            </div>
            <span id="Modules"> </span> <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span>
            </a> <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span>
            </a></div>
    </header>
    <section style="    padding-top: 20px;padding-bottom: 20px">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading text-uppercase">NETROOTS PMS -
                        <small>We rekindle your love for property management</small>
                    </h2>
                    <h3 class="section-subheading text-muted" style="font-style: normal">NetRoots PMS simplifies
                        residential and commercial property management so that you can stop worrying about the tedious
                        tasks and find enjoyment in your work. With information about property, property owners,
                        tenants, and support staff as well as contracts and complaints all digitized, you can stop
                        worrying about files and folders and instead focus on making deals.</h3></div>
            </div>
            <div class="row text-center">
                <div class="col-md-3 hvr-grow-shadow pt-3 pb-3 animated fadeInUp visible" data-animation="fadeInUp">
                    <span class="fa-stack fa-4x">            <i class="fas fa-circle fa-stack-2x text-primary"></i>            <i class="fas fa-tachometer-alt fa-stack-1x fa-inverse"></i>          </span>
                    <h4 class="service-heading">Dashboard</h4>
                    <p class="text-muted">An executive summary of the property, people, and processes relevant to your
                        property management business, contracts, properties, tenants and the owners of the properties
                        under your care. </p>
                    <!--                <button type="button" class="btnss btn btn-dark">Read More</button>-->
                </div>
                <div class=" col-md-3 hvr-grow-shadow pt-3 pb-3"><span class="fa-stack fa-4x">            <i class="fas fa-circle fa-stack-2x text-primary"></i>            <i class="far fa-user fa-stack-1x fa-inverse"></i>          </span>                <h4 class=" service-heading">Property Owners</h4>
                    <p class="text-muted">A log of each of the pieces of property you are managing, along with its
                        owner’s name, its type (whether for sale or for rent) and address, and its status (whether it is
                        currently being managed or was previously managed). </p>
                    <!--                <button type="button" class="btnss btn btn-dark">Read More</button>-->
                </div>
                <div class=" col-md-3 hvr-grow-shadow pt-3 pb-3"><span class="fa-stack fa-4x">            <i class="fas fa-circle fa-stack-2x text-primary"></i>            <i class="fas fa-map-marked-alt fa-stack-1x fa-inverse"></i>          </span>
                    <h4 class=" service-heading">Property</h4>
                    <p class="text-muted">A record of all property owners that have entrusted their property to you,
                        listed with their names, contact information, social security numbers, bank account numbers, and
                        active/inactive status. </p>
                    <!--                <button type="button" class="btnss btn btn-dark">Read More</button>-->
                </div>
                <div class=" col-md-3 hvr-grow-shadow pt-3 pb-3"><span class="fa-stack fa-4x">            <i class="fas fa-circle fa-stack-2x text-primary"></i>            <i class="fas fa-male fa-stack-1x fa-inverse"></i>          </span>                <h4 class="service-heading">Tenants</h4>
                    <p class="text-muted">A list of tenants renting the properties under your care, along with their
                        names, contact information, social security numbers, bank account numbers, and active/inactive
                        status.</p>
                    <!--                <button type="button" class="btnss btn btn-dark">Read More</button>-->
                </div>
                <div class=" col-md-3 hvr-grow-shadow pt-3 pb-3 mt-3"><span class="fa-stack fa-4x">            <i class="fas fa-circle fa-stack-2x text-primary"></i>            <i class="fas fa-people-carry fa-stack-1x fa-inverse"></i>          </span>
                    <h4 class="service-heading">Support Staff</h4>
                    <p class="text-muted">A database of all support staff that you have hired to resolve tenants’
                        complaints about the rented property, listed with their contact information, skills, and
                        active/inactive status. </p>
                    <!--                <button type="button" class="btnss btn btn-dark">Read More</button>-->
                </div>
                <div class=" col-md-3 hvr-grow-shadow pt-3 pb-3 mt-3"><span class="fa-stack fa-4x">            <i class="fas fa-circle fa-stack-2x text-primary"></i>            <i class="fas fa-address-card fa-stack-1x fa-inverse"></i>          </span>
                    <h4 class="service-heading">Contracts</h4>
                    <p class="text-muted">A store of all active and inactive contracts concluded between you and your
                        clients, including the names of the tenants/buyers and assets, end and start dates, amount of
                        security deposited, and the commission you received.</p>
                    <!--                <button type="button" class="btnss btn btn-dark">Read More</button>-->
                </div>
                <div class=" col-md-3 hvr-grow-shadow pt-3 pb-3 mt-3"><span class="fa-stack fa-4x">            <i class="fas fa-circle fa-stack-2x text-primary"></i>            <i class="fas fa-clipboard-list fa-stack-1x fa-inverse"></i>          </span>
                    <h4 class=" service-heading">Complaints</h4>
                    <p class="text-muted">A log of all complaints submitted by tenants through the system, along with
                        the names and addresses of the property, the dates of complaint, the members of support staff to
                        whom the complaints were assigned, and their status (whether open, resolved, or in
                        progress).</p>
                    <!--                <button type="button" class="btnss btn btn-dark">Read More</button>-->
                </div>
                <div class=" col-md-3 hvr-grow-shadow pt-3 pb-3 mt-3"><span class="fa-stack fa-4x">            <i class="fas fa-circle fa-stack-2x text-primary"></i>            <i class="fas fa-at fa-stack-1x fa-inverse"></i>          </span>                <h4 class="service-heading">Mail log</h4>
                    <p class="text-muted">A record of the e-mails you have sent to the property owners, tenants, and
                        support staff, saved with the names of recipients and the dates on which they were sent.</p>
                    <!--                <button type="button" class="btnss btn btn-dark">Read More</button>-->
                </div>
            </div>
        </div>
    </section>


  




    <section class="page-section" id="Features">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8 text-center"><h1 class="mt-0">WHY CHOOSE US</h1>
                    <hr class="divider my-4">
                    <!--                <p class="text-muted mb-5">Ready to start your next project with us? Give us a call or send us an email and we will get back to you as soon as possible!</p>-->
                </div>
            </div>
            <div class="row">
                <div class="f col-lg-3 ml-auto text-center pt-5 pb-5 hvr-glow  ">
                    <div class="hvr-icon-pulse-grow"><i class="iconss fas fa-tags fa-2x mb-3 text-muted hvr-icon"></i>
                        <h5>Affordable</h5>
                        <div><p>Get more for less</p></div>
                    </div>
                </div>
                <div class="f col-lg-3 ml-auto text-center pt-5 pb-5 hvr-glow hvr-icon-spin"><i class="iconss fas fa-history fa-2x mb-3 text-muted hvr-icon"></i>                <h5>
                        Round-the-clock support</h5>
                    <div><p>Call us any time for help with our PMS system.</p></div>
                </div>
                <div class="f col-lg-3 ml-auto text-center pt-5 pb-5  hvr-glow hvr-icon-bob"><i class="iconss fas fa-mobile-alt fa-2x mb-3 text-muted hvr-icon"></i>                <h5>
                        Responsive</h5>
                    <div><p>Use it on your cellphone, tablet, or computer.</p></div>
                </div>
                <div class="f col-lg-3 ml-auto text-center pt-5 pb-5  hvr-glow hvr-icon-bob "><i class="iconss fas fa-external-link-alt fa-2x mb-3 text-muted hvr-icon"></i>
                    <h5> Ease of use</h5>
                    <div><p>Make your life easy with the system’s simple interface.</p></div>
                </div>
                <div class="f col-lg-3 ml-auto text-center pt-5 pb-5  hvr-glow hvr-icon-bob"><i class="iconss fab fa-intercom fa-2x mb-3 text-muted hvr-icon"></i>                <h5>
                        Customizable</h5>
                    <div><p>Add features/modules to meet your specific business needs.</p></div>
                </div>
                <div class="f col-lg-3 ml-auto text-center pt-5 pb-5  hvr-glow hvr-icon-spin"><i class="iconss fas fa-sync-alt fa-2x mb-3 text-muted hvr-icon"></i>                <h5>
                        updates</h5>
                    <div><p>Receive frequent updates so that your PMS system keeps running smoothly.</p></div>
                </div>
                <div class="f col-lg-3 ml-auto text-center pt-5 pb-5  hvr-glow hvr-icon-bob"><i class="iconss fas fa-tachometer-alt fa-2x mb-3 text-muted hvr-icon"></i>                <h5>
                        Increased collaboration</h5>
                    <div><p>Collaborate with your team members with a centralized PMS</p></div>
                </div>
                <div class="f col-lg-3 ml-auto text-center pt-5 pb-5  hvr-glow hvr-icon-bob"><i class="iconss  fas fa-palette fa-2x mb-3 text-muted hvr-icon"></i>                <h5>Multi
                        Dashboard</h5>
                    <div><p>Choose from among a range of colors to configure your system’s aesthetics according to your
                            preferences.</p></div>
                </div>
            </div>
        </div>
    </section>
    <section id="Blogs" class="page-section" style="background-color: #f2f2f2;">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <div class="section-title text-left animated fadeInLeft visible" data-animation="fadeInLeft">
                        <!-- Heading --> <h2 class="title">Latest Posts</h2></div>
                    <ul class="latest-posts">
                        <li data-animation="fadeInLeft" class="animated fadeInLeft visible">
                            <div class="post-thumb"><img class="img-rounded" src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/img/Property-Management.jpg" alt="" title="" width="84" height="84"></div>
                            <div class="post-details">
                                <div class="description"><a href="blog.php">
                                        <!-- Text -->                                    Advantages of a Property
                                        Management System</a></div>
                                <div class="meta">                                <!-- Meta Date --> <span class="time">                                    <i class="fa fa-calendar"></i> 23.04.2019</span></div>
                            </div>
                        </li>
                  
                    </ul>
                </div>
                <div class="col-sm-12 col-md-6 testimonails">
                    <div class="section-title text-left animated fadeInRight visible" data-animation="fadeInRight">
                        <!-- Heading --> <h2 class="title">Testimonials</h2></div>
                    <div class="owl-carousel pagination-1 dark-switch owl-theme animated fadeInRight visible" data-effect="backSlide" data-pagination="true" data-autoplay="true" data-navigation="false" data-singleitem="true" data-animation="fadeInRight" style="opacity: 1; display: block;">
                        <div class="owl-wrapper-outer"><div class="owl-wrapper" style="width: 3420px; left: 0px; display: block; transition: all 0ms ease 0s; transform: translate3d(-570px, 0px, 0px); transform-origin: 855px center; perspective-origin: 855px center;"><div class="owl-item" style="width: 570px;"><div class="item">
                            <div class="desc-border bottom-arrow quote">
                                <blockquote class="small-text">We acquired a HR system from NetRoots, and have found it
                                    to exceed expectations. NetRoots was able to customize our software down to a T,
                                    creating for us a solution that addressed every one of our HR needs for an
                                    affordable price. Perhaps the best thing about the company was that their team
                                    worked closely with us to figure out what we need and then made us exactly that.
                                </blockquote>
                                <div class="star-rating text-right"><i class="fa fa-star text-color"></i> <i class="fa fa-star text-color"></i> <i class="fa fa-star text-color"></i> <i class="fa fa-star text-color"></i> <i class="fa fa-star-half-o text-color"></i></div>
                            </div>
                            <div class="client-details text-center">
                                <div class="client-image">                                <!-- Image --> <img class="img-circle" src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/img/TUv.png" width="80" height="80" alt=""></div>
                                <div class="client-details">                                <!-- Name --> <strong class="text-color">TUV Austria</strong>
                                    <!-- Company -->
                                    <!--                                    <span>Designer, zozothemes</span>-->
                                </div>
                            </div>
                        </div></div><div class="owl-item" style="width: 570px;"><div class="item">
                            <div class="desc-border bottom-arrow quote">
                                <blockquote class="small-text">NetRoots Technologies created an ERP system including
                                    HRMS and CRM for us. They automated all of our main business processes, and really
                                    simplified our work. Throughout the process, they remained in contact with us and
                                    took our suggestions and requirements into consideration. What we love about this
                                    ERP is that anyone can use it – young or old, tech-savvy or not! We are happy that
                                    we can focus on growing our business now that we don’t have to worry about doing
                                    everything manually.
                                </blockquote>
                                <div class="star-rating text-right"><i class="fa fa-star text-color"></i> <i class="fa fa-star text-color"></i> <i class="fa fa-star text-color"></i> <i class="fa fa-star text-color"></i> <i class="fa fa-star-half-o text-color"></i></div>
                            </div>
                            <div class="client-details text-center">
                                <div class="client-image">                                <!-- Image --> <img class="img-circle" src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/img/OAG.png" width="80" height="80" alt=""></div>
                                <div class="client-details">                                <!-- Name --> <strong class="text-color">Office Automation Group</strong>
                                    <!-- Company -->
                                    <!--                                    <span>Designer, zozothemes</span>-->
                                </div>
                            </div>
                        </div></div><div class="owl-item" style="width: 570px;"><div class="item">
                            <div class="desc-border bottom-arrow quote">
                                <blockquote class="small-text">Flour mills in Pakistan mostly operate in the traditional
                                    way, using manual records. We are glad that we made the switch. It was becoming
                                    difficult to keep record of and manually manage production and tracking and our ERP
                                    solved those problems. Our ERP also includes HR functions such as payroll and
                                    employee asset management. This makes keeping account of our finances much easier!
                                </blockquote>
                                <div class="star-rating text-right"><i class="fa fa-star text-color"></i> <i class="fa fa-star text-color"></i> <i class="fa fa-star text-color"></i> <i class="fa fa-star text-color"></i> <i class="fa fa-star-half-o text-color"></i></div>
                            </div>
                            <div class="client-details text-center">
                                <div class="client-image">                                <!-- Image --> <img class="img-circle" src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/img/Sufi.png" width="80" height="80" alt=""></div>
                                <div class="client-details">                                <!-- Name --> <strong class="text-color">Sufi Flour Mills</strong>
                                    <!-- Company -->
                                    <!--                                    <span>Designer, zozothemes</span>-->
                                </div>
                            </div>
                        </div></div></div></div>
                        
                        
                    <div class="owl-controls clickable"><div class="owl-pagination"><div class="owl-page"><span class=""></span></div><div class="owl-page active"><span class=""></span></div><div class="owl-page"><span class=""></span></div></div></div></div>
                </div>
            </div>
        </div>
    </section>
    <section id="clients" class="page-section tb-pad-30">
        <div class="container">
            <div class="section-title text-center animated fadeInUp visible" data-animation="fadeInUp">            <!-- Heading --> <h1 class="title"> Our Best Clients</h1></div>
            <div class="row">
                <div class="col-md-12 text-center animated fadeInDown visible" data-animation="fadeInDown">
                    <div class="owl-carousel navigation-1 owl-theme" data-pagination="false" data-items="6" data-autoplay="true" data-navigation="true" style="opacity: 1; display: block;"><div class="owl-wrapper-outer"><div class="owl-wrapper" style="width: 2730px; left: 0px; display: block; transition: all 1000ms ease 0s; transform: translate3d(0px, 0px, 0px);"><div class="owl-item" style="width: 195px;"><a> <img src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/img/1.jpg" width="150" height="150" alt=""> </a></div><div class="owl-item" style="width: 195px;"><a> <img src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/img/2.jpg" width="150" height="150" alt=""> </a></div><div class="owl-item" style="width: 195px;"><a> <img src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/img/3.jpg" width="150" height="150" alt=""> </a></div><div class="owl-item" style="width: 195px;"><a>
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/img/5.jpg" width="150" height="150" alt=""> </a></div><div class="owl-item" style="width: 195px;"><a> <img src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/img/6.jpg" width="150" height="150" alt="">
                        </a></div><div class="owl-item" style="width: 195px;"><a> <img src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/img/7.jpg" width="150" height="150" alt=""> </a></div><div class="owl-item" style="width: 195px;"><a> <img src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/img/8.jpg" width="150" height="150" alt=""> </a></div></div></div>      
                        <!--                        <a href="#">-->
                        <!--                            <img src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/img/sections/clients/2.png" width="150" height="150" alt="" />-->
                        <!--                        </a>-->                <div class="owl-controls clickable"><div class="owl-buttons"><div class="owl-prev"><i class="fa fa-angle-left"></i></div><div class="owl-next"><i class="fa fa-angle-right"></i></div></div></div></div>
                </div>
            </div>
        </div>
    </section><style>

    .okewa-style_3.okewa-text_3 #okewa-floating_cta{

        bottom: 55px;
        right: 13px;
    }
    .okewa-pulse_3{
        bottom: 55px !important;
    }
</style>






<div id="get-quote" class="bg-color get-a-quote black text-center">
    <div class="container animated pulse visible" data-animation="pulse">
        <div class="row">
            <div class="col-md-12"><p>Get A Free Quote / Need a Help ? <a class="black" href="../contact-us.php">Contact
                        Us</a></p></div>
        </div>
    </div>
</div>
<footer id="footer" style="padding: 0 0; font-family: Montserrat;">    <style>

    .okewa-style_3.okewa-text_3 #okewa-floating_cta{

        bottom: 55px;
        right: 13px;
    }
    .okewa-pulse_3{
        bottom: 55px !important;
    }
</style>






<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500&amp;display=swap" rel="stylesheet">
<div class="footer-widget">
    <div class="container">
        <div class="row">
            <style>
                .widget li a, .widget li a:visited {
                    color: #000000;
                    text-decoration: none;
                }
                .widget a, .widget a:visited {
                    color: #7f7f7f;
                    text-decoration: none;
                    -webkit-transition: color .2s linear;
                    transition: color .2s linear;
                }
                a, a:hover, a:visited, a:focus {
                    outline: 0 none;
                }
                .footer-blog a{
                    color: #000000;
                }
                .footer-blog a:hover{
                    color: #7f7f7f;
                }
            </style>

            <div class="col-xs-12 col-sm-6 col-md-4 widget">                <!-- Title -->
                <div class="titles"><h5 class="titles">Head Office</h5></div>
                <hr>
                <nav>
                    <ul class="footer-blog">                        <!-- List Items -->
                        <li><p><i class="fass fas fa-map-marker-alt"></i> 11 Grattan St, Kylekiproe, Portlaoise, Laois, R32 HY59,Ireland
                            </p>
                            <p><i class="fass fa fa-phone"></i> <a href="tel:+353578601255" target="_blank">+353 (57) 8601255</a></p>
                            <p><i class="farr fa fa-envelope"></i> <a href="mailto:info@netrootstech.com">info@netrootstech.com</a></p>

                            <p></p></li>
                    </ul>
                </nav>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 widget newsletter bottom-xs-pad-20">                <!-- Title -->
                <div class="titles"><h5 class="titles">USA Office</h5></div>
                <hr>
                <div class="footer-blog">
                    <p><i class="fass fas fa-map-marker-alt"></i> 1845 Freemansburg Ave, Easton, Pennsylvania 18042,USA</p>
                    <p><i class="fass fa fa-phone"></i><a href="tel:+13023001742" target="_blank">+1 (302) 300 1742</a></p>
                    <p><i class="fass fa fa-phone"></i><a href="tel:+16109053046" target="_blank">+1 (610) 905 3046</a></p>
                    <p><i class="farr fa fa-envelope"></i> <a href="mailto:info@netrootstech.com">info@netrootstech.com</a></p>

                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 widget newsletter bottom-xs-pad-20">                <!-- Title -->
                <div class="titles"><h5 class="titles">Canada Office</h5></div>
                <hr>
                <div class="footer-blog">
                    <p><i class="fass fas fa-map-marker-alt"></i>  1325 Derry Rd East,
Unit 3 (2nd Floor)
Mississauga, ON, L5T1B6, CANADA </p>
                    <p><i class="fass fa fa-phone"></i><a href="tel:+16474906750" target="_blank">+1 (647) 490 6750</a></p>
                    <p><i class="farr fa fa-envelope"></i> <a href="mailto:info@netrootstech.com">info@netrootstech.com</a></p>

                </div>
            </div>
        </div>
        <hr>
        <div class="history row">
            <div class="col-sm-12 col-md-4"><img src="https://www.netrootstech.com//assets/<?php echo get_template_directory_uri(); ?>/assets/pms-assets/img/footer-logo.png" alt="" class="logo-img-fluid img-fluid" width="100%"></div>
            <div class="col-sm-12 col-md-8">
                <div class="text" style="font-family: Montserrat; font-weight: 300;"><p class="text-p"> NetRoots Technologies is a modern Information Technology services
                        and consultancy firm, designed especially to help businesses succeed in the increasingly digital
                        times. From application development to knowledge and business process management, we address
                        your IT needs with care and precision. </p></div>
            </div>
        </div>
    </div>
</div><!-- footer-top -->
<div class="copyright">
    <div class="container">
        <div class="row">            <!-- Copyrights -->
            <div align="left" class="rights-text col-xs-12 col-sm-12 col-md-6"> © copyright 2019 <a href="https://www.netrootstech.com/">netrootstech.com</a>. <a href="#">All rights reserved.</a>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 text-right page-scroll gray-bg icons-circle i-3x">
                <div class="social-icon gray-bg icons-circle i-3x">
                    <ul class="social-icons">
                        <li class="fb"><a href="https://www.facebook.com/netrootstech/"> <img src="https://www.netrootstech.com//assets/images/sm_icon_1.png" alt="FaceBook">
                            </a></li>

                        <li class="twitter"><a href="https://twitter.com/Netrootstech7"> <img src="https://www.netrootstech.com//assets/images/sm_icon_2.png" alt="Twitter">
                            </a></li>

                        <li class="pintress"><a href="https://www.pinterest.com/netrootsfb/"> <img src="https://www.netrootstech.com//assets/images/sm_icon_3.png" alt="pintress">
                            </a></li>
                        <li class="linked-in"><a href="https://www.linkedin.com/company/13277143/admin/"> <img src="https://www.netrootstech.com//assets/images/sm_icon_4.png" alt="FaceBook">
                            </a></li>

                        <li class="linked-in" style=" background: #d6249f;
  background: radial-gradient(circle at 30% 107%, #fdf497 0%, #fdf497 5%, #fd5949 45%,#d6249f 60%,#285AEB 90%);"><a href="https://www.instagram.com/netrootstech7/">
                                <img src="https://www.netrootstech.com//assets/images/insta.png" alt="linked-in" width="21" height="21"></a></li>

                        <li class="aero"><a href="#page"> <i class="glyphicon glyphicon-arrow-up"></i> </a></li>
                    </ul>                    <!-- Goto Top -->                </div>
            </div>
        </div>
    </div>
</div><!-- footer-bottom -->
<style>    @media (min-width: 320px) and (max-width: 767px) {
        .titles {
            text-align: center !important;
        }

        .footer-blog p {
            text-align: center;
        }

        .rights-text {
            text-align: center;
        }

        .social-icon {
            display: flex;
            align-content: center;
            justify-content: center;
            padding-top: 10px;
            margin-top: 10px;
        }

        .text:before {
          display: none;
        }

        #footer {
            padding-top: 20px !important;
        }
    }

    #footer {
        background-image: url("https://www.netrootstech.com//assets/<?php echo get_template_directory_uri(); ?>/assets/pms-assets/img/footer.jpg");
        background-size: cover;
        color: black;
        font-family: 'Montserrat', sans-serif;
        font-weight: 400;
    }

    #footer .footer-widget {
        background: #f2f2f200;
        color: black;
        font-family: 'Montserrat', sans-serif;
        font-weight: 400;
    }

    .titles {
        text-align: left;
        font-size: 22px;
        font-weight: normal;
    }

    .footer-blog p {
        font-size: 16px;
        color: #1A1A1A;
    }

    .fass {
        color: #33889f;
    }

    .farr {
        color: #33889f;
    }

    .history {
        padding: 15px 0 15px 0;
        font-family: "Montserrat Medium";
        font-weight: lighter;
    }

    .text {
        font-size: 16px;
        text-align: justify;
    }

    .text:before {
        background: #58929c;
        content: "";
        display: block;
        height: 100%;
        position: absolute;
        width: 5px;
        left: -1%;
    }

    .text-p {
        margin-top: 15px;
    }

    .copyright {
        background: #097589 !important;
        padding: 20px 0 !important;
        color: white;
    }

    .copyright a {
        color: white;
    }

    ul.footer-blog li {
        list-style-type: none;
        padding-right: 0;
    }

    ul.footer-blog {
        padding: 0;
    }

    ul.social-icons {
        margin: 0;
        padding: 0;
        list-style: none;
    }

    .social-icons {
        float: right;
    }

    .social-icons li {
        float: left;
        margin: 0 4px;
        width: 30px;
        height: 30px;
        text-align: center;
    }

    .social-icons li.fb {
        background: #3360ad;
    }

    .social-icons li.twitter {
        background: #23bbeb;
    }

    .social-icons li.pintress {
        background: #FF0004;
    }

    .social-icons li img {
        margin-top: 6px;
    }

    .social-icons li.linked-in {
        background: #178dc4;
    }
    .okewa-style_3.okewa-text_3 #okewa-floating_cta{

        bottom: 55px;
        right: 13px;
    }
    .okewa-pulse_3{
        bottom: 55px !important;
    }



</style>













</footer>
<script async="" src="https://embed.tawk.to/5cc2d770ee912b07bec4f7fa/default" charset="UTF-8" crossorigin="*"></script><script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script><!-- Plugin JavaScript -->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script><!-- Contact form JavaScript -->
<script src="js/jqBootstrapValidation.js"></script>
<script src="js/contact_me.js"></script><!-- Custom scripts for this template -->
<script src="js/agency.min.js"></script>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script><!-- Menu jQuery plugin -->
<script type="text/javascript" src="js/hover-dropdown-menu.js"></script><!-- Menu jQuery Bootstrap Addon -->
<script type="text/javascript" src="js/jquery.hover-dropdown-menu-addon.js"></script><!-- Scroll Top Menu -->
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script><!-- Sticky Menu -->
<script type="text/javascript" src="js/jquery.sticky.js"></script><!-- Bootstrap Validation -->
<script type="text/javascript" src="js/bootstrapValidator.min.js"></script><!-- Revolution Slider -->
<script type="text/javascript" src="rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="js/revolution-custom.js"></script><!-- Portfolio Filter -->
<script type="text/javascript" src="js/jquery.mixitup.min.js"></script><!-- Animations -->
<script type="text/javascript" src="js/jquery.appear.js"></script>
<script type="text/javascript" src="js/effect.js"></script><!-- Owl Carousel Slider -->
<script type="text/javascript" src="js/owl.carousel.min.js"></script><!-- Pretty Photo Popup -->
<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script><!-- Parallax BG -->
<script type="text/javascript" src="js/jquery.parallax-1.1.3.js"></script><!-- Fun Factor / Counter -->
<script type="text/javascript" src="js/jquery.countTo.js"></script><!-- Twitter Feed -->
<script type="text/javascript" src="js/tweet/carousel.js"></script>
<script type="text/javascript" src="js/tweet/scripts.js"></script>
<script type="text/javascript" src="js/tweet/tweetie.min.js"></script><!-- Background Video -->
<script type="text/javascript" src="js/jquery.mb.YTPlayer.js"></script><!-- Custom Js Code -->
<script type="text/javascript" src="js/custom.js"></script> <!--Start of Tawk.to Script-->
<script type="text/javascript">var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
    (function () {
        var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
        s1.async = true;
        s1.src = 'https://embed.tawk.to/5cc2d770ee912b07bec4f7fa/default';
        s1.charset = 'UTF-8';
        s1.setAttribute('crossorigin', '*');
        s0.parentNode.insertBefore(s1, s0);
    })();</script><!--End of Tawk.to Script-->
<script>    $('.navbar-collapse a').click(function (e) {
        if ($(e.target).is('.nav-item a') && $(e.target).attr('class') != 'dropdown-toggle') {
            $('.navbar-collapse.in').collapse('toggle');
        }
    });</script>
<div id="k6eQzcU-1566218496833" class="" style="display: block !important;"><iframe id="pkacVbi-1566218496834" src="about:blank" frameborder="0" scrolling="no" title="chat widget" class="" style="outline: none !important; visibility: visible !important; resize: none !important; box-shadow: none !important; overflow: visible !important; background: none transparent !important; opacity: 1 !important; top: auto !important; right: auto !important; bottom: auto !important; left: auto !important; position: static !important; border: 0px !important; min-height: auto !important; min-width: auto !important; max-height: none !important; max-width: none !important; padding: 0px !important; margin: 0px !important; transition-property: none !important; transform: none !important; width: 280px !important; z-index: 999999 !important; cursor: auto !important; float: none !important; border-radius: unset !important; display: none !important; height: 120px !important;"></iframe><iframe id="YkXilh6-1566218496835" src="about:blank" frameborder="0" scrolling="no" title="chat widget" class="" style="outline: none !important; visibility: visible !important; resize: none !important; overflow: visible !important; background: none transparent !important; opacity: 1 !important; position: fixed !important; border: 0px !important; padding: 0px !important; transition-property: none !important; z-index: 1000001 !important; cursor: auto !important; float: none !important; box-shadow: rgba(0, 0, 0, 0.16) 0px 2px 10px 0px !important; height: 60px !important; min-height: 60px !important; max-height: 60px !important; width: 60px !important; min-width: 60px !important; max-width: 60px !important; border-radius: 50% !important; transform: rotate(0deg) translateZ(0px) !important; transform-origin: 0px center !important; margin: 0px !important; top: auto !important; bottom: 20px !important; left: 20px !important; right: auto !important; display: block !important;"></iframe><iframe id="w6HQTv0-1566218496836" src="about:blank" frameborder="0" scrolling="no" title="chat widget" class="" style="outline: none !important; visibility: visible !important; resize: none !important; box-shadow: none !important; overflow: visible !important; background: none transparent !important; opacity: 1 !important; position: fixed !important; border: 0px !important; padding: 0px !important; margin: 0px !important; transition-property: none !important; transform: none !important; z-index: 1000003 !important; cursor: auto !important; float: none !important; border-radius: unset !important; top: auto !important; bottom: 60px !important; left: 64px !important; right: auto !important; width: 21px !important; max-width: 21px !important; min-width: 21px !important; height: 21px !important; max-height: 21px !important; min-height: 21px !important; display: block !important;"></iframe><iframe id="bsjY4Pp-1566218496836" src="about:blank" frameborder="0" scrolling="no" title="chat widget" class="" style="outline: none !important; visibility: visible !important; resize: none !important; box-shadow: none !important; overflow: visible !important; background: none transparent !important; opacity: 1 !important; position: fixed !important; border: 0px !important; padding: 0px !important; transition-property: none !important; cursor: auto !important; float: none !important; border-radius: unset !important; transform: rotate(0deg) translateZ(0px) !important; transform-origin: 0px center !important; bottom: 30px !important; top: auto !important; left: 0px !important; right: auto !important; width: 124px !important; max-width: 124px !important; min-width: 124px !important; height: 95px !important; max-height: 95px !important; min-height: 95px !important; z-index: 1000002 !important; margin: 0px !important; display: block !important;"></iframe><div class="" style="outline: none !important; visibility: visible !important; resize: none !important; box-shadow: none !important; overflow: visible !important; background: none transparent !important; opacity: 1 !important; top: 0px !important; right: auto !important; bottom: auto !important; left: 0px !important; position: absolute !important; border: 0px !important; min-height: auto !important; min-width: auto !important; max-height: none !important; max-width: none !important; padding: 0px !important; margin: 0px !important; transition-property: none !important; transform: none !important; width: 100% !important; height: 100% !important; display: none !important; z-index: 1000001 !important; cursor: move !important; float: left !important; border-radius: unset !important;"></div><div id="My6BoVW-1566218496833" class="" style="outline: none !important; visibility: visible !important; resize: none !important; box-shadow: none !important; overflow: visible !important; background: none transparent !important; opacity: 1 !important; top: 0px !important; right: 0px !important; bottom: auto !important; left: auto !important; position: absolute !important; border: 0px !important; min-height: auto !important; min-width: auto !important; max-height: none !important; max-width: none !important; padding: 0px !important; margin: 0px !important; transition-property: none !important; transform: none !important; width: 6px !important; height: 100% !important; display: block !important; z-index: 999998 !important; cursor: e-resize !important; float: none !important; border-radius: unset !important;"></div><div id="I8HQnbk-1566218496833" class="" style="outline: none !important; visibility: visible !important; resize: none !important; box-shadow: none !important; overflow: visible !important; background: none transparent !important; opacity: 1 !important; top: 0px !important; right: 0px !important; bottom: auto !important; left: auto !important; position: absolute !important; border: 0px !important; min-height: auto !important; min-width: auto !important; max-height: none !important; max-width: none !important; padding: 0px !important; margin: 0px !important; transition-property: none !important; transform: none !important; width: 100% !important; height: 6px !important; display: block !important; z-index: 999998 !important; cursor: n-resize !important; float: none !important; border-radius: unset !important;"></div><div id="ZVrLs6T-1566218496834" class="" style="outline: none !important; visibility: visible !important; resize: none !important; box-shadow: none !important; overflow: visible !important; background: none transparent !important; opacity: 1 !important; top: 0px !important; right: 0px !important; bottom: auto !important; left: auto !important; position: absolute !important; border: 0px !important; min-height: auto !important; min-width: auto !important; max-height: none !important; max-width: none !important; padding: 0px !important; margin: 0px !important; transition-property: none !important; transform: none !important; width: 12px !important; height: 12px !important; display: block !important; z-index: 999998 !important; cursor: ne-resize !important; float: none !important; border-radius: unset !important;"></div><iframe id="OgYWyoF-1566218496900" src="about:blank" frameborder="0" scrolling="no" title="chat widget" class="" style="outline: none !important; visibility: visible !important; resize: none !important; box-shadow: none !important; overflow: visible !important; background: none transparent !important; opacity: 1 !important; position: fixed !important; border: 0px !important; min-height: auto !important; min-width: auto !important; max-height: none !important; max-width: none !important; padding: 0px !important; margin: 0px !important; transition-property: none !important; transform: none !important; width: 378px !important; height: 520px !important; display: none !important; z-index: 999999 !important; cursor: auto !important; float: none !important; border-radius: unset !important; bottom: 100px !important; top: auto !important; left: 20px !important; right: auto !important;"></iframe></div><script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false&amp;callback=GmapInit"></script></body>