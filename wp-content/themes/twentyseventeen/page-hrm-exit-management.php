<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_template_part( 'template-parts/header/header', 'hrm' ); ?>

<!-- ------------------------------------------------template body start from here -->

<div class="top-banner">
        <div class="container">
            <div class="banner-content">
                <h1>
                    Exit Management
                </h1>
                <p>
                   Goodbyes are a part of life. But who says they have to be hard?
                </p>
                


            </div>




        </div>



    </div>

    <!--banner ends-->


    <!--module-area start-->

    <div class="module-area">

        <div class="container">
            <h2>
                Goodbyes are a part of life. But who says they have to be hard? 
            </h2>
            <p>
                Fórsa HR software provides exit management solutions that make saying goodbye easier. 
Our exit management software allows employees to place a resignation request and the management to accept or reject it.

            </p>
            <div class="module-chart">
                <div class="row">
                    <div class="col-md-4">

                        <div class="branches-exit" data-aos="flip-up">

                            <a href="#">
                                <h4>
                                    Resignation Requests
                                </h4>
                                <p>
                                 Employees can submit resignation requests by filling out and submitting a form. The request is then received by the management, who can decide whether to accept or deny the request.
                                </p>
                            </a>

                        </div>

                       

                    </div>
                    <div class="col-md-4">

                        <figure class="module-tablet"  data-aos="zoom-in">

                            <a href="#">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/images/exit-tab.png" alt="dashboard" width="100%">
                            </a>

                        </figure>

                    </div>
                    <div class="col-md-4">

                        <div class="branches-right-exit" data-aos="flip-up">
                            <a href="#">

                                <h4>
                                    Resignation Formalities
                                </h4>
                                <p>
                                  If the request is accepted, the departing employee may be given exit interview forms so that the company can get beneficial feedback to help it improve. The employees may also be issued departmental clearance forms through the HR software.
                                </p>
                            </a>

                        </div>

                       


                    </div>


                </div>



            </div>



        </div>

    </div>

    <!--module-area ends-->


    <!--content-wraper start-->

    <div class="content-wraper">

        <div class="container">

            <div class="row">



                <div class="col-md-6">

                    <div class="wraper-text"data-aos="fade-right"
                         data-aos-offset="300"
                         data-aos-easing="ease-in-sine">

                        <h2>
                            Modern, Customizable, Tightly Integrated 
                        </h2>
                        <p>
                           Like the rest of Fórsa, its exit management module is sleek and modern, greatly simplifying the exit process. Although it is designed to automate all tasks related to an employee’s exit, it can be tailored to your requirements. In addition, once an employee has resigned, the exit management software, which is integrated with the other modules of Fórsa HR software, will communicate with the payroll software so that the former employee’s name is removed from the payroll. 

                        </p>



                    </div>



                </div>
                <div class="col-md-6">
                    <figure class="wraper-image" data-aos="zoom-in-up">

                        <a href="#">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/images/exit-laptop.png" alt="dashboard" width="100%">
                        </a>

                    </figure>

                </div>

            </div>



        </div>

    </div>

    <!--content-wraper end-->

    <!--text area-->

    <div class="text-area" data-aos="zoom-in-down">

        <div class="container">

            <h2>
                Why use Fórsa’s Exit Management Software? 
            </h2>

            <p>

               
Like other HR software, Fórsa works to cut down time and money spent on mundane HR tasks. However, Fórsa stands out thanks to the incorporation of business intelligent into the software. Therefore, one of the most beneficial features of Fórsa’s exit management software is the insight it provides into employee satisfaction in your company. In addition to facilitating the requesting and approval of resignations, the exit management module helps the company’s management and leadership draw conclusions about retention and turn-over rate. 

            </p>


        </div>



    </div>


<!-- ----------------------------------------------template end here -->

<script>
	AOS.init();
	</script>
<?php
get_footer();
