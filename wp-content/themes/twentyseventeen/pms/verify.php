<?php
//Checking For reCAPTCHA
$captcha;
require 'PHPMailer-master/PHPMailerAutoload.php';
$mail = new PHPMailer();
$myemail = 'ContactUs@netrootstech.com'  ;
//<-----Put Your email address here.
//$myemail = ' ContactUs@netrootstech.com, toseef.ahmed@netrootstech.com, salman.rafique@netrootstech.com , m.zain@netrootstech.com ,farhan.nazim@netrootstech.com, mustafa.mughal@netrootstech.com  ';
$errors = '';

if(isset($_POST["submit"])){

    // Checking For Blank Fields..
    $name = trim($_POST['contact_name']);
    $email_address = trim($_POST['contact_email']);
    $phone = trim($_POST['contact_phone']);
    $message = trim($_POST['contact_message']);

    if(empty($name)  ||
        empty($email_address) ||
        empty($message) ||
        empty($phone) )
    {
        $errors .= "\n Error: all fields are required";
    }
    
    if (!preg_match(
        "/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i",
        $email_address))
    {
        $errors .= "\n Error: Invalid email address";
    }


    if($errors == '')
    {
        $addresses = explode(',', $myemail);
        foreach ($addresses as $address) {

            $to = $address;
            $email_subject = "Netroots Technologies: $name";
            $email_body = "You have received a new message. ".
                " Here are the details:\n Name: $name \n ".
                "Email: $email_address\n Phone-No:  $phone\nMessage \n $message";
            $headers = "From: $email_address\n";
            $headers .= "Reply-To: $email_address";
            mail($to,$email_subject,$email_body,$headers);
        }

        header('Location: production-management-software-solutions.php?email_status=email_sent');


    }

}
else{
    echo "error";
}

?>
