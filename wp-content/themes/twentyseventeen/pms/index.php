<?php include 'header.php' ?>
    <header>
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <!-- Slide One - Set the background image for this slide in the line below -->
                <div class="carousel-item active"
                     style="background-image: url('img/cropped-Savin-NY-Website-Background-Web.jpg');">
                    <div class="carousel-caption d-none d-md-block">
                        <div class="first"><h1 class="display-4" style="color: black;font-size: 60px;font-weight: bold">
                                NRT Property Management System</h1>
                            <h3 style="color: black">Automate and simplify residential and commercial property
                                management </h3></div>
                    </div>
                </div>            <!-- Slide Two - Set the background image for this slide in the line below -->
                <div class="carousel-item" style="background-image: url('img/Property-Management2.jpg')">
                    <div class="carousel-caption d-none d-md-block"><h1 class="display-4 "
                                                                        style="text-align: left ;margin-left: 50px;color: black;font-size: 60px;">
                            Digitize your information</h1>                    <h4
                                style="text-align: left ;margin-left: 50px;color: black">Create a centralized database
                            of all your properties, property owners, and tenants </h4></div>
                </div>            <!-- Slide Three - Set the background image for this slide in the line below -->
                <div class="carousel-item" style="background-image: url('img/hero-accounting-hands.jpg')">
                    <div class="carousel-caption d-none d-md-block"><h1 class="display-4"
                                                                        style="text-align: left ;margin-left: 50px; color:black;font-size: 60px;font-weight: bold">
                            <span style="background-color: #ffffff4f">Manage your accounts</span></h1>
                        <h4 style="text-align: left ;margin-left: 50px;color: black;padding-bottom: 100px;"><span
                                    style="background-color: #ffffff4f">Keep track of your commissions and expenses</span>
                        </h4></div>
                </div>
            </div>
            <span id="Modules"> </span> <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                                           data-slide="prev"> <span class="carousel-control-prev-icon"
                                                                    aria-hidden="true"></span> <span class="sr-only">Previous</span>
            </a> <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span>
            </a></div>
    </header>
    <section style="    padding-top: 20px;padding-bottom: 20px">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading text-uppercase">NETROOTS PMS -
                        <small>We rekindle your love for property management</small>
                    </h2>
                    <h3 class="section-subheading text-muted" style="font-style: normal">NetRoots PMS simplifies
                        residential and commercial property management so that you can stop worrying about the tedious
                        tasks and find enjoyment in your work. With information about property, property owners,
                        tenants, and support staff as well as contracts and complaints all digitized, you can stop
                        worrying about files and folders and instead focus on making deals.</h3></div>
            </div>
            <div class="row text-center">
                <div class=" col-md-3 hvr-grow-shadow pt-3 pb-3 animated fadeInUp visible" data-animation="fadeInUp">
                    <span class="fa-stack fa-4x">            <i class="fas fa-circle fa-stack-2x text-primary"></i>            <i
                                class="fas fa-tachometer-alt fa-stack-1x fa-inverse"></i>          </span>
                    <h4 class="service-heading">Dashboard</h4>
                    <p class="text-muted">An executive summary of the property, people, and processes relevant to your
                        property management business, contracts, properties, tenants and the owners of the properties
                        under your care. </p>
                    <!--                <button type="button" class="btnss btn btn-dark">Read More</button>-->
                </div>
                <div class=" col-md-3 hvr-grow-shadow pt-3 pb-3"><span class="fa-stack fa-4x">            <i
                                class="fas fa-circle fa-stack-2x text-primary"></i>            <i
                                class="far fa-user fa-stack-1x fa-inverse"></i>          </span>                <h4
                            class=" service-heading">Property Owners</h4>
                    <p class="text-muted">A log of each of the pieces of property you are managing, along with its
                        owner’s name, its type (whether for sale or for rent) and address, and its status (whether it is
                        currently being managed or was previously managed). </p>
                    <!--                <button type="button" class="btnss btn btn-dark">Read More</button>-->
                </div>
                <div class=" col-md-3 hvr-grow-shadow pt-3 pb-3"><span class="fa-stack fa-4x">            <i
                                class="fas fa-circle fa-stack-2x text-primary"></i>            <i
                                class="fas fa-map-marked-alt fa-stack-1x fa-inverse"></i>          </span>
                    <h4 class=" service-heading">Property</h4>
                    <p class="text-muted">A record of all property owners that have entrusted their property to you,
                        listed with their names, contact information, social security numbers, bank account numbers, and
                        active/inactive status. </p>
                    <!--                <button type="button" class="btnss btn btn-dark">Read More</button>-->
                </div>
                <div class=" col-md-3 hvr-grow-shadow pt-3 pb-3"><span class="fa-stack fa-4x">            <i
                                class="fas fa-circle fa-stack-2x text-primary"></i>            <i
                                class="fas fa-male fa-stack-1x fa-inverse"></i>          </span>                <h4
                            class="service-heading">Tenants</h4>
                    <p class="text-muted">A list of tenants renting the properties under your care, along with their
                        names, contact information, social security numbers, bank account numbers, and active/inactive
                        status.</p>
                    <!--                <button type="button" class="btnss btn btn-dark">Read More</button>-->
                </div>
                <div class=" col-md-3 hvr-grow-shadow pt-3 pb-3 mt-3"><span class="fa-stack fa-4x">            <i
                                class="fas fa-circle fa-stack-2x text-primary"></i>            <i
                                class="fas fa-people-carry fa-stack-1x fa-inverse"></i>          </span>
                    <h4 class="service-heading">Support Staff</h4>
                    <p class="text-muted">A database of all support staff that you have hired to resolve tenants’
                        complaints about the rented property, listed with their contact information, skills, and
                        active/inactive status. </p>
                    <!--                <button type="button" class="btnss btn btn-dark">Read More</button>-->
                </div>
                <div class=" col-md-3 hvr-grow-shadow pt-3 pb-3 mt-3"><span class="fa-stack fa-4x">            <i
                                class="fas fa-circle fa-stack-2x text-primary"></i>            <i
                                class="fas fa-address-card fa-stack-1x fa-inverse"></i>          </span>
                    <h4 class="service-heading">Contracts</h4>
                    <p class="text-muted">A store of all active and inactive contracts concluded between you and your
                        clients, including the names of the tenants/buyers and assets, end and start dates, amount of
                        security deposited, and the commission you received.</p>
                    <!--                <button type="button" class="btnss btn btn-dark">Read More</button>-->
                </div>
                <div class=" col-md-3 hvr-grow-shadow pt-3 pb-3 mt-3"><span class="fa-stack fa-4x">            <i
                                class="fas fa-circle fa-stack-2x text-primary"></i>            <i
                                class="fas fa-clipboard-list fa-stack-1x fa-inverse"></i>          </span>
                    <h4 class=" service-heading">Complaints</h4>
                    <p class="text-muted">A log of all complaints submitted by tenants through the system, along with
                        the names and addresses of the property, the dates of complaint, the members of support staff to
                        whom the complaints were assigned, and their status (whether open, resolved, or in
                        progress).</p>
                    <!--                <button type="button" class="btnss btn btn-dark">Read More</button>-->
                </div>
                <div class=" col-md-3 hvr-grow-shadow pt-3 pb-3 mt-3"><span class="fa-stack fa-4x">            <i
                                class="fas fa-circle fa-stack-2x text-primary"></i>            <i
                                class="fas fa-at fa-stack-1x fa-inverse"></i>          </span>                <h4
                            class="service-heading">Mail log</h4>
                    <p class="text-muted">A record of the e-mails you have sent to the property owners, tenants, and
                        support staff, saved with the names of recipients and the dates on which they were sent.</p>
                    <!--                <button type="button" class="btnss btn btn-dark">Read More</button>-->
                </div>
            </div>
        </div>
    </section>


  




    <section class="page-section" id="Features">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8 text-center"><h1 class="mt-0">WHY CHOOSE US</h1>
                    <hr class="divider my-4">
                    <!--                <p class="text-muted mb-5">Ready to start your next project with us? Give us a call or send us an email and we will get back to you as soon as possible!</p>-->
                </div>
            </div>
            <div class="row">
                <div class="f col-lg-3 ml-auto text-center pt-5 pb-5 hvr-glow  ">
                    <div class="hvr-icon-pulse-grow"><i class="iconss fas fa-tags fa-2x mb-3 text-muted hvr-icon"></i>
                        <h5>Affordable</h5>
                        <div><p>Get more for less</p></div>
                    </div>
                </div>
                <div class="f col-lg-3 ml-auto text-center pt-5 pb-5 hvr-glow hvr-icon-spin"><i
                            class="iconss fas fa-history fa-2x mb-3 text-muted hvr-icon"></i>                <h5>
                        Round-the-clock support</h5>
                    <div><p>Call us any time for help with our PMS system.</p></div>
                </div>
                <div class="f col-lg-3 ml-auto text-center pt-5 pb-5  hvr-glow hvr-icon-bob"><i
                            class="iconss fas fa-mobile-alt fa-2x mb-3 text-muted hvr-icon"></i>                <h5>
                        Responsive</h5>
                    <div><p>Use it on your cellphone, tablet, or computer.</p></div>
                </div>
                <div class="f col-lg-3 ml-auto text-center pt-5 pb-5  hvr-glow hvr-icon-bob "><i
                            class="iconss fas fa-external-link-alt fa-2x mb-3 text-muted hvr-icon"></i>
                    <h5> Ease of use</h5>
                    <div><p>Make your life easy with the system’s simple interface.</p></div>
                </div>
                <div class="f col-lg-3 ml-auto text-center pt-5 pb-5  hvr-glow hvr-icon-bob"><i
                            class="iconss fab fa-intercom fa-2x mb-3 text-muted hvr-icon"></i>                <h5>
                        Customizable</h5>
                    <div><p>Add features/modules to meet your specific business needs.</p></div>
                </div>
                <div class="f col-lg-3 ml-auto text-center pt-5 pb-5  hvr-glow hvr-icon-spin"><i
                            class="iconss fas fa-sync-alt fa-2x mb-3 text-muted hvr-icon"></i>                <h5>
                        updates</h5>
                    <div><p>Receive frequent updates so that your PMS system keeps running smoothly.</p></div>
                </div>
                <div class="f col-lg-3 ml-auto text-center pt-5 pb-5  hvr-glow hvr-icon-bob"><i
                            class="iconss fas fa-tachometer-alt fa-2x mb-3 text-muted hvr-icon"></i>                <h5>
                        Increased collaboration</h5>
                    <div><p>Collaborate with your team members with a centralized PMS</p></div>
                </div>
                <div class="f col-lg-3 ml-auto text-center pt-5 pb-5  hvr-glow hvr-icon-bob"><i
                            class="iconss  fas fa-palette fa-2x mb-3 text-muted hvr-icon"></i>                <h5>Multi
                        Dashboard</h5>
                    <div><p>Choose from among a range of colors to configure your system’s aesthetics according to your
                            preferences.</p></div>
                </div>
            </div>
        </div>
    </section>
    <section id="Blogs" class="page-section" style="background-color: #f2f2f2;">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <div class="section-title text-left" data-animation="fadeInLeft">
                        <!-- Heading --> <h2 class="title">Latest Posts</h2></div>
                    <ul class="latest-posts">
                        <li data-animation="fadeInLeft">
                            <div class="post-thumb"><img class="img-rounded" src="img/Property-Management.jpg" alt=""
                                                         title="" width="84" height="84"/></div>
                            <div class="post-details">
                                <div class="description"><a href="blog.php">
                                        <!-- Text -->                                    Advantages of a Property
                                        Management System</a></div>
                                <div class="meta">                                <!-- Meta Date --> <span class="time">                                    <i
                                                class="fa fa-calendar"></i> 23.04.2019</span></div>
                            </div>
                        </li>
                  
                    </ul>
                </div>
                <div class="col-sm-12 col-md-6 testimonails">
                    <div class="section-title text-left" data-animation="fadeInRight">
                        <!-- Heading --> <h2 class="title">Testimonials</h2></div>
                    <div class="owl-carousel pagination-1 dark-switch" data-effect="backSlide" data-pagination="true"
                         data-autoplay="true" data-navigation="false" data-singleitem="true"
                         data-animation="fadeInRight">
                        <div class="item">
                            <div class="desc-border bottom-arrow quote">
                                <blockquote class="small-text">We acquired a HR system from NetRoots, and have found it
                                    to exceed expectations. NetRoots was able to customize our software down to a T,
                                    creating for us a solution that addressed every one of our HR needs for an
                                    affordable price. Perhaps the best thing about the company was that their team
                                    worked closely with us to figure out what we need and then made us exactly that.
                                </blockquote>
                                <div class="star-rating text-right"><i class="fa fa-star text-color"></i> <i
                                            class="fa fa-star text-color"></i> <i class="fa fa-star text-color"></i> <i
                                            class="fa fa-star text-color"></i> <i
                                            class="fa fa-star-half-o text-color"></i></div>
                            </div>
                            <div class="client-details text-center">
                                <div class="client-image">                                <!-- Image --> <img
                                            class="img-circle" src="img/TUv.png" width="80" height="80" alt=""/></div>
                                <div class="client-details">                                <!-- Name --> <strong
                                            class="text-color">TUV Austria</strong>
                                    <!-- Company -->
                                    <!--                                    <span>Designer, zozothemes</span>-->
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="desc-border bottom-arrow quote">
                                <blockquote class="small-text">NetRoots Technologies created an ERP system including
                                    HRMS and CRM for us. They automated all of our main business processes, and really
                                    simplified our work. Throughout the process, they remained in contact with us and
                                    took our suggestions and requirements into consideration. What we love about this
                                    ERP is that anyone can use it – young or old, tech-savvy or not! We are happy that
                                    we can focus on growing our business now that we don’t have to worry about doing
                                    everything manually.
                                </blockquote>
                                <div class="star-rating text-right"><i class="fa fa-star text-color"></i> <i
                                            class="fa fa-star text-color"></i> <i class="fa fa-star text-color"></i> <i
                                            class="fa fa-star text-color"></i> <i
                                            class="fa fa-star-half-o text-color"></i></div>
                            </div>
                            <div class="client-details text-center">
                                <div class="client-image">                                <!-- Image --> <img
                                            class="img-circle" src="img/OAG.png" width="80" height="80" alt=""/></div>
                                <div class="client-details">                                <!-- Name --> <strong
                                            class="text-color">Office Automation Group</strong>
                                    <!-- Company -->
                                    <!--                                    <span>Designer, zozothemes</span>-->
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="desc-border bottom-arrow quote">
                                <blockquote class="small-text">Flour mills in Pakistan mostly operate in the traditional
                                    way, using manual records. We are glad that we made the switch. It was becoming
                                    difficult to keep record of and manually manage production and tracking and our ERP
                                    solved those problems. Our ERP also includes HR functions such as payroll and
                                    employee asset management. This makes keeping account of our finances much easier!
                                </blockquote>
                                <div class="star-rating text-right"><i class="fa fa-star text-color"></i> <i
                                            class="fa fa-star text-color"></i> <i class="fa fa-star text-color"></i> <i
                                            class="fa fa-star text-color"></i> <i
                                            class="fa fa-star-half-o text-color"></i></div>
                            </div>
                            <div class="client-details text-center">
                                <div class="client-image">                                <!-- Image --> <img
                                            class="img-circle" src="img/Sufi.png" width="80" height="80" alt=""/></div>
                                <div class="client-details">                                <!-- Name --> <strong
                                            class="text-color">Sufi Flour Mills</strong>
                                    <!-- Company -->
                                    <!--                                    <span>Designer, zozothemes</span>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="clients" class="page-section tb-pad-30">
        <div class="container">
            <div class="section-title text-center" data-animation="fadeInUp">            <!-- Heading --> <h1
                        class="title"> Our Best Clients</h1></div>
            <div class="row">
                <div class="col-md-12 text-center" data-animation="fadeInDown">
                    <div class="owl-carousel navigation-1" data-pagination="false" data-items="6" data-autoplay="true"
                         data-navigation="true"><a> <img src="img/1.jpg" width="150" height="150" alt=""/> </a> <a> <img
                                    src="img/2.jpg" width="150" height="150" alt=""/> </a> <a> <img src="img/3.jpg"
                                                                                                    width="150"
                                                                                                    height="150"
                                                                                                    alt=""/> </a> <a>
                            <img src="img/5.jpg" width="150" height="150" alt=""/> </a> <a> <img src="img/6.jpg"
                                                                                                 width="150"
                                                                                                 height="150" alt=""/>
                        </a> <a> <img src="img/7.jpg" width="150" height="150" alt=""/> </a> <a> <img src="img/8.jpg"
                                                                                                      width="150"
                                                                                                      height="150"
                                                                                                      alt=""/> </a>
                        <!--                        <a href="#">-->
                        <!--                            <img src="img/sections/clients/2.png" width="150" height="150" alt="" />-->
                        <!--                        </a>-->                </div>
                </div>
            </div>
        </div>
    </section><?php include 'footer.php' ?>

<?php
if(isset($_GET['email_status']) == 'email_sent'){?>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>        swal("Good job!", "You clicked the button!", "success");    </script><?php } ?>