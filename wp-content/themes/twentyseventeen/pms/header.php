<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>NETROOTS PMS</title>
    <link rel="shortcut icon" href="favicon.ico"/>    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet'
          type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
    <!-- Custom styles for this template -->
    <link href="css/agency.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/custome.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
          integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
          integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Arimo:300,400,700,400italic,700italic'/>
    <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'/>
    <!-- Font Awesome Icons -->
    <link href='css/font-awesome.min.css' rel='stylesheet' type='text/css'/><!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <link href="css/hover-dropdown-menu.css" rel="stylesheet"/><!-- Icomoon Icons -->
    <link href="css/icons.css" rel="stylesheet"/><!-- Revolution Slider -->
    <link href="css/revolution-slider.css" rel="stylesheet"/>
    <link href="rs-plugin/css/settings.css" rel="stylesheet"/><!-- Animations -->
    <link href="css/animate.min.css" rel="stylesheet"/><!-- Owl Carousel Slider -->
    <link href="css/owl/owl.carousel.css" rel="stylesheet"/>
    <link href="css/owl/owl.theme.css" rel="stylesheet"/>
    <link href="css/owl/owl.transitions.css" rel="stylesheet"/><!-- PrettyPhoto Popup -->
    <link href="css/prettyPhoto.css" rel="stylesheet"/><!-- Custom Style -->
    <link href="css/style.css" rel="stylesheet"/>
    <link href="css/responsive.css" rel="stylesheet"><!-- Color Scheme -->
    <link href="css/color.css" rel="stylesheet"/>
    <link href="css/stylesheet hrm.css" rel="stylesheet"/>
    <link rel="stylesheet" href="css/hover.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" media="all">
</head>
<body id="page"><!---->
<div id="top-bar" class="top-bar-section top-bar-bg-color">
    <div class="container">
        <div class="row">
            <div class=" col-sm-12 text-center">                <!-- Top Contact -->
                <div class="top-contact link-hover-black"><a href="#"> <i class="fa fa-phone"></i> +1 (647) 490 6750</a>
                    <a href="#"> <i class="fa fa-envelope"></i> info@netrootstech.com</a></div>
            </div>
        </div>
    </div>
</div><!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav"
     style="    background-color: white;    box-shadow: 0 0 5px rgba(0, 0, 0, 0.15);  position: sticky; ">
    <div class="container"><a class="float-left navbar-brand js-scroll-trigger" href="../../"><img
                    src="img/logo (1).png" alt="" width="180"> </a>
        <button class="float-right navbar-toggler navbar-toggler-right ml-auto" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation"> Menu <i class="fas fa-bars"></i></button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav text-uppercase ml-auto">
                <li class="nav-item"><a class="nav-link js-scroll-trigger page-scroll" href="./">Home</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger page-scroll" href="#Modules">Modules</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger page-scroll" href="#Features">Why us</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger page-scroll" href="#get-quote">Follow us</a>
                </li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger page-scroll" href="blog.php">Blog</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger page-scroll" href="../contact-us.php">Contact
                        us</a></li>
                <a target="_blank" class="bt1 btn btn-warning" href="../contact-us.php">REQUEST FREE DEMO</a></ul>
        </div>
        <div style="float: right;font-weight: bold">
            <ul class="cd-header-buttons">
                <li>Talk To Our Experts<br> <i class="fas fa-mobile-alt"></i> <a href="tel:+13023001742"
                                                                                 target="_blank">+1 (302) 300 1742</a>
                </li>
                <li><a class="cd-nav-trigger" href="#cd-primary-nav"><span></span></a></li>
            </ul>
        </div>
    </div>
</nav>
<div class="sticky-container">
    <div class="sticky-container">    <?php include '../sticky.php'; ?>
        <style>        .sticky-number {
                color: black;
                text-decoration: underline;
            }    </style>
    </div>
    <style>        .sticky-number {
            color: black;
            text-decoration: underline;
        }    </style>
</div>