<?php include '../config.php'; ?>
<div id="get-quote" class="bg-color get-a-quote black text-center">
    <div class="container" data-animation="pulse">
        <div class="row">
            <div class="col-md-12"><p>Get A Free Quote / Need a Help ? <a class="black" href="../contact-us.php">Contact
                        Us</a></p></div>
        </div>
    </div>
</div>
<footer id="footer" style="padding: 0 0; font-family: Montserrat;">    <?php include '../one-footer.php'; ?></footer>
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script><!-- Plugin JavaScript -->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script><!-- Contact form JavaScript -->
<script src="js/jqBootstrapValidation.js"></script>
<script src="js/contact_me.js"></script><!-- Custom scripts for this template -->
<script src="js/agency.min.js"></script>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script><!-- Menu jQuery plugin -->
<script type="text/javascript" src="js/hover-dropdown-menu.js"></script><!-- Menu jQuery Bootstrap Addon -->
<script type="text/javascript" src="js/jquery.hover-dropdown-menu-addon.js"></script><!-- Scroll Top Menu -->
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script><!-- Sticky Menu -->
<script type="text/javascript" src="js/jquery.sticky.js"></script><!-- Bootstrap Validation -->
<script type="text/javascript" src="js/bootstrapValidator.min.js"></script><!-- Revolution Slider -->
<script type="text/javascript" src="rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="js/revolution-custom.js"></script><!-- Portfolio Filter -->
<script type="text/javascript" src="js/jquery.mixitup.min.js"></script><!-- Animations -->
<script type="text/javascript" src="js/jquery.appear.js"></script>
<script type="text/javascript" src="js/effect.js"></script><!-- Owl Carousel Slider -->
<script type="text/javascript" src="js/owl.carousel.min.js"></script><!-- Pretty Photo Popup -->
<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script><!-- Parallax BG -->
<script type="text/javascript" src="js/jquery.parallax-1.1.3.js"></script><!-- Fun Factor / Counter -->
<script type="text/javascript" src="js/jquery.countTo.js"></script><!-- Twitter Feed -->
<script type="text/javascript" src="js/tweet/carousel.js"></script>
<script type="text/javascript" src="js/tweet/scripts.js"></script>
<script type="text/javascript" src="js/tweet/tweetie.min.js"></script><!-- Background Video -->
<script type="text/javascript" src="js/jquery.mb.YTPlayer.js"></script><!-- Custom Js Code -->
<script type="text/javascript" src="js/custom.js"></script> <!--Start of Tawk.to Script-->
<script type="text/javascript">var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
    (function () {
        var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
        s1.async = true;
        s1.src = 'https://embed.tawk.to/5cc2d770ee912b07bec4f7fa/default';
        s1.charset = 'UTF-8';
        s1.setAttribute('crossorigin', '*');
        s0.parentNode.insertBefore(s1, s0);
    })();</script><!--End of Tawk.to Script-->
<script>    $('.navbar-collapse a').click(function (e) {
        if ($(e.target).is('.nav-item a') && $(e.target).attr('class') != 'dropdown-toggle') {
            $('.navbar-collapse.in').collapse('toggle');
        }
    });</script></body></html>