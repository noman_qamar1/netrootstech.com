\<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_template_part( 'template-parts/header/header', 'hrm' ); ?>
<!-- ------------------------------------------------template body start from here -->


<div class="top-banner">
    <div class="container">
        <div class="banner-content">
            <h1>
                Attendance Management
            </h1>
            <p>
                It’s because humans forget. But Fórsa HR doesn’t
            </p>


        </div>




    </div>



</div>

<!--banner ends-->


<!--module-area start-->

<div class="module-area">

    <div class="container">
        <h2>
            There’s a reason why the “Forgot Password” option exists on almost every website that requires a password.
        </h2>
        <p>
            It’s because humans forget. But Fórsa HR doesn’t
        </p>
        <div class="module-chart">
            <div class="row">
                <div class="col-md-4">

                    <div class="branches" data-aos="flip-up">

                        <a href="#">
                            <h4>
                                Attendance Management
                            </h4>
                            <p>
                                The attendance management sub-module allows you to keep a daily record of each employee’s attendance and time of attendance
                            </p>
                        </a>

                    </div>
                    <div class="branches" data-aos="flip-up">

                        <a href="#">
                            <h4>
                                Attendance Report
                            </h4>
                            <p>
                                The attendance report sub-module lets you extract attendance reports of employees of different departments so that you can evaluate their performance.
                            </p>
                        </a>

                    </div>

                </div>
                <div class="col-md-4">

                    <figure class="module-tablet"  data-aos="zoom-in">

                        <a href="#">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/images/module-tab.png" alt="dashboard" width="100%">
                        </a>

                    </figure>

                </div>
                <div class="col-md-4">

                    <div class="branches-right" data-aos="flip-up">
                        <a href="#">

                            <h4>
                                Attendance Summary
                            </h4>
                            <p>
                                This sub-module creates a summary of the monthly attendance of each employee, giving information such as average working hours, average time out, salary deductions and so on
                            </p>
                        </a>

                    </div>



                </div>


            </div>



        </div>



    </div>

</div>

<!--module-area ends-->


<!--content-wraper start-->

<div class="content-wraper">

    <div class="container">

        <div class="row">



            <div class="col-md-6">

                <div class="wraper-text"data-aos="fade-right"
                     data-aos-offset="300"
                     data-aos-easing="ease-in-sine">

                    <h2>
                        Accurate, simple, and easy to use
                    </h2>
                    <p>
                        Our HR software eliminates the human error in tracking employee attendance. It knows and remembers the times of attendance and exit and the hours worked by each employee. It also generates attendance reports, enables deductions for late arrivals, and lets you make inferences about employee performance. Because this module records the times of employee attendance and the total time worked by each employee, it helps the HRMS system’s payroll software calculate the pay that is due to each employee.



                    </p>
                    <p>
                        The attendance management software of NetRoots HRMS exceeds other attendance management solutions in its simplicity and ease-of-use

                    </p>




                </div>



            </div>
            <div class="col-md-6">
                <figure class="wraper-image" data-aos="zoom-in-up">

                    <a href="#">
                       <img src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/images/module-laptop.png" alt="dashboard" width="100%">
                    </a>

                </figure>

            </div>

        </div>



    </div>

</div>

<!--content-wraper end-->

<!--text area-->

<div class="text-area" data-aos="zoom-in-down">

    <div class="container">

        <h2>
            Why Use Fórsa Attendance Management Software?
        </h2>

        <p>

            Besides keep an accurate and reliable track of employee attendance and working hours, Fórsa attendance management software stands out due to its business intelligence component. Through its reporting feature, Fórsa helps you make effective and efficient business decisions.

        </p>


    </div>



</div>

<!-- ----------------------------------------------template end here -->
<?php
get_footer();