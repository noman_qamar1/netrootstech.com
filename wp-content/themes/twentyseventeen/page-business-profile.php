<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header(); ?>

<main class="cd-main-content">

    <style>    .pdf {
            padding-top: 120px;
        }

        /*iframe{*/ /*padding-top: 150px;*/ /*}*/</style>
    <!--<div class=" pdf container embed-responsive embed-responsive-21by9" style="padding-top: 250px">-->
    <!--    <iframe class="embed-responsive-item" src="assets/images/1.pdf" type="application/pdf" allowfullscreen width="100%">
    </iframe>-->
    <!--</div>-->
    <!--    <div class="pdf container  embed-responsive embed-responsive-1by1">-->
    <!--        <embed class="embed-responsive-item" src="https://drive.google.com/viewerng/-->
    <!--viewer?embedded=true&url=https://netrootstech.ca/assets/images/1.pdf" type="application/pdf" width="100%" height="550px;" style="margin-top: 100px;">-->
    <!--    </div>-->
    <div class="pdf container">
        <iframe src="https://docs.google.com/viewer?url=https://netrootstech.ca/assets/images/International-business-profile.pdf&embedded=true" frameborder="0" height="500px" width="100%"></iframe>

    </div>

</main>
<?php
get_footer();
