\<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_template_part( 'template-parts/header/header', 'hrm' ); ?>
<!-- ------------------------------------------------template body start from here -->


<div class="top-banner">
    <div class="container">
        <div class="banner-content">
            <h1>
                 Leave Management
            </h1>
            <p>
                With our mobile-compatible HR software,
            </p>
            <p>
                it will take you just minutes to put in a request for leave!
            </p>


        </div>




    </div>



</div>

<!--banner ends-->


<!--module-area start-->

<div class="module-area">

    <div class="container">
        <h2>
            Are you sick? Do you have to travel out of town for your best friend’s wedding?Are you sick? Do you have to travel out of town for your best friend’s wedding?
        </h2>
        <p>
            With our mobile-compatible HR software, it will take you just minutes to put in a request for leave!
        </p>
        <div class="module-chart">
            <div class="row">
                <div class="col-md-4">

                    <div class="branches" data-aos="flip-up">

                        <a href="#">
                            <h4>
                                Leave Request
                            </h4>
                            <p>
                                Fórsa leave management software allows employees to put in leave requests including information such as type of leave, reason for leave, and days of leave as well as documents for proof such as medical certfiicates. The management can then approve or decline the leave requests through the HR software.
                            </p>
                        </a>

                    </div>


                </div>
                <div class="col-md-4">

                    <figure class="module-tablet"  data-aos="zoom-in">

                        <a href="#">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/images/module-tab.png" alt="dashboard" width="100%">
                        </a>

                    </figure>

                </div>
                <div class="col-md-4">



                    <div class="branches" data-aos="flip-up">
                        <a href="#" class="text-left" style="text-align: left !important;">

                            <h4>
                                Leave Settings
                            </h4>
                            <p class="text-left">
                                The leave settings sub-module allows you to differentiate holidays and working days, and enumerate leaves for each type of employee, including for contractual and permanent employees.
                            </p>
                        </a>

                    </div>



                </div>


            </div>



        </div>



    </div>

</div>

<!--module-area ends-->


<!--content-wraper start-->

<div class="content-wraper">

    <div class="container">

        <div class="row">



            <div class="col-md-6">

                <div class="wraper-text"data-aos="fade-right"
                     data-aos-offset="300"
                     data-aos-easing="ease-in-sine">

                    <h2>
                        Easy and integrated
                    </h2>
                    <p>
                        As an employee, our leave management solutions make your life easier by letting you put in leave requests in a few easy clicks. If you’re a part of management, you can not only approve and decline leave requests but also keep track of how many and what type of leaves an employee has taken. The Leave Management module also communicates with the payroll software of the system to automatically calculate the pay owed to each employee.
                    </p>
                </div>



            </div>
            <div class="col-md-6">
                <figure class="wraper-image" data-aos="zoom-in-up">

                    <a href="#">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/images/module-laptop.png" alt="dashboard" width="100%">
                    </a>

                </figure>

            </div>

        </div>



    </div>

</div>

<!--content-wraper end-->

<!--text area-->

<div class="text-area" data-aos="zoom-in-down">

    <div class="container">

        <h2>
            Why Use Fórsa Leave Management Software?
        </h2>

        <p>

            Fórsa offers easy and convenient solutions for tedious leave management manual work. But that’s not all! If you have a sandwich leave policy in which leave is deducted for weekly off-days or holidays if the employee applies for leave on the day preceding and succeeding them, our HR software can entertain a sandwich leave system. Among other customizable features, the gazetted holidays and proof attachment provisions make Fórsa the best HR software out there.

        </p>


    </div>



</div>

<!-- ----------------------------------------------template end here -->
<?php
get_footer();