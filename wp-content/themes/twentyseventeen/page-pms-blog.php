<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_template_part( 'template-parts/header/header', 'pms' ); ?>


<div class="page-header" style="background-color: #4b98a7a6!important;color: white">
        <div class="container"><h1 class="title">Blog</h1></div>
        <div class="breadcrumb-box">
            <div class="container">
                <ul class="breadcrumb"><!--                    <li><a href="index.php">Home</a></li>-->
                    <!--                    <li><a href="page-ContactUs.php">Contact us</a></li>--><!---->
                    <!--                    <li class="active"> Blog</li>-->                </ul>
            </div>
        </div>
    </div>


<?php
// the query
$wpb_all_query = new WP_Query(array('post_type'=>'post', 'cat'=> 16 , 'post_status'=>'publish', 'posts_per_page'=>10)); ?>
<?php if ( $wpb_all_query->have_posts() ) : ?>
    <!-- the loop -->
    <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>


<section class="page-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12"><h2><?php the_title(); ?></h2>
                    <div class="post-image opacity">
                        <?php the_post_thumbnail();?>
                    </div>
                    <div class="post-meta">
                        <span class="author"><i class="fa fa-user"></i>
                            <?php the_author(); ?>
                        </span>                        <!-- Meta Date --> 
                        <span class="time"><i class="fa fa-calendar"></i> <?php the_date(); ?>
                    </span>
                        <!-- Comments --> 
                        <span class="category "><i class="fa fa-heart"></i> PMS, Technology, </span>

                        <!-- Category --> 
                        <span class="comments pull-right"><i class="fa fa-comments"></i>
                            <?php echo get_comments_number($post->ID);?>
                        </span>
                    </div>
                    <div class="post-content top-pad-20">
                        <p align="justify">
                            <?php echo the_content(); ?>
                        </p>
                        
                </div>
            </div>
        </div>
    </div>
    </section>

    <?php endwhile; ?>
    <?php wp_reset_postdata(); ?>
<?php else : ?>
    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>






<div id="get-quote" class="bg-color get-a-quote black text-center">
    <div class="container animated pulse visible" data-animation="pulse">
        <div class="row">
            <div class="col-md-12"><p>Get A Free Quote / Need a Help ? <a class="black" href="<?php echo get_page_link(47); ?>">Contact
                        Us</a></p></div>
        </div>
    </div>
</div>


<?php
get_footer();
