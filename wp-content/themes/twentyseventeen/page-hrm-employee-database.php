<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_template_part( 'template-parts/header/header', 'hrm' ); ?>

<!-- ------------------------------------------------template body start from here -->

<div class="top-banner">
    <div class="container">
        <div class="banner-content">
            <h1>
                Employee Database
            </h1>
            <p>
                Get a 360-degree overview of your employees through 
            </p>
            <p>
                the Fórsa HRMS employee database software
            </p>


        </div>




    </div>



</div>

<!--banner ends-->


<!--module-area start-->

<div class="module-area">

    <div class="container">
        <h2>
            Get a 360-degree overview of your employees through the Fórsa HRMS employee database software 
        </h2>
        <p>
           Fórsa HR software includes employee database solutions that help you record and track a wide range of employee information. Rather than being a simple contact list with employee names, identification numbers, and contact information, our employee database software gives a much broader insight into each employee. 
        </p>
        <div class="module-chart">
            <div class="row">
                <div class="col-md-4">

                    <div class="branches-Employee" data-aos="flip-up">

                        <a href="#">
                            <h4>
                               Employee Data Management
                            </h4>
                            <p>
                                Fórsa HR software stores not only the basic identification and contact information of employees, but also each employee’s qualifications, work history, position in the company, salary, and performance indicators on one organized platform.
                            </p>
                        </a>

                    </div>

                  

                </div>
                <div class="col-md-4">

                    <figure class="module-tablet"  data-aos="zoom-in">

                        <a href="#">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/images/employee-tab.png" alt="dashboard" width="100%">
                        </a>

                    </figure>

                </div>
                <div class="col-md-4">

                    <div class="branches-right-Employee" data-aos="flip-up">
                        <a href="#">

                            <h4>
                               Performance Analysis
                            </h4>
                            <p>
                                Using information stored on the database, the software enables you to draw inferences about the strengths, weaknesses, and requirements of each employee, allowing efficient task allocation. The information stored in the database also simplifies decisions related to training, development, and promotion.
                            </p>
                        </a>

                    </div>

                   



                </div>


            </div>



        </div>



    </div>

</div>

<!--module-area ends-->


<!--content-wraper start-->

<div class="content-wraper">

    <div class="container">

        <div class="row">



            <div class="col-md-6">

                <div class="wraper-text"data-aos="fade-right"
                     data-aos-offset="300"
                     data-aos-easing="ease-in-sine">

                    <h2>
                        Simple, Efficient, Integrated
                    </h2>
                    <p>
                       Our HR software is highly integrated, with information from the employee database software being shared with the other modules including the payroll software. Indicators of employee performance like probation evaluations can prove useful for the calculation of salary at the end of each month.
                    </p>
                    


                </div>



            </div>
            <div class="col-md-6">
                <figure class="wraper-image" data-aos="zoom-in-up">

                    <a href="#">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/images/employee-laptop.png" alt="dashboard" width="100%">
                    </a>

                </figure>

            </div>

        </div>



    </div>

</div>

<!--content-wraper end-->

<!--text area-->

<div class="text-area" data-aos="zoom-in-down">

    <div class="container">

        <h2>
            Why use Fórsa’s Employee Database Software? 
        </h2>

        <p>

           In addition to a host of benefits with regard to decision-making and task allocation, Fórsa employee database software acts as a safe storage space for a large amount of information that would be otherwise stored in either paper files or Excel sheets. Providing a centralized database for all employee information, our HR software ensures that information is not lost or compromised, while also saving physical space and paper. Thanks to the presence of all relevant employee information in one centralized database, making important business decisions is simplified as you can get an overview of the fortes and limitations of your employees.

        </p>


    </div>



</div>


<!-- ----------------------------------------------template end here -->

<script>
	AOS.init();
	</script>
<?php
get_footer();
