<!DOCTYPE html>
<html lang="en">
<head  <?php language_attributes(); ?> class="no-js no-svg no_margin">

    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <?php wp_head(); ?>

    <link rel="shortcut icon" href="favicon.ico"/>    <!-- Font -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
          integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Arimo:300,400,700,400italic,700italic'/>
    <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'/>
    <!-- Font Awesome Icons -->
    <link href='<?php echo get_template_directory_uri(); ?>/assets/crm-assets/css/font-awesome.min.css' rel='stylesheet' type='text/css'/>    <!-- Bootstrap core CSS -->
    <link href='<?php echo get_template_directory_uri(); ?>/assets/crm-assets/css/bootstrap.min.css' rel="stylesheet"/>
    <link href='<?php echo get_template_directory_uri(); ?>/assets/crm-assets/css/hover-dropdown-menu.css' rel="stylesheet"/>    <!-- Icomoon Icons -->
    <link href='<?php echo get_template_directory_uri(); ?>/assets/crm-assets/css/icons.css' rel="stylesheet"/>    <!-- Revolution Slider -->
    <link href='<?php echo get_template_directory_uri(); ?>/assets/crm-assets/css/revolution-slider.css' rel="stylesheet"/>
    <link href='<?php echo get_template_directory_uri(); ?>/assets/crm-assets/rs-plugin/css/settings.css' rel="stylesheet"/>    <!-- Animations -->
    <link href='<?php echo get_template_directory_uri(); ?>/assets/crm-assets/css/animate.min.css' rel="stylesheet"/>    <!-- Owl Carousel Slider -->
    <link href='<?php echo get_template_directory_uri(); ?>/assets/crm-assets/css/owl/owl.carousel.css' rel="stylesheet"/>
    <link href='<?php echo get_template_directory_uri(); ?>/assets/crm-assets/css/owl/owl.theme.css' rel="stylesheet"/>
    <link href='<?php echo get_template_directory_uri(); ?>/assets/crm-assets/css/owl/owl.transitions.css' rel="stylesheet"/>    <!-- PrettyPhoto Popup -->
    <link href='<?php echo get_template_directory_uri(); ?>/assets/crm-assets/css/prettyPhoto.css' rel="stylesheet"/>    <!-- Custom Style -->
    <link href='<?php echo get_template_directory_uri(); ?>/assets/crm-assets/css/style.css' rel="stylesheet"/>
    <link href='<?php echo get_template_directory_uri(); ?>/assets/crm-assets/css/responsive.css' rel="stylesheet">    <!-- Color Scheme -->
    <link href='<?php echo get_template_directory_uri(); ?>/assets/crm-assets/css/color.css' rel="stylesheet"/>
    <!--<link href='<?php /*echo get_template_directory_uri(); */?>/assets/crm-assets/css/stylesheet-hrm.css' rel="stylesheet"/>-->
    <link rel="stylesheet" href='<?php echo get_template_directory_uri(); ?>/assets/crm-assets/css/hover.css'>
    <link rel="stylesheet" href='<?php echo get_template_directory_uri(); ?>/assets/crm-assets/css/query-resposive.css'>
    <link rel="stylesheet" href='<?php echo get_template_directory_uri(); ?>/assets/erp-assets/css/custom-style.css'>




    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/crm-assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/crm-assets/js/bootstrap.min.js"></script><!-- Menu jQuery plugin -->
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/crm-assets/js/hover-dropdown-menu.js"></script><!-- Menu jQuery Bootstrap Addon -->
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/crm-assets/js/jquery.hover-dropdown-menu-addon.js"></script><!-- Scroll Top Menu -->
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/crm-assets/js/jquery.easing.1.3.js"></script><!-- Sticky Menu -->
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/crm-assets/js/jquery.sticky.js"></script><!-- Bootstrap Validation -->
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/crm-assets/js/bootstrapValidator.min.js"></script><!-- Revolution Slider -->
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/crm-assets/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/crm-assets/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/crm-assets/js/revolution-custom.js"></script><!-- Portfolio Filter -->
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/crm-assets/js/jquery.mixitup.min.js"></script><!-- Animations -->
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/crm-assets/js/jquery.appear.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/crm-assets/js/effect.js"></script><!-- Owl Carousel Slider -->
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/crm-assets/js/owl.carousel.min.js"></script><!-- Pretty Photo Popup -->
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/crm-assets/js/jquery.prettyPhoto.js"></script><!-- Parallax BG -->
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/crm-assets/js/jquery.parallax-1.1.3.js"></script><!-- Fun Factor / Counter -->
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/crm-assets/js/jquery.countTo.js"></script><!-- Twitter Feed -->
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/crm-assets/js/tweet/carousel.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/crm-assets/js/tweet/scripts.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/crm-assets/js/tweet/tweetie.min.js"></script><!-- Background Video -->
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/crm-assets/js/jquery.mb.YTPlayer.js"></script><!-- Custom Js Code -->
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/crm-assets/js/custom.js"></script> <!--Start of Tawk.to Script-->

    <style>
    .navbar-brand{
   
   margin-top:-7px;
  padding-left:49px;
}
    </style>
</head>
<body id="page"  <?php body_class(); ?>>

<div id="top-bar" class="top-bar-section top-bar-bg-color">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">                <!-- Top Contact -->
                <div class="top-contact text-center"><a href="#"> <i class="fa fa-phone"></i> +1 (647) 490 6750</a> <a
                            href="#"> <i class="fa fa-envelope"></i> info@netrootstech.com</a></div>
            </div>
        </div>
    </div>
</div>
<div id="home">    <!-- Page Loader -->
    <!--<div id="pageloader">        <div class="loader-item fa fa-spin text-color"></div>    </div>-->
    <!-- Fixed Navbar -->
    <div id="sticker-sticky-wrapper" class="sticky-wrapper">
        <header id="sticker" class="sticky-navigation">            <!-- Sticky Menu -->
            <div class="sticky-menu relative">                <!-- navbar -->
                <div class="navbar navbar-default navbar-bg-light" role="navigation">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="navbar-header">
                                    <!-- Button For Responsive toggle -->
                                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                                            data-target=".navbar-collapse"><span
                                                class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
                                        <span class="icon-bar"></span> <span class="icon-bar"></span></button>

                                    <!-- Logo --><a class="navbar-brand" href="../../"> <img style="padding-top: 8px;"
                                                                                             class="site_logo"
                                                                                             alt="Site Logo"
                                                                                             src="<?php echo get_template_directory_uri(); ?>/assets/crm-assets/img/logo (1).png">
                                    </a></div>                                <!-- Navbar Collapse -->
                                <!-- Navbar Collapse -->
                                <div style="float: right;">
                                    <ul class="cd-header-buttons" style="margin-top: 37px; margin-right: -8px;">

                                        <li>Talk To Our Experts<br> <i class="fas fa-mobile-alt"></i> <a
                                                    href="tel:+13023001742" target="_blank">+1 (302) 300 1742</a></li>
                                        <li><a class="cd-nav-trigger" href="#cd-primary-nav"><span></span></a></li>
                                    </ul>
                                </div>
                                <div class="navbar-collapse collapse">                                    <!-- nav -->
                                    <ul class="nav navbar-nav sm" data-smartmenus-id="15547838803600474">
                                        <!-- Home  Mega Menu -->
                                        <li class="mega-menu page-scroll"><a data-toggle="collapse" data-target=".navbar-collapse.in"
                                            href="<?php echo get_page_link(119);?>">Home</a></li>
                                        <!-- Mega Menu Ends -->
                                        <!-- Pages Mega Menu -->
                                        <!--											<li class="mega-menu page-scroll"> <a href="#Modules">Modules</a> </li>-->
                                        <!-- Pages Menu Ends -->
                                        <li class="dropdown"><a data-toggle="collapse" data-target=".navbar-collapse.in"
                                               class="nav-link dropdown-toggle js-scroll-trigger page-scroll"
                                               href="<?php echo get_page_link(119);?>#Modules" id="navbarDropdown" role="button"
                                               data-hover="dropdown" aria-haspopup="true"
                                               aria-expanded="false"> Modules
                                               <i class="fas fa-caret-down"></i> </a>



                                            <?php wp_nav_menu( array(
                                                'theme_location'         => 'Crm-Module-Menu',
                                                'menu_id'           => '',
                                                'items_wrap'        => '<ul class="dropdown-menu" aria-labelledby="navbarDropdown">%3$s</ul>',
                                                'container'         => ''
                                            ) ); ?>

                                        </li>                                        <!-- Portfolio Menu -->
                                        <li class="page-scroll"><a data-toggle="collapse"
                                                                   data-target=".navbar-collapse.in" href="./#Features">Why
                                                us</a></li>
                                        <!-- Portfolio Menu -->
                                        <!-- Shop Menu -->
                                        <li class="page-scroll"><a data-toggle="collapse"
                                                                   data-target=".navbar-collapse.in"
                                                                   href="<?php echo get_page_link(119);?>#get-quote">Follow us</a>
                                            <!-- Shop Dropdown Menu -->
                                            <!-- Ends Shop Dropdown Menu -->
                                        </li>                                        <!-- Ends Shop Menu -->
                                        <!-- Features Menu -->
                                        <li class="page-scroll"><a data-toggle="collapse"
                                                                   data-target=".navbar-collapse.in"
                                                                   href="<?php echo get_page_link(121); ?>">Blog</a></li>
                                        <!-- Ends Features Menu -->
                                        <!-- Blog Menu -->
                                        <li class="page-scroll"><a data-toggle="collapse"
                                                                   data-target=".navbar-collapse.in"
                                                                   href="<?php echo get_page_link(47); ?>">Contact us</a></li>
                                        <!-- Contact Block -->
                                        <button type="button" class="btn btn-warning"><a data-toggle="collapse"
                                                                                         data-target=".navbar-collapse.in"
                                                                                         href="<?php echo get_page_link(47); ?> ">REQUEST
                                                     DEMO</a></button>
                                    </ul>                                    <!-- Right nav -->
                                    <!-- Header Contact Content -->
                                    <div class="bg-white hide-show-content no-display header-contact-content"
                                         style="display: none;"><p class="vertically-absolute-middle">Call Us <strong><a
                                                        href="tel:+16474906750" target="_blank">+1 (647) 490
                                                    6750</a></strong></p>
                                        <button class="close"><i class="fa fa-times"></i></button>
                                    </div>                                    <!-- Header Contact Content -->
                                    <div class=" bg-white hide-show-content no-display header-share-content"
                                         style="display: none;">
                                        <div class="vertically-absolute-middle social-icon gray-bg icons-circle i-3x"><a
                                                    href="https://www.facebook.com/pg/Netrootstech-CRM-2368281133457121/about/?ref=page_internal">
                                                <i class="fa fa-facebook"></i> </a>
                                            <!--                                            <a href="#">-->
                                            <!--                                                <i class="fa fa-twitter"></i>-->
                                            <!--                                            </a>--><a
                                                    href="https://www.pinterest.ie/netrootstechcrm/boards/"> <i
                                                        class="fa fa-pinterest"></i> </a> <a
                                                    href="https://www.linkedin.com/company/14605525/admin/"> <i
                                                        class="fa fa-linkedin"></i> </a></div>
                                        <button class="close"><i class="fa fa-times"></i></button>
                                    </div>                                    <!-- Header Share Content -->
                                </div>                                <!-- /.navbar-collapse -->
                            </div>                            <!-- /.col-md-12 -->                        </div>
                        <!-- /.row -->                    </div>                    <!-- /.container -->
                </div>                <!-- navbar -->            </div>            <!-- Sticky Menu -->        </header>
    </div>

    <div class="sticky-container">
    <?php get_template_part( 'template-parts/', 'sticky.php' );?>    
</div>
    <script>
        if (window.location.href.includes("div" + "")) {
            $(document).scrollTop(0);
        }    
    </script>

