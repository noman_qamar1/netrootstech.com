<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php wp_head(); ?>

<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>//assets/hrm-assets/css/font-awesome.min.css">
<!--<link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Arimo:300,400,700,400italic,700italic">
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Oswald:400,300,700">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
<link rel="stylesheet" type="text/css" href="https://unpkg.com/aos@2.3.1/dist/aos.css">-->
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>//assets/hrm-assets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>//assets/hrm-assets/css/hover-dropdown-menu.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>//assets/hrm-assets/css/icons.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>//assets/hrm-assets/css/revolution-slider.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>//assets/hrm-assets/rs-plugin/css/settings.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>//assets/hrm-assets/css/animate.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>//assets/hrm-assets/css/owl/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>//assets/hrm-assets/css/owl/owl.theme.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>//assets/hrm-assets/css/owl/owl.transitions.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>//assets/hrm-assets/css/prettyPhoto.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>//assets/hrm-assets/css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>//assets/hrm-assets/css/responsive.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>//assets/hrm-assets/css/color.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>//assets/hrm-assets/css/stylesheet hrm.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>//assets/hrm-assets/css/hover.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>//assets/hrm-assets/css/query-resposive.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>//assets/hrm-assets/css/module.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>//assets/hrm-assets/css/documents.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>//assets/hrm-assets/css/employe-database.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>//assets/hrm-assets/css/exit.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>//assets/hrm-assets/css/payroll.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>//assets/hrm-assets/css/performance.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>//assets/hrm-assets/css/recruitment-module.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>//assets/hrm-assets/css/training.css">
	<?php
 if (is_page( 447 )) {
        wp_enqueue_style('documents', get_theme_file_uri('/assets/hrm-assets/css/documents.css'));
    }
    if ( is_page( 449 )) {
        wp_enqueue_style('employe-database', get_theme_file_uri('/assets/hrm-assets/css/employe-database.css'));
    }
    if ( is_page( 451 )) {
        wp_enqueue_style('exit', get_theme_file_uri('/assets/hrm-assets/css/exit.css'));
    }
    if ( is_page( 453 )) {
        wp_enqueue_style('payroll', get_theme_file_uri('/assets/hrm-assets/css/payroll.css'));
    }
    if ( is_page( 455 )) {
        wp_enqueue_style('performance', get_theme_file_uri('/assets/hrm-assets/css/performance.css'));
    }
    if ( is_page( 457 )) {
        wp_enqueue_style('recruitment-module', get_theme_file_uri('/assets/hrm-assets/css/recruitment-module.css'));
    }
    if ( is_page( 459 )) {
	    wp_enqueue_style( 'training', get_theme_file_uri( '/assets/hrm-assets/css/hrm-assets/training.css' ) );
    }

?>

 
<script type="text/javascript" src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script type="text/javascript" src="https://kit.fontawesome.com/c14c0137f1.js"></script>
<script type="text/javascript" src="https://cdn.widgetwhats.com/script.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/js/hover-dropdown-menu.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/js/jquery.hover-dropdown-menu-addon.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/js/jquery.sticky.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/js/bootstrapValidator.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/js/revolution-custom.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/js/jquery.mixitup.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/js/jquery.appear.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/js/effect.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/js/jquery.prettyPhoto.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/js/jquery.parallax-1.1.3.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/js/jquery.countTo.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/js/tweet/carousel.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/js/tweet/scripts.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/js/tweet/tweetie.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/js/jquery.mb.YTPlayer.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/js/custom.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/js/popper.min.js"></script>

<script>
	AOS.init();
	</script>

<style>
.navbar-brand{
   
    margin-top:-2px;
   padding-left:49px;
}



.navbar-default {
    background-color: white;
    border-color: #1c7992;
}

.banner{
    padding: 100px 0;
}
.banner h1{
    font-size:80px;
}
.banner h2{
    font-size:70px;
}

.navbar-default .navbar-nav > li > a {
    color: black;
}

.navbar-nav > li > a {
    font-size: 16px;
}

.navbar {
    min-height: 50px;
    margin-bottom: 0;
    border: 1px solid transparent;
    padding-top: 10px;
    padding-bottom: 10px;
    top: 0;
    position: sticky;
    z-index: 1;
    box-shadow: 0px 1px 10px #999;
    border-radius: 0;
    height: 123px;
    border-bottom: 2px solid #007488;
    width: 100%;
}

header {
    width: 100%;
    padding: 0 0;
}
.cd-header-buttons{
    position:absolute;
    font-family:Arimo, sans-serif;
    line-height: 1.2;
    margin-right: 5px;


}

.btn-warning {
    color: #fff!important;
    background:#FF9800!important;

}
.btn-warning:hover{
    color:#fff!important;
}

.navbar-default .navbar-nav > li > a:hover {
    color: #007488;
}
.site_logo{
    margin-top:7.1px;
}


</style>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>


<div id="page">    <!-- Page Loader -->
    <div id="pageloader">
        <div class="loader-item fa fa-spin text-color"></div>
    </div>
    <div id="top-bar" class="top-bar-section top-bar-bg-color">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">                        <!-- Top Contact -->
                    <div class="top-contact link-hover-black"><a href="#"> <i class="fa fa-phone"></i>+1 (647) 490 6750</a>
                        <a href="#"> <i class="fa fa-envelope"></i>info@netrootstech.com</a></div>
                </div>
            </div>
        </div>
    </div>    <!-- Top Bar -->    <!-- Sticky Navbar -->
    <header id="sticker" class="sticky-navigation">        <!-- Sticky Menu -->
        <div class="sticky-menu relative">            <!-- navbar -->
            <div class="navbar navbar-default navbar-bg-light" role="navigation">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="navbar-header">
                                <!-- Button For Responsive toggle -->
                                <button type="button" class="navbar-toggle" data-toggle="collapse"
                                        data-target=".navbar-collapse"><span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span> <span class="icon-bar"></span> <span
                                            class="icon-bar"></span></button>
                                <!-- Logo --><a class="navbar-brand" href="https://netrootstech.com/"> 
                                	<img class="site_logo side-logopage" alt="Site Logo" width="190"height="86" 
                                	src="<?php echo get_template_directory_uri(); ?>/assets/crm-assets/img/logo (1).png"/>

                                </a>
                            </div>                            <!-- Navbar Collapse -->
                            <div class="navbar-collapse collapse">                                <!-- nav -->
                                <ul class="nav navbar-nav">                                    <!-- Home  Mega Menu -->
                                    <li class="mega-menu page-scroll">
                                        <a href="<?php echo get_permalink(441); ?>">Home</a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="<?php echo get_permalink(441); ?>#Modules" id="navbarDropdown" role="button" data-hover="dropdown"
                                                aria-haspopup="true" aria-expanded="false"> Modules
                                                <i class="fas fa-caret-down"></i> 
                                                </a>
                                       <?php wp_nav_menu( array(
                                'theme_location'    => 'Hrm-Module-Menu',
                           'items_wrap' => '<ul class="dropdown-menu" aria-labelledby="navbarDropdown">%3$s</ul>',
                                        'container'   => ''
                                            ) ); ?>
										
                                    </li>
                                    <li class="mega-menu page-scroll">
                                        <a href="<?php echo get_permalink(441); ?>#whyus">Why us</a>
                                    </li>
                                  
                   <li class="mega-menu page-scroll">
					   <a href="<?php echo get_permalink(441); ?>#fellowus">Follow us</a>
									</li>
                   <li class="page-scroll">
					   <a href="<?php echo get_permalink(494); ?>">Careers</a>
									</li> 
					<li class="page-scroll">
						<a href="<?php echo get_permalink(488); ?>">Blog</a>
									</li>
                     <li class="mega-menu page-scroll">
                                        <a href="<?php echo get_permalink(47); ?>">Contact us</a>
                                    </li>
                                    <!-- Ends Widgets Block --> 
                  <a class="btn btn-warning"style="color: #fff"  href="<?php echo get_permalink(47); ?>">
                                        REQUEST DEMO
                                    </a>
                                </ul>                                

                                <!-- Right nav -->
                                <!-- Header Contact Content -->
                                <div class="bg-white hide-show-content no-display header-contact-content"><p
                                            class="vertically-absolute-middle">Call Us <strong>+0 (123) 456 78
                                            90</strong></p>
                                    <button class="close"><i class="fa fa-times"></i></button>
                                </div>                                <!-- Header Contact Content -->
                                <!-- Header Search Content -->
                                <div class="bg-white hide-show-content no-display header-search-content">
                                    <form role="search" class="navbar-form vertically-absolute-middle">
                                        <div class="form-group">
                                            <input type="text" placeholder="Enter your text &amp; Search Here" class="form-control" id="s" name="s" value=""/>
                                        </div>
                                    </form>
                                    <button class="close">
                                        <i class="fa fa-times">
                                            
                                        </i></button>
                                </div>                                <!-- Header Search Content -->
                                <!-- Header Share Content -->
                                <div class="bg-white hide-show-content no-display header-share-content">
                                    <div class="vertically-absolute-middle social-icon gray-bg icons-circle i-3x"><a
                                                href="#"> <i class="fa fa-facebook"></i> </a> <a href="#"> <i
                                                    class="fa fa-twitter"></i> </a> <a href="#"> <i
                                                    class="fa fa-pinterest"></i> </a> <a href="#"> <i
                                                    class="fa fa-google"></i> </a> <a href="#"> <i
                                                    class="fa fa-linkedin"></i> </a></div>
                                    <button class="close"><i class="fa fa-times"></i></button>
                                </div>                                <!-- Header Share Content -->
                            </div>                            <!-- /.navbar-collapse -->
                            <div style="float: right;">
                                <ul class="cd-header-buttons" style="margin-top: 29px;">
                                    <li>Talk To Our Experts<br> <i class="fas fa-mobile-alt"></i> <a
                                                href="tel:+13023001742" target="_blank">+1 (302) 300 1742</a></li>
                                    <li><a class="cd-nav-trigger" href="#cd-primary-nav"><span></span></a></li>
                                </ul>
                            </div>
                        </div>                     
                         </div>
                        </div>           
                      </div>
               </div>     
       </header>

<div class="sticky-container">
        <div class="sticky-container">    
            <style>        .sticky-number {
                    color: black;
                    text-decoration: underline;
                }    </style>
        </div>
        <style>            .sticky-number {
                color: black;
                text-decoration: underline;
            }        </style>
    </div> 
   
</div>
<script type="text/javascript">var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
    (function () {
        var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
        s1.async = true;
        s1.src = 'https://embed.tawk.to/5cc2d770ee912b07bec4f7fa/default';
        s1.charset = 'UTF-8';
        s1.setAttribute('crossorigin', '*');
        s0.parentNode.insertBefore(s1, s0);
    })();</script>