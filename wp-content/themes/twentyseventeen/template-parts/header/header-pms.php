<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Online Project management software | NETROOTS PMS</title>
    <meta name="Description"
          content="At NetRoots, we believe in the transformative power of education. We also understand the obstacles that prevent educational institutes from focusing exclusively on teaching and learning.">
    <meta name="Keywords"
          content="online school management software, school management software, school management software price list, how to make school management software, school management software uk, school information management systems">

<?php wp_head(); ?>

    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/favicon.ico"/>            <!-- Bootstrap core CSS -->
    <link href="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Custom fonts for this template -->
    <link href="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet'
          type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
    <link href="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/css/landing-page.min.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/css/landing-page.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/css/stylish-portfolio.min.css" rel="stylesheet">        <!-- Custom styles for this template -->
    <link href="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/css/agency.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/css/custome.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
          integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Arimo:300,400,700,400italic,700italic'/>
    <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'/>
    <!-- Font Awesome Icons -->
    <link href="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/css/font-awesome.min.css" rel='stylesheet' type='text/css'/>        <!-- Bootstrap core CSS -->
    <link href="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/css/hover-dropdown-menu.css" rel="stylesheet"/>        <!-- Icomoon Icons -->
    <link href="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/css/icons.css" rel="stylesheet"/>        <!-- Revolution Slider -->
    <link href="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/css/revolution-slider.css" rel="stylesheet"/>
    <link href="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/rs-plugin/css/settings.css" rel="stylesheet"/>        <!-- Animations -->
    <link href="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/css/animate.min.css" rel="stylesheet"/>        <!-- Owl Carousel Slider -->
    <link href="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/css/owl/owl.carousel.css" rel="stylesheet"/>
    <link href="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/css/owl/owl.theme.css" rel="stylesheet"/>
    <link href="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/css/owl/owl.transitions.css" rel="stylesheet"/>        
    <!-- PrettyPhoto Popup -->
    <link href="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/css/prettyPhoto.css" rel="stylesheet"/>        <!-- Custom Style -->
    <link href="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/css/style.css" rel="stylesheet"/>
    <link href="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/css/responsive.css" rel="stylesheet">        <!-- Color Scheme -->
    <link href="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/css/color.css" rel="stylesheet"/>
    <link href="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/css/stylesheet hrm.css" rel="stylesheet"/>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/css/hover.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/css2/custome.css">



    <!-- js including -->

    <script src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script><!-- Plugin JavaScript -->
<script src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/vendor/jquery-easing/jquery.easing.min.js"></script><!-- Contact form JavaScript -->
<script src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/js/jqBootstrapValidation.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/js/contact_me.js"></script><!-- Custom scripts for this template -->
<script src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/js/agency.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/js/bootstrap.min.js"></script><!-- Menu jQuery plugin -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/js/hover-dropdown-menu.js"></script><!-- Menu jQuery Bootstrap Addon -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/js/jquery.hover-dropdown-menu-addon.js"></script><!-- Scroll Top Menu -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/js/jquery.easing.1.3.js"></script><!-- Sticky Menu -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/js/jquery.sticky.js"></script><!-- Bootstrap Validation -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/js/bootstrapValidator.min.js"></script><!-- Revolution Slider -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/js/revolution-custom.js"></script><!-- Portfolio Filter -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/js/jquery.mixitup.min.js"></script><!-- Animations -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/js/jquery.appear.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/js/effect.js"></script><!-- Owl Carousel Slider -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/js/owl.carousel.min.js"></script><!-- Pretty Photo Popup -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/js/jquery.prettyPhoto.js"></script><!-- Parallax BG -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/js/jquery.parallax-1.1.3.js"></script><!-- Fun Factor / Counter -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/js/jquery.countTo.js"></script><!-- Twitter Feed -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/js/tweet/carousel.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/js/tweet/scripts.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/js/tweet/tweetie.min.js"></script><!-- Background Video -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/js/jquery.mb.YTPlayer.js"></script><!-- Custom Js Code -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/js/custom.js"></script><!--Start of Tawk.to Script-->

<style>
.navbar-brand img{
    margin-left:41px;
    margin-top:-23px;
}
ul.cd-header-buttons {
    margin-right:6px;
    top:-25px;
    font-family:Arimo, sans-serif!important;
    font-weight: 400;
     line-height: 1.2;

   
}
 

</style>
</head>
<body id="page"  <?php body_class(); ?>>
<div id="top-bar" class="top-bar-section top-bar-bg-color">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="top-contact link-hover-black"><a href="#"> <i class="fa fa-phone"></i>+1 (647) 490 6750</a>
                    <a href="#"> <i class="fa fa-envelope"></i>info@netrootstech.com</a></div>
            </div>
        </div>
    </div>
</div><!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav" style="background-color: white;">
    <div class="container-fluid"><a class="navbar-brand js-scroll-trigger" href="../../"><img src="<?php echo get_template_directory_uri(); ?>/assets/crm-assets/img/logo (1).png" alt=""
                                                                                        width="180"> </a>
        <button class=" navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"> Menu <i
                    class="fas fa-bars"></i></button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav text-uppercase ml-auto" style="margin-left:165px !important;">
                <li class="nav-item page-scroll"><a class="nav-link js-scroll-trigger page-scroll"href="<?php echo get_page_link(268); ?>">Home</a></li>
                <li class="nav-item page-scroll"><a class="nav-link js-scroll-trigger page-scroll"href="./#module">Modules</a></li>
             
                <li class="nav-item page-scroll"><a class="nav-link js-scroll-trigger page-scroll" href="<?php echo get_page_link(268); ?>#Features">Why
                        us</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger page-scroll" href="<?php echo get_page_link(268); ?>#get-quote">Follow us</a>
                </li>
                <li class="nav-item page-scroll"><a class="nav-link js-scroll-trigger page-scroll"
                                                    href="<?php echo get_page_link(285); ?>">Blog</a></li>
                <li class="nav-item page-scroll"><a class="nav-link js-scroll-trigger page-scroll"
                                                    href="<?php echo get_page_link(47); ?>">Contact us</a></li>
                <li>
                    <button style="    margin-top: 14px;" type="button" class="crmbtn btn btn-warning"><a
                                href="<?php echo get_page_link(47); ?>">REQUEST DEMO</a></button>
                </li>
            </ul>
        </div>
         <div>
            <ul class="cd-header-buttons">
                <li>Talk To Our Experts<br> <i class="fas fa-mobile-alt"></i> <a href="tel:+13023001742"
                                                                                 target="_blank">+1 (302) 300 1742</a>
                </li>
                <li><a class="cd-nav-trigger" href="#cd-primary-nav"><span></span></a></li>
            </ul>
        </div>
    </div>
</nav>

 <div class="sticky-container">
    <?php get_template_part( 'template-parts/', 'sticky.php' );?>    
</div>
    <script>
        if (window.location.href.includes("div" + "")) {
            $(document).scrollTop(0);
        }    
    </script>
