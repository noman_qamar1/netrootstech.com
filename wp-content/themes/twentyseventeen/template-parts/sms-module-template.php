<?php
/*
Template Name: sms - Module Layout
Template Post Type:  page
*/
// Page code here...

get_template_part( 'template-parts/header/header', 'sms' );
?>

<section id="service" class="page-section">
    <div class="container">

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <div class="row top-pad-30">

            <div class="col-md-6 col-sm-6 text-center animated fadeInLeft visible" data-animation="fadeInLeft">
                <!-- Image -->
                <?php the_post_thumbnail('full'); ?>
            </div>
            <div class="col-md-6 col-sm-6 animated fadeInRight visible" data-animation="fadeInRight">
                <h3><?php the_title(); ?></h3>
                <p align="justify" class="lead">
                    <?php the_content(); ?>
                </p>

            </div>
        </div>


        <?php
             endwhile;
                         else: ?>
        <p>Sorry, no posts matched your criteria.</p>
        <?php endif; ?>
    </div>
</section>

<?php
get_footer(); ?>
