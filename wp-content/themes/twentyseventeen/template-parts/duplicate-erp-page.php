<?php
/*
Template Name: Duplicate erp Page
Template Post Type:  page
*/
// Page code here...


get_template_part('template-parts/header/header', 'erp'); ?>

    <head>

        <link href="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/css/responsive.css" rel="stylesheet"
              type="text/css">
        <link href="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/css/stylesheet.css" rel="stylesheet"
              type="text/css">
        <link href="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/css/bootstrap.min.css"
              rel="stylesheet">
        <link href="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/css/owl.carousel.min.css"
              rel="stylesheet">
        <link href="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/css/owl.theme.default.min.css"
              rel="stylesheet">
        <link href="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/animate.css-master/animate.min.css"
              rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
              integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay"
              crossorigin="anonymous">
        <link href="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/css/font-awesome.min.css"
              rel="stylesheet" type="text/css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/js/bootstrap.min.js"></script>
        <style>        .navbar-default {
                background-color: white;
                border-color: #1c7992;
            }

            .navbar-default .navbar-nav > li > a {
                color: black;
            }

            .navbar-nav > li > a {
                font-size: 16px;
            }

            .navbar {
                min-height: 50px;
                margin-bottom: 0;
                border: 1px solid transparent;
                padding-top: 10px;
                padding-bottom: 10px;
                top: 0;
                position: sticky;
                z-index: 1;
                box-shadow: 0px 1px 10px #999;
                border-radius: 0;
                height: 123px;
                border-bottom: 2px solid #007488;
                width: 100%;
            }

            header {
                width: 100%;
                padding: 0 0;
            }

            .btn-warning {
                color: #fff;
                background-color: #FF9800;
                border-color: #FF9800;
                margin-top: 8px;
            }

            .navbar-default .navbar-nav > li > a:hover {
                color: #007488;
            }    </style>
    </head>
    <!--   banner start  -->
    <div class="banner">
        <div class="container"><h2 style="color: white"
                                   class="wow slideInDown"><?php echo get_field('banner_heading'); ?></h2>
            <h1 style="color: white" class="wow slideInRight"
                data-wow-delay="0.5s"><?php echo get_field('banner_sub_heading'); ?></h1>
            <p class="wow slideInUp" data-wow-delay="0.7s"><?php echo get_field('banner_sub_heading_2'); ?></p>
        </div>
    </div>
    <div class="sticky-container">
        <style>        .sticky-number {
                color: black;
                text-decoration: underline;
            }    </style>
    </div>                   <!--   banner end  -->
    <!--   about us start  -->
    <div id="get-quote" class="aboutus wow fadeInUp" data-wow-delay="0.3s" id="about">
        <div class="container"><h1 class="wow slideInDown"
                                   data-wow-delay="1s"><?php echo get_field('about_us_heading'); ?></h1>
            <p class="subheading"><?php echo get_field('about_us_sub_heading'); ?></p>
            <p><?php echo get_field('about_us_paragraph'); ?></p></div>
    </div>                                <!--   about us end  -->
    <!--   module start  -->
    <div id="modules" class="module">
        <div class="container">
            <div class="heading"><h1><?php echo get_field('core_modules'); ?></h1>
                <p><?php echo get_field('core_modules_paragraph'); ?></p></div>
            <div class="row"><a href="sales-management-solutions.php">
                    <div class="col-xs-12 col-sm-6 col-md-3 wow shake">
                        <div class="moduledata">
                            <div class="imgholder1"></div>
                            <div class="moduleinfo"><h4>Sales</h4>
                                <p>Track and manage all sales activities <span style="visibility: hidden">active</span>
                                </p>
                            </div>
                            <i class="fa fa-long-arrow-right"></i>
                </a></div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3 wow shake" data-wow-delay="3.2s"><a
                    href="procurement-management-solutions.php">
                <div class="moduledata">
                    <div class="imgholder2"></div>
                    <div class="moduleinfo"><h4>Procurement</h4>
                        <p>Manage and record all procurement tasks </p></div>
                    <i class="fa fa-long-arrow-right"></i>
            </a></div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-3 wow shake" data-wow-delay="1s"><a href="inventory-management-solutions.php">
            <div class="moduledata">
                <div class="imgholder3"></div>
                <div class="moduleinfo"><h4>Inventory</h4>
                    <p>Log stock inventory and goods issues/received</p></div>
                <i class="fa fa-long-arrow-right"></i>
        </a></div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-3"><a href="accounts-finance-management-software-solutions.php">
            <div class="moduledata">
                <div class="imgholder4"></div>
                <div class="moduleinfo"><h4>Accounts</h4>
                    <p>Automate accounting and financial tasks</p></div>
                <i class="fa fa-long-arrow-right"></i>
        </a></div>
    </div>                                    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-3"><a href="production-management-software-solutions.php">
                <div class="moduledata">
                    <div class="imgholder5"></div>
                    <div class="moduleinfo"><h4>Production</h4>
                        <p>Track and process the production of finished goods</p></div>
                    <i class="fa fa-long-arrow-right"></i>
            </a></div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-3 wow headShake" data-wow-delay="1.7s"><a href="../hrm">
            <div class="moduledata">
                <div class="imgholder6"></div>
                <div class="moduleinfo"><h4>HRM</h4>
                    <p>Organize and consolidate your HR processes <br> <span style="visibility: hidden"> </span></p>
                </div>
                <i class="fa fa-long-arrow-right"></i>
        </a></div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-3"><a href="../crm">
            <div class="moduledata">
                <div class="imgholder7"></div>
                <div class="moduleinfo"><h4>CRM</h4>
                    <p>Record, manage, and analyze interactions with customers <span
                                style="visibility: hidden"> <br></span>
                    </p></div>
                <i class="fa fa-long-arrow-right"></i>
        </a></div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-3 wow shake" data-wow-delay="2.1s"><a
                href="supplychain-management-software-solutions.php">
            <div class="moduledata">
                <div class="imgholder8"></div>
                <div class="moduleinfo"><h4>Supply Chain</h4>
                    <p>Manage the flow of goods and services</p></div>
                <i class="fa fa-long-arrow-right"></i>
        </a></div>
    </div>
    </div>
    </div>
    </div>
    <!--   modules end  -->                                      <!--   clients start  -->
    <div id="clinet" class="clients">
        <div class="container">
            <div class="clientheading"><h1 class="wow fadeInDown"><?php echo get_field('happy_clients_heading'); ?></h1>
                <p class="wow fadeInUp" data-wow-delay="0.6s"><?php echo get_field('happy_clients_content'); ?></p>
            </div>
            <div class="partners">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 wow fadeInRight" data-wow-delay="1.3s">
                        <div class="owl-carousel owl-theme" style="z-index: -1">
                            <div class="item">
                                <div class="slider">
                                    <div class="clientimg"><img
                                                src="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/img/client.png"
                                                alt="#"></div>
                                    <div class="client_info"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                                class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                                class="fa fa-star"></i>                   <h5>TUV Austria</h5></div>
                                    <div class="clearfix"></div>
                                    <div class="clientdetails"><p>We acquired a HR system from NetRoots, and have found
                                            it
                                            to exceed expectations. NetRoots was able to customize our software down to
                                            a T,
                                            creating for us a solution that addressed every one of our HR needs for an
                                            affordable price. Perhaps the best thing about the company was that their
                                            team
                                            worked closely with us to figure out what we need and then made us exactly
                                            that.</p></div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider">
                                    <div class="clientimg"><img
                                                src="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/img/client1.png"
                                                alt="#"></div>
                                    <div class="client_info"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                                class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                                class="fa fa-star"></i>                   <h5>Office Automation
                                            Group</h5>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="clientdetails"><p>NetRoots Technologies created an ERP system including
                                            HRMS
                                            and CRM for us. They automated all of our main business processes, and
                                            really
                                            simplified our work. Throughout the process, they remained in contact with
                                            us
                                            and took our suggestions and requirements into consideration. What we love
                                            about
                                            this ERP is that anyone can use it – young or old, tech-savvy or not! We are
                                            happy that we can focus on growing our business now that we don’t have to
                                            worry
                                            about doing everything manually.</p></div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider">
                                    <div class="clientimg"><img
                                                src="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/img/client2.png"
                                                alt="#"></div>
                                    <div class="client_info"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                                class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                                class="fa fa-star"></i>                   <h5>Sufi Flour Mills</h5>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="clientdetails"><p>Flour mills in Pakistan mostly operate in the
                                            traditional
                                            way, using manual records. We are glad that we made the switch. It was
                                            becoming
                                            difficult to keep record of and manually manage production and tracking and
                                            our
                                            ERP solved those problems. Our ERP also includes HR functions such as
                                            payroll
                                            and employee asset management. This makes keeping account of our finances
                                            much
                                            easier!</p></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 wow fadeInLeft" data-wow-delay="1.3s">
                        <div class="clientholder"><img
                                    src="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/img/logo-client.jpg"
                                    alt="#"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>                                      <!--   clients end  -->

    </body>
    </html>


<?php
get_footer();
