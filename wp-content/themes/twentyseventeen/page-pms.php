<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_template_part( 'template-parts/header/header', 'pms' ); ?>



<!--********************************** body -->

<body id="page" cz-shortcut-listen="true">


<div class="sticky-container">        <style>

    .okewa-style_3.okewa-text_3 #okewa-floating_cta{

        bottom: 55px;
        right: 13px;
    }
    .okewa-pulse_3{
        bottom: 55px !important;
    }
</style>







<ul class="sticky">

        <li>

            <img src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/img/ireland.png" width="32" height="32">

            <p><a href="tel:+353578601255" target="_blank">+353 (57) 8601255</a></p>

</li>

<li>

    <img src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets//img/canada.png" width="32" height="32">

    <p><a href="tel:+16474906750" target="_blank">+1 (647) 490 6750</a></p>

</li>

<li>

    <img src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/img/usa.png" width="32" height="32">

    <p><a href="tel:+13023001742" target="_blank">+1 (302) 300 1742</a></p>

</li> <li>
    <img src="<?php echo get_template_directory_uri(); ?>/assets/pms-assets/img/pakistan.png" width="32" height="32">
    <p><a href="tel:+924236287770" target="_blank">+92 42 36287770</a></p>
</li>






</ul>


    </div>

<header>
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class=""></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2" class=""></li>
            </ol>

            <div class="carousel-inner" role="listbox">
                <!-- Slide One - Set the background image for this slide in the line below -->
			<?php
			$count = 0;
			// the query
			$wpb_all_query = new WP_Query(array('post_type'=>'post', 'cat'=> 15 , 'post_status'=>'publish', 'posts_per_page'=>5)); ?>
			<?php if ( $wpb_all_query->have_posts() ) : ?>
			    <!-- the loop -->
			    <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?> 
                    	<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), '' ); ?>


                <div class="carousel-item <?php if($count == 1){echo "active";} ?>" style="background-image: url('<?php echo $url?>');">
                    <div class="carousel-caption d-none d-md-block">

							

                        <div class="first">
                        	<h1 class="display-4" style="color: black;font-size: 60px;font-weight: bold"> <?php the_title(); ?></h1>
                            <h3 style="color: black"> <?php echo the_content(); ?></h3>
                        </div>
                    </div>
                </div>

    <?php $count++; endwhile; ?>
    <?php wp_reset_postdata(); ?>
<?php else : ?>
    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>
            </div>

            <span id="Modules"> </span> <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span>
            </a> <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span>
            </a>
        </div>
    </header>





    <section id="module" style="    padding-top: 20px;padding-bottom: 20px">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading text-uppercase">NETROOTS PMS -
                        <small>We rekindle your love for property management</small>
                    </h2>
                    <h3 class="section-subheading text-muted" style="font-style: normal">NetRoots PMS simplifies
                        residential and commercial property management so that you can stop worrying about the tedious
                        tasks and find enjoyment in your work. With information about property, property owners,
                        tenants, and support staff as well as contracts and complaints all digitized, you can stop
                        worrying about files and folders and instead focus on making deals.</h3></div>
            </div>
            <div class="row text-center">

                <?php
                  // the query
                $wpb_all_query = new WP_Query(array('post_type'=>'post', 'cat'=> 20 , 'post_status'=>'publish', 'posts_per_page'=>20)); ?>

                <?php if ( $wpb_all_query->have_posts() ) : ?>
                    <!-- the loop -->
                    <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>


                <div class="col-md-3 hvr-grow-shadow pt-3 pb-3 animated fadeInUp visible" data-animation="fadeInUp">
                    <span class="fa-stack fa-4x image-holder">
                                
                     <i style="font-size:50px;" class="<?php the_field('icon-module'); ?> fa-stack-1x fa-inverse"></i>
                 </span>
                    <h4 class="service-heading"><?php the_title(); ?></h4>
                    <p class="text-muted" style="font-size: 18px;line-height: 1.75;"><?php echo the_content(); ?></p>
                    <!-- <button type="button" class="btnss btn btn-dark"><a href="<?php //the_permalink(); ?>">Read more</a></button> -->
                </div>
                
                 <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>


            </div>
        </div>
    </section>




<!-- ********************************************************WHY CHOOSE US********** -->




    <section class="page-section" id="Features">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8 text-center"><h1 class="mt-0">WHY CHOOSE US</h1>
                    <hr class="divider my-4">
                    <!--                <p class="text-muted mb-5">Ready to start your next project with us? Give us a call or send us an email and we will get back to you as soon as possible!</p>-->
                </div>
            </div>
            <div class="row">
                <?php
                $args=array(
                'post_type' => 'post',
                'post_status' => 'publish',
                'posts_per_page' => 20,
                'cat'=> 18
                );
                $my_query = null;
                $my_query = new WP_Query($args);
            if( $my_query->have_posts() ) {
                
            $i = 1;

                   ?> 
                    <?php
            while ($my_query->have_posts()) : $my_query->the_post();
                if($i % 4 == 0) {  
                print_r($my_query->the_post());?>
                <?php
                }
                ?>
                

                    <div class="f col-lg-3 ml-auto text-center pt-5 pb-5 hvr-glow ">
                        <div class="hvr-icon-pulse-grow"><i class="iconss fas <?php the_field('pms'); ?>  fa-2x mb-3 text-muted hvr-icon "></i>
                        <h5><?php the_title(); ?></h5>
                        <div class="grow-pulse-text"><p><?php echo the_content(); ?></p></div>
                    </div>
                    
                        </div>
               
                <?php    
                if($i % 4 == 0) { ?> 
                <?php

                $i++;
                }
            endwhile;
               ?>
               <?php
            }
            wp_reset_query();
            ?>
            </div>
        </div>
    </section>



<!-- ****************************************************************** -->


    <section class="page-section testimonails" style="background-color: #f2f2f2;">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <div class="section-title text-left" data-animation="fadeInLeft">
                        <!-- Heading --> <h2 class="title">Latest Posts</h2></div>
                    <ul class="latest-posts">

            <?php
            // the query
            $wpb_all_query = new WP_Query(array('post_type'=>'post', 'cat'=> 16 , 'post_status'=>'publish', 'posts_per_page'=>2)); ?>
            <?php if ( $wpb_all_query->have_posts() ) : ?>
                <!-- the loop -->
                <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>

                        <li data-animation="fadeInLeft">
                            <div class="post-thumb">
                             
                             <?php the_post_thumbnail( array(85,85) );?>

                            </div>

                            <style>
                                .description a {text-decoration: underline; }
                                .description a:hover {text-decoration: underline;color: #00094d;}
                            </style>

                            <div class="post-details">
                                <div class="description"><a href="<?php echo get_page_link(285); ?>">
                                <?php the_title();?>

                                </a>
                                    </div>

                                <div class="meta">
                                <span class="time">
                                    <i class="fa fa-calendar"></i>
                                    <?php the_date( 'd-m-Y'); ?>
                                </span></div>
                            </div>
                        </li>


            <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>

                    </ul>
                </div>

                <!-- testemonials start from here -->

                <div class="col-sm-12 col-md-6 testimonails">
                    <div class="section-title text-left" data-animation="fadeInRight">
                        <!-- Heading --> <h2 class="title">Testimonials</h2>
                    </div>
                    <div class="owl-carousel pagination-1 dark-switch" data-effect="backSlide" data-pagination="true"
                         data-autoplay="true" data-navigation="false" data-singleitem="true"
                         data-animation="fadeInRight">

            <?php
            // the query
            $wpb_all_query = new WP_Query(array('post_type'=>'post', 'cat'=> 7 , 'post_status'=>'publish','order'=>'asc', 'posts_per_page'=>3)); ?>
            <?php if ( $wpb_all_query->have_posts() ) : ?>
                <!-- the loop -->
                <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>

                        <div class="item">
                            <div class="desc-border bottom-arrow quote">
                                <blockquote class="small-text">
                                  <?php echo the_content(); ?>
                                </blockquote>
                                <div class="star-rating text-right"><i class="fa fa-star text-color"></i> <i
                                        class="fa fa-star text-color"></i> <i class="fa fa-star text-color"></i> <i
                                        class="fa fa-star text-color"></i> <i
                                        class="fa fa-star-half-o text-color"></i></div>
                            </div>
                            <div class="client-details text-center">
                                <div class="client-image">      
                             <div>
								 <?php the_post_thumbnail( array(80, 80), array('class' => 'img-circle'));?>
									</div>
                                    </div>

                                <div class="client-details">
                                    <strong class="text-color">
                                        <?php the_title();?>
                                        </strong>
                                </div>
                            </div>
                        </div>

            <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>

                        
                    </div>
                </div>
            </div>
        </div>
    </section>

    
       <section id="clients" class="page-section tb-pad-30">
        <div class="container">
            <div class="section-title text-center" data-animation="fadeInUp">
                <h1 class="title">Our Best Clients</h1></div>
            <div class="row">
                <div class="col-md-12 text-center" data-animation="fadeInDown">
                    <div class="owl-carousel navigation-1" data-pagination="false" data-items="6" data-autoplay="true"
                         data-navigation="true">


                         <?php
                // the query
                $wpb_all_query = new WP_Query(array('post_type'=>'post', 'cat'=> 8 , 'post_status'=>'publish', 'posts_per_page'=>10)); ?>
                <?php if ( $wpb_all_query->have_posts() ) : ?>
                    <!-- the loop -->
                    <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
             <a style="margin:10px;">
             <?php the_post_thumbnail( array(150, 150));?>
                           
                 </a> 

          <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?> 


                 </div>
                </div>
            </div>
        </div>
    </section>    <!--testimonials-->


    <style>

    .okewa-style_3.okewa-text_3 #okewa-floating_cta{

        bottom: 55px;
        right: 13px;
    }
    .okewa-pulse_3{
        bottom: 55px !important;
    }
</style>






<div id="get-quote" class="bg-color get-a-quote black text-center">
    <div class="container animated pulse visible" data-animation="pulse">
        <div class="row">
            <div class="col-md-12"><p>Get A Free Quote / Need a Help ? <a class="black" href="<?php echo get_page_link(47); ?>">Contact
                        Us</a></p></div>
        </div>
    </div>
</div>

</body>

<!--**********************************end body  -->

<?php 
get_footer();
