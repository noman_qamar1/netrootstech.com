<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="sticky-container" >

    <?php
        require 'inc/config.php';
        require 'inc/sticky.php';
 ?>

</div>
    <div class="blog-box">

        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/blog-banner.jpg" alt="" width="100%" class="services-img">

        <div class="container">
            <div class="cd-articles">

<?php
// the query
$wpb_all_query = new WP_Query(array('post_type'=>'post', 'cat'=> 4 , 'post_status'=>'publish', 'posts_per_page'=>4)); ?>
<?php if ( $wpb_all_query->have_posts() ) : ?>
    <!-- the loop -->
    <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
                <article>
                    <header>

                        <h1><?php the_title();?></h1>

                     <?php the_post_thumbnail('full'); ?>



                    </header>
                    <?php the_content() ?>
                </article>


    <?php endwhile; ?>
    <?php wp_reset_postdata(); ?>
<?php else : ?>
    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>
                <aside class="cd-read-more" style="">
                    <ul>

                        <li><a href="HRMS" class=""> <em>5 Ways HRMS Can Increase
                                    Employee Retention</em> <b>by Netroots</b>
                                <svg x="0px" y="0px" width="36px" height="36px" viewBox="0 0 36 36">
                                    <circle fill="none" stroke-width="2" cx="18" cy="18" r="16" stroke-dasharray="100 100"
                                            stroke-dashoffset="87.25356196156395" transform="rotate(-90 18 18)"></circle>
                                </svg>
                            </a></li>
                        <li><a href="Content-Marketing" class=""> <em>Content Marketing: Online
                                    Marketing that Doesn’t Try to Sell</em> <b>by Netroots</b>
                                <svg x="0px" y="0px" width="36px" height="36px" viewBox="0 0 36 36">
                                    <circle fill="none" stroke-width="2" cx="18" cy="18" r="16" stroke-dasharray="100 100"
                                            stroke-dashoffset="99.70473884976526" transform="rotate(-90 18 18)"></circle>
                                </svg>
                            </a></li>



                        <li><a href="CRM" class=""> <em>How the CRM customer complaint
                                    module can save your business</em> <b>by Netroots</b>
                                <svg x="0px" y="0px" width="36px" height="36px" viewBox="0 0 36 36">
                                    <circle fill="none" stroke-width="2" cx="18" cy="18" r="16" stroke-dasharray="100 100"
                                            stroke-dashoffset="13.829677152317876" transform="rotate(-90 18 18)"></circle>
                                </svg>
                            </a></li>


                        <li><a href="ERP" class=""> <em>Factors to Consider When Choosing ERP Software</em> <b>by
                                    Netroots</b>
                                <svg x="0px" y="0px" width="36px" height="36px" viewBox="0 0 36 36">
                                    <circle fill="none" stroke-width="2" cx="18" cy="18" r="16" stroke-dasharray="100 100"
                                            stroke-dashoffset="99.48486799742435" transform="rotate(-90 18 18)"></circle>
                                </svg>
                            </a></li>


                    </ul>
                </aside>
            </div>
        </div>
    </div>

<main class="cd-main-content">
<?php
get_footer();
	?>
</main>
