<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_template_part( 'template-parts/header/header', 'hrm' ); ?>
<!-- ------------------------------------------------template body start from here -->


<div class="top-banner">
    <div class="container">
        <div class="banner-content">
            <h1>
                Attendance and Leave Management
            </h1>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
            </p>
            <p>
                do eiusmod tempor incididunt labore
            </p>


        </div>




    </div>



</div>

<!--banner ends-->


<!--module-area start-->

<div class="module-area">

    <div class="container">
        <h2>
            Photoshop Mockups Included Free!
        </h2>
        <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt labore dolore magna aliqua.
            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi aliquip ex ea commodo consequat.
        </p>
        <div class="module-chart">
            <div class="row">
                <div class="col-md-4">

                    <div class="branches" data-aos="flip-up">

                        <a href="#">
                            <h4>
                                module
                            </h4>
                            <p>
                                Lorem ipsum dolor sit amet
                            </p>
                        </a>

                    </div>

                    <div class="branches" data-aos="flip-up">
                        <a href="#">

                            <h4>
                                module
                            </h4>
                            <p>
                                Lorem ipsum dolor sit amet
                            </p>
                        </a>

                    </div>


                    <div class="branches" data-aos="flip-up">

                        <a href="#">

                            <h4>
                                module
                            </h4>
                            <p>
                                Lorem ipsum dolor sit amet
                            </p>
                        </a>

                    </div>

                    <div class="branches" data-aos="flip-up">

                        <a href="#">

                            <h4>
                                module
                            </h4>
                            <p>
                                Lorem ipsum dolor sit amet
                            </p>
                        </a>

                    </div>


                    <div class="branches" data-aos="flip-up">

                        <a href="#">

                            <h4>
                                module
                            </h4>
                            <p>
                                Lorem ipsum dolor sit amet
                            </p>
                        </a>

                    </div>

                    <div class="branches" data-aos="flip-up">

                        <a href="#">

                            <h4>
                                module
                            </h4>
                            <p>
                                Lorem ipsum dolor sit amet
                            </p>
                        </a>

                    </div>


                </div>
                <div class="col-md-4">

                    <figure class="module-tablet"  data-aos="zoom-in">

                        <a href="#">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/images/module-tab.png" alt="dashboard" width="100%">
                        </a>

                    </figure>

                </div>
                <div class="col-md-4">

                    <div class="branches-right" data-aos="flip-up">
                        <a href="#">

                            <h4>
                                module
                            </h4>
                            <p>
                                Lorem ipsum dolor sit amet
                            </p>
                        </a>

                    </div>

                    <div class="branches-right" data-aos="flip-up">
                        <a href="#">

                            <h4>
                                module
                            </h4>
                            <p>
                                Lorem ipsum dolor sit amet
                            </p>
                        </a>

                    </div>

                    <div class="branches-right" data-aos="flip-up">
                        <a href="#">

                            <h4>
                                module
                            </h4>
                            <p>
                                Lorem ipsum dolor sit amet
                            </p>
                        </a>

                    </div>

                    <div class="branches-right" data-aos="flip-up">
                        <a href="#">

                            <h4>
                                module
                            </h4>
                            <p>
                                Lorem ipsum dolor sit amet
                            </p>
                        </a>

                    </div>

                    <div class="branches-right" data-aos="flip-up">
                        <a href="#">

                            <h4>
                                module
                            </h4>
                            <p>
                                Lorem ipsum dolor sit amet
                            </p>
                        </a>

                    </div>

                    <div class="branches-right" data-aos="flip-up">
                        <a href="#">

                            <h4>
                                module
                            </h4>
                            <p>
                                Lorem ipsum dolor sit amet
                            </p>
                        </a>

                    </div>



                </div>


            </div>



        </div>



    </div>

</div>

<!--module-area ends-->


<!--content-wraper start-->

<div class="content-wraper">

    <div class="container">

        <div class="row">



            <div class="col-md-6">

                <div class="wraper-text"data-aos="fade-right"
                     data-aos-offset="300"
                     data-aos-easing="ease-in-sine">

                    <h2>
                        Lorem Ipsum is dolor
                    </h2>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                        labore dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                        laboris nisi aliquip ex ea commodo consequat.
                    </p>
                    <p>
                        <i class="far fa-check-circle"></i> &nbsp;Accept all credit cards and free bank transfers, right in the invoice

                    </p>

                    <p>
                        <i class="far fa-check-circle"></i>  &nbsp;Track invoice status, send payment reminders, and match payments to invoices,
                        automatically

                    </p>

                    <p>
                        <i class="far fa-check-circle"></i>  &nbsp;Create professional custom invoices with your logo that you can send from any device

                    </p>



                </div>



            </div>
            <div class="col-md-6">
                <figure class="wraper-image" data-aos="zoom-in-up">

                    <a href="#">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/images/module-laptop.png" alt="dashboard" width="100%">
                    </a>

                </figure>

            </div>

        </div>



    </div>

</div>

<!--content-wraper end-->

<!--text area-->

<div class="text-area" data-aos="zoom-in-down">

    <div class="container">

        <h2>
            Text area
        </h2>

        <p>

            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt labore dolore magna aliqua.
            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi aliquip ex ea commodo consequat.
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt labore dolore magna aliqua.
            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi aliquip ex ea commodo consequat.
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt labore dolore magna aliqua.
            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi aliquip ex ea commodo consequat.

        </p>


    </div>



</div>

<script>
	AOS.init();
	</script>
<!-- ----------------------------------------------template end here -->
<?php
get_footer();