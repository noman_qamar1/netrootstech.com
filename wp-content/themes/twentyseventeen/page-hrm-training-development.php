<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_template_part( 'template-parts/header/header', 'hrm' ); ?>

<!-- ------------------------------------------------template body start from here -->

<div class="top-banner">
        <div class="container">
            <div class="banner-content">
                <h1>
                    Training & Development
                </h1>
                <p>
                    To get the most out of a training program, a number 
                </p>
                <p>
                   of factors must be considered.
                </p>


            </div>




        </div>



    </div>

    <!--banner ends-->


    <!--module-area start-->

    <div class="module-area">

        <div class="container">
            <h2>
                To get the most out of a training program, a number of factors must be considered. For example, what is the ideal time for training? Is training even needed? How can it be made more useful and efficient in the future?
            </h2>
            <p>
               Our powerful human resource management system helps you answer all of these questions through its Training & Development module. The training management solutions provided by Fórsa HR software enable you to carry out training need analysis, schedule and create a training calendar, and receive feedback from employees. 
            </p>
            <div class="module-chart">
                <div class="row">
                    <div class="col-md-4">

                        <div class="branches-training" data-aos="flip-up">

                            <a href="#">
                                <h4>
                                    Training Evaluation
                                </h4>
                                <p>
                                   The second sub-module of our training management software, Training Evaluation, simplifies and aids in the process of selecting employees for training and evaluating their performance post-training. 
                                </p>
                            </a>

                        </div>

                        <div class="branches-training" data-aos="flip-up">
                            <a href="#">

                                <h4>
                                   Training institutes Management
                                </h4>
                                <p>
                                    Lastly, the Training Institutes Management sub-module of our HR software lists details of the institutes that are facilitating or conducting training.
                                </p>
                            </a>

                        </div>



                    </div>
                    <div class="col-md-4">

                        <figure class="module-tablet"  data-aos="zoom-in">

                            <a href="#">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/images/training-tab.png" alt="dashboard" width="100%">
                            </a>

                        </figure>

                    </div>
                    <div class="col-md-4">

                        <div class="branches-right-training" data-aos="flip-up">
                            <a href="#">

                                <h4>
                                  Training Lists
                                </h4>
                                <p>
                                    The Training Lists sub-module allows you to add a new training program, with information such as its title, training type, cost, start and end dates, duration, trainer profile, description of program, objectives, and key benefits. This sub-module lists all active and inactive training programs.
                                </p>
                            </a>

                        </div>

                      


                    </div>


                </div>



            </div>



        </div>

    </div>

    <!--module-area ends-->


    <!--content-wraper start-->

    <div class="content-wraper">

        <div class="container">

            <div class="row">



                <div class="col-md-6">

                    <div class="wraper-text"data-aos="fade-right"
                         data-aos-offset="300"
                         data-aos-easing="ease-in-sine">

                        <h2>
                            Organized, Integrated, Efficient
                        </h2>
                        <p>
                        
                        Fórsa’s training management software tracks every element of a training program and records it in one convenient system, facilitating you to not only prepare effective training programs, but also to view their results on the same platform. Our training management software is also strongly integrated with other modules of our HR software, such as our payroll software and performance management software. Due to this, relevant data can be shared quickly and efficiently between the modules. 

                        </p>



                    </div>



                </div>
                <div class="col-md-6">
                    <figure class="wraper-image" data-aos="zoom-in-up">

                        <a href="#">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/images/training-laptop.png" alt="dashboard" width="100%">
                        </a>

                    </figure>

                </div>

            </div>



        </div>

    </div>

    <!--content-wraper end-->

    <!--text area-->

    <div class="text-area" data-aos="zoom-in-down">

        <div class="container">

            <h2>
                Why use Fórsa’s Training and Development Software? 
            </h2>

            <p>

             Equipped with Fórsa’s training management solutions, you can maximize the utility of training and development programs. Our software also lets you analyze the performance of your employees, determining which employees would benefit the most from a training program, as well as which training program may be most effective. Paired with the other modules of Fórsa HR software, such as the payroll software and employee database, the Training and Development module has the capacity to revolutionize employee management.

            </p>


        </div>



    </div>



<!-- ----------------------------------------------template end here -->

<script>
	AOS.init();
	</script>
<?php
get_footer();