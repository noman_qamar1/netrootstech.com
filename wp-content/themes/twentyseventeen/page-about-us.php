<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<main class="cd-main-content">
    <div class="image-aboutus-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12"  style="margin-top: 90px;">
                    <div class="about_banner_content"><h1>About us</h1>
                        <p>NetRoots Technologies provides IT services and consultancy to help <br>businesses
                            succeed in the increasingly digital times. At NetRoots,<br> we ensure that your
                            business finds exactly what it needs to grow.</p></div>
                    <!--                <h1 class="lg-text">About Us</h1>-->
                    <!--                <p class="image-aboutus-para">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>-->
                </div>
            </div>
        </div>
    </div><!-- Projects Section -->
    <section id="projects" class="projects-section ">
        <div class="container">        <!-- Featured Project Row -->
            <section class="about-section">
                <div class="container">
                    <div class="row">

                        <!-- Image Column -->
                        <div class="image-column col-lg-6 col-md-12 col-sm-12">
                            <div class="inner-column wow fadeInLeft">
                                <figure class="image-1"><a href="#" class="lightbox-image"
                                                           data-fancybox="images"><img
                                                src="<?php echo get_template_directory_uri(); ?>/assets/img/about-us-new (3)-new.jpg" width="100%" alt=""></a></figure>
                                <figure class="image-2"><a href="#" class="lightbox-image"
                                                           data-fancybox="images"><img
                                                src="<?php echo get_template_directory_uri(); ?>/assets/img/image-2-about.jpg" width="100%" alt=""></a>
                                </figure>
                            </div>
                        </div>



                        <div class="content-column col-lg-6 col-md-12 col-sm-12 order-2">
                            <div class="inner-column">
                                <div class="sec-title"><span class="title">About Company</span>
                                    <h2>We are offering our services since 2014</h2></div>
                                <div class="text">NetRoots Technologies was visualized by our founders over a
                                    decade ago in a town in Ireland. The aim was to find simple, customized
                                    technological solutions for complex business problems. Over a short span of
                                    time, our company grew from one office in Ireland to offices all around the
                                    world, including in
                                </div>
                                <ul class="list-style-one">
                                    <li>Delaware, USA, Toronto,</li>
                                    <li>Canada, Pakistan,</li>
                                    <li>Dubai, UAE.</li>
                                </ul>
                                <div class="btn-box"><a href="<?php echo get_page_link(47); ?>" class="theme-btn btn-style-one">Contact
                                        Us</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
</main>
<?php
get_footer();
