<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>


<?php the_post_thumbnail('full', ['class' => 'img-responsive responsive--full', 'title' => 'Feature image']);?>

    <div class="page_content">

        <div class="container">



            <p>
                If you want to be in the limelight then improve your digital visibility with the right, genuine and planned SEO services with digital marketing to make the impact long term. 

Digital marketing and SEO services provide you easy access to your customers. Our team of experienced professionals makes it easy for you by studying the market trends and then designs a complete SEO plan for you to increase the genuine traffic to your website. Then the implementation phase starts down where different techniques are used to enhance the visibility to target the right audience with right and profound data.

We provide the following services for your business to enhance it and to spread it to the right end-users.</p>
<ol style="margin-left:40px;">
		<li>SEO</li>	
	<li>ASO</li>
	<li>SEM</li>
	<li>DMM</li>
	<li>SMM</li>
	<li>Virtual walkthroughs</li>
			
			</ol>	

				
<p>Choose wisely and enjoy the perks of the right SEO strategy! Pick the package you like and suitable for you and witness the brightening future. 

            </p>


        </div>



    </div>
    <section class="tab_wraper">
        <div class="container">
            <div class="row">
                <div class="board">
                    <!-- <h2>Welcome to IGHALO!<sup>™</sup></h2>-->
                    <div class="board-inner">
                        <ul class="nav nav-tabs">

                            <li class="active">

                                <a href="#Basic" class="Basic" data-toggle="tab" title="price">
                                    <div class="tabinner_con">
                                        <div class="tabinner">

                                            <h3>Basic</h3>

                                            <div class="price_con">
                                                <div class="price_inner">
                                                    <span>$299</span><br>
                                                    per month

                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </a>





                            </li>

                            <li>

                                <div class="standard">


                                    <a href="#standard" data-toggle="tab" title="price">


                                        <div class="tabinner_con2">
                                            <div class="tabinner">

                                                <h3>Standard</h3>

                                                <div class="price_con">
                                                    <div class="price_inner">
                                                        <span>$499</span><br>
                                                        per month

                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </a>

                                </div>

                            </li>

                            <li>

                                <div class="Premium">


                                    <a href="#Premium" data-toggle="tab" title="price">


                                        <div class="tabinner_con3">
                                            <div class="tabinner">

                                                <h3>Premium</h3>

                                                <div class="price_con">
                                                    <div class="price_inner">
                                                        <span>$799</span><br>
                                                        per month

                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </a>

                                </div>

                            </li>

                            <li>

                                <div class="Executive">


                                    <a href="#Executive" data-toggle="tab" title="price">


                                        <div class="tabinner_con4">
                                            <div class="tabinner">

                                                <h3>Executive</h3>

                                                <div class="price_con">
                                                    <div class="price_inner">
                                                        <span>$999</span><br>
                                                        per month

                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </a>

                                </div>



                            </li>



                        </ul>
                    </div>

                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="Basic">

                            <table class="table table-striped table-bordered dt-responsive nowrap basic_tab" cellspacing="0" width="100%">
                                <thead>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Targeted Number of Key Phrases Website</td>
                                    <td>5</td>


                                    <td>Article Submission Directories</td>
                                    <td>15</td>
                                </tr>
                                <tr>
                                    <td>Keyword+Competitors Analysis</td>

                                    <td><i class="fas fa-check"></i></td>
                                    <td>Backlinks from Forums</td>
                                    <td>20</td>
                                </tr>
                                <tr>
                                    <td>SEO Friendly URL Structures</td>
                                    <td><i class="fas fa-times"></i></td>

                                    <td>Backlinks from question and answers sites</td>
                                    <td>No</td>
                                </tr>
                                <tr>
                                    <td>Unique Meta Information</td>

                                    <td>5</td>
                                    <td>Search Engine Submissions</td>
                                    <td>Yes</td>
                                </tr>
                                <tr>
                                    <td>Images and Alt tag</td>
                                    <td>All</td>

                                    <td>Community Creation in Social Networking Sites</td>
                                    <td>Yes</td>
                                </tr>
                                <tr>
                                    <td>Xml Sitemaps Creation</td>

                                    <td><i class="fas fa-check"></i></td>
                                    <td>Blogging</td>
                                    <td>5 Mini blogs</td>
                                </tr>
                                <tr>
                                    <td>Robots.txt</td>

                                    <td><i class="fas fa-check"></i></td>
                                    <td>Classifieds Submission</td>
                                    <td>No</td>
                                </tr>
                                <tr>
                                    <td>W3C Verified</td>

                                    <td><i class="fas fa-check"></i></td>
                                    <td>Price Press Release submission</td>
                                    <td>25</td>
                                </tr>
                                <tr>
                                    <td>Google Analytics Installation</td>

                                    <td>Home page</td>
                                    <td>RSS feed</td>
                                    <td>25</td>
                                </tr>
                                <tr>
                                    <td>Directory Submissions</td>

                                    <td>1000</td>
                                    <td>Video sharing, Photo sharing</td>
                                    <td>No</td>
                                </tr>
                                <tr>
                                    <td>Social Bookmarking</td>

                                    <td>200 Home page</td>
                                    <td>SEM</td>
                                    <td>Yes</td>
                                </tr>
                                <tr>
                                    <td>Blog Commenting</td>

                                    <td>200 Home page</td>
                                    <td>SMO</td>
                                    <td>Yes</td>
                                </tr>
                                <tr>
                                    <td>.org and EDU Backlinks</td>

                                    <td><i class="fas fa-times"></i></td>
                                    <td>PPC</td>
                                    <td>On Demand</td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade " id="standard">

                            <table class="table table-striped table-bordered dt-responsive nowrap standard_tab" cellspacing="0" width="100%">
                                <thead>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Targeted Number of Key Phrases Website</td>
                                    <td>10</td>


                                    <td>Article Submission Directories</td>
                                    <td>30</td>
                                </tr>
                                <tr>
                                    <td>Keyword+Competitors Analysis</td>

                                    <td><i class="fas fa-check"></i></td>
                                    <td>Backlinks from Forums</td>
                                    <td>30</td>
                                </tr>
                                <tr>
                                    <td>SEO Friendly URL Structures</td>
                                    <td><i class="fas fa-check"></i></td>

                                    <td>Backlinks from question and answers sites</td>
                                    <td><i class="fas fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td>Unique Meta Information</td>

                                    <td><i class="fas fa-check"></i></td>
                                    <td>Search Engine Submissions</td>
                                    <td><i class="fas fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td>Images and Alt tag</td>
                                    <td>All</td>

                                    <td>Community Creation in Social Networking Sites</td>
                                    <td><i class="fas fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td>Xml Sitemaps Creation</td>

                                    <td><i class="fas fa-check"></i></td>
                                    <td>Blogging</td>
                                    <td>7 Mini blogs</td>
                                </tr>
                                <tr>
                                    <td>Robots.txt</td>

                                    <td><i class="fas fa-check"></i></td>
                                    <td>Classifieds Submission</td>
                                    <td>No</td>
                                </tr>
                                <tr>
                                    <td>W3C Verified</td>

                                    <td><i class="fas fa-check"></i></td>
                                    <td>Price Press Release submission</td>
                                    <td>50</td>
                                </tr>
                                <tr>
                                    <td>Google Analytics Installation</td>

                                    <td><i class="fas fa-check"></i></td>
                                    <td>RSS feed</td>
                                    <td>50</td>
                                </tr>
                                <tr>
                                    <td>Directory Submissions</td>

                                    <td>2000</td>
                                    <td>Video sharing, Photo sharing</td>
                                    <td><i class="fas fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td>Social Bookmarking</td>

                                    <td>250 Home page</td>
                                    <td>SEM</td>
                                    <td><i class="fas fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td>Blog Commenting</td>

                                    <td>300 Home page</td>
                                    <td>SMO</td>
                                    <td><i class="fas fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td>.org and EDU Backlinks</td>

                                    <td>10</td>
                                    <td>PPC</td>
                                    <td>On Demand</td>
                                </tr>

                                </tbody>
                            </table>

                        </div>
                        <div class="tab-pane fade " id="Premium">

                            <table class="table table-striped table-bordered dt-responsive nowrap Premium_tab" cellspacing="0" width="100%">
                                <thead>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Targeted Number of Key Phrases Website</td>
                                    <td>15</td>


                                    <td>Article Submission Directories</td>
                                    <td>60</td>
                                </tr>
                                <tr>
                                    <td>Keyword+Competitors Analysis</td>

                                    <td><i class="fas fa-check"></i></td>
                                    <td>Backlinks from Forums</td>
                                    <td>50</td>
                                </tr>
                                <tr>
                                    <td>SEO Friendly URL Structures</td>
                                    <td><i class="fas fa-check"></i></td>

                                    <td>Backlinks from question and answers sites</td>
                                    <td><i class="fas fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td>Unique Meta Information</td>

                                    <td><i class="fas fa-check"></i></td>
                                    <td>Search Engine Submissions</td>
                                    <td><i class="fas fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td>Images and Alt tag</td>
                                    <td>All</td>

                                    <td>Community Creation in Social Networking Sites</td>
                                    <td><i class="fas fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td>Xml Sitemaps Creation</td>

                                    <td><i class="fas fa-check"></i></td>
                                    <td>Blogging</td>
                                    <td>10 Mini blogs</td>
                                </tr>
                                <tr>
                                    <td>Robots.txt</td>

                                    <td><i class="fas fa-check"></i></td>
                                    <td>Classifieds Submission</td>
                                    <td><i class="fas fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td>W3C Verified</td>

                                    <td><i class="fas fa-check"></i></td>
                                    <td>Price Press Release submission</td>
                                    <td>75</td>
                                </tr>
                                <tr>
                                    <td>Google Analytics Installation</td>

                                    <td><i class="fas fa-check"></i></td>
                                    <td>RSS feed</td>
                                    <td>75</td>
                                </tr>
                                <tr>
                                    <td>Directory Submissions</td>

                                    <td>3000</td>
                                    <td>Video sharing, Photo sharing</td>
                                    <td><i class="fas fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td>Social Bookmarking</td>

                                    <td>300 Home page</td>
                                    <td>SEM</td>
                                    <td><i class="fas fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td>Blog Commenting</td>

                                    <td>400 Home page</td>
                                    <td>SMO</td>
                                    <td><i class="fas fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td>.org and EDU Backlinks</td>

                                    <td>30</td>
                                    <td>PPC</td>
                                    <td>On Demand</td>
                                </tr>

                                </tbody>
                            </table>

                        </div>
                        <div class="tab-pane fade " id="Executive">

                            <table class="table table-striped table-bordered dt-responsive nowrap Executive_tab" cellspacing="0" width="100%">
                                <thead>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Targeted Number of Key Phrases Website</td>
                                    <td>30</td>


                                    <td>Article Submission Directories</td>
                                    <td>80</td>
                                </tr>
                                <tr>
                                    <td>Keyword+Competitors Analysis</td>

                                    <td><i class="fas fa-check"></i></td>
                                    <td>Backlinks from Forums</td>
                                    <td>60</td>
                                </tr>
                                <tr>
                                    <td>SEO Friendly URL Structures</td>
                                    <td><i class="fas fa-check"></i></td>

                                    <td>Backlinks from question and answers sites</td>
                                    <td><i class="fas fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td>Unique Meta Information</td>

                                    <td><i class="fas fa-check"></i></td>
                                    <td>Search Engine Submissions</td>
                                    <td><i class="fas fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td>Images and Alt tag</td>
                                    <td>All</td>

                                    <td>Community Creation in Social Networking Sites</td>
                                    <td><i class="fas fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td>Xml Sitemaps Creation</td>

                                    <td><i class="fas fa-check"></i></td>
                                    <td>Blogging</td>
                                    <td>10 Mini blogs</td>
                                </tr>
                                <tr>
                                    <td>Robots.txt</td>

                                    <td><i class="fas fa-check"></i></td>
                                    <td>Classifieds Submission</td>
                                    <td><i class="fas fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td>W3C Verified</td>

                                    <td><i class="fas fa-check"></i></td>
                                    <td>Price Press Release submission</td>
                                    <td>150</td>
                                </tr>
                                <tr>
                                    <td>Google Analytics Installation</td>

                                    <td><i class="fas fa-check"></i></td>
                                    <td>RSS feed</td>
                                    <td>100</td>
                                </tr>
                                <tr>
                                    <td>Directory Submissions</td>

                                    <td>3000</td>
                                    <td>Video sharing, Photo sharing</td>
                                    <td><i class="fas fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td>Social Bookmarking</td>

                                    <td>Entire Site</td>
                                    <td>SEM</td>
                                    <td><i class="fas fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td>Blog Commenting</td>

                                    <td>900 Home page</td>
                                    <td>SMO</td>
                                    <td><i class="fas fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td>.org and EDU Backlinks</td>

                                    <td>30</td>
                                    <td>PPC</td>
                                    <td>On Demand</td>
                                </tr>

                                </tbody>
                            </table>

                        </div>

                        <div class="clearfix"></div>
                    </div>



                </div>
            </div>
        </div>


    </section>

<?php
get_footer();
