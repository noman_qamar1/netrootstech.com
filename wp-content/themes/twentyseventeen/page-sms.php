<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_template_part( 'template-parts/header/header', 'sms' ); ?>

<div class="sticky-container">
    <div class="sticky-container">    <style>

    .okewa-style_3.okewa-text_3 #okewa-floating_cta{

        bottom: 55px;
        right: 13px;
    }
    .okewa-pulse_3{
        bottom: 55px !important;
    }
</style>
<ul class="sticky">

        <li>
            <img src="https://www.netrootstech.com<?php echo get_template_directory_uri(); ?>/assets/sms-assets/img/ireland.png" width="32" height="32">
            <p><a href="tel:+353578601255" target="_blank">+353 (57) 8601255</a></p>
</li>
<li>
    <img src="https://www.netrootstech.com<?php echo get_template_directory_uri(); ?>/assets/sms-assets/img/canada.png" width="32" height="32">
    <p><a href="tel:+16474906750" target="_blank">+1 (647) 490 6750</a></p>
</li>
<li>
    <img src="https://www.netrootstech.com<?php echo get_template_directory_uri(); ?>/assets/sms-assets/img/usa.png" width="32" height="32">
    <p><a href="tel:+13023001742" target="_blank">+1 (302) 300 1742</a></p>
</li> <li>
    <img src="https://www.netrootstech.com<?php echo get_template_directory_uri(); ?>/assets/sms-assets/img/pakistan.png" width="32" height="32">
    <p><a href="tel:+924236287770" target="_blank">+92 42 36287770</a></p>
</li>
</ul>
        <style>        .sticky-number {
                color: black;
                text-decoration: underline;
            }    </style>
    </div>
    <style>        .sticky-number {
            color: black;
            text-decoration: underline;
        }    </style>
</div>

	<script async="" src="https://embed.tawk.to/5cc2d770ee912b07bec4f7fa/default" charset="UTF-8" crossorigin="*"></script><script>    if (window.location.href.includes("Features")) {
        $(document).scrollTop(450);
    }</script>    
    <header>
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1" class=""></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2" class=""></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <!-- Slide One - Set the background image for this slide in the line below -->



                <div class="carousel-inner" role="listbox">
                <!-- Slide One - Set the background image for this slide in the line below -->
            <?php
            $count = 0;
            // the query
            $wpb_all_query = new WP_Query(array('post_type'=>'post', 'cat'=> 14 , 'post_status'=>'publish', 'posts_per_page'=>5)); ?>
            <?php if ( $wpb_all_query->have_posts() ) : ?>
                <!-- the loop -->
                <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?> 
                        <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), '' ); ?>


                <div class="carousel-item <?php if($count == 1){echo "active";} ?>" style="background-image: url('<?php echo $url?>');">
                    <div class="carousel-caption d-none d-md-block">

                           <div class="first"><h1 class="display-4" style="color: white;font-size: 60px;font-weight: bold">
                                <span style="background-color: #00000087;padding:5px;"><?php the_title();?></span>
                            </h1>
                            <div>
                              <p>
								  <?php echo the_content();?>
                            </p>
  
                            </div>
                            

                        </div> 

                    </div>
                </div>


                <?php $count++; endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>
                        </div>


            </div>
            <a style="margin:250px 0 0 50px; width:5%;" class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> 
			<a style="margin:250px 50px 0 0; width:5%;" class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span></a>
		</div>
    </header>
    <!--<section id="Modules" style="    padding-top: 50px;padding-bottom: 50px">--><!--    <div class="container">--><!--        <div class="row">--><!--            <div class="col-lg-12 text-center">--><!--                <h2 class="section-heading text-uppercase">NETROOTS SMS - <small>Student Management System</small></h2>--><!--                <h3 class="section-subheading text-muted" style="font-style: normal">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse id neque numquam perspiciatis quam sequi sit vel voluptatum! Commodi consequuntur dicta dignissimos ea, enim explicabo nesciunt numquam officia pariatur quo sit vero. Cum dolor doloribus ducimus mollitia necessitatibus quaerat quia quidem repellendus soluta tempore, totam voluptate voluptates voluptatum! Aspernatur, doloribus.</h3>--><!--            </div>--><!--        </div>--><!--    </div>--><!--</section>-->
    <div><span id="Modules"></span></div>
    <section class="features-icons bg-light text-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center"><h2 class="section-heading text-uppercase">NetRoots SMS </h2>
                    <h2>
                        <small>Build a better world, one student at a time</small>
                    </h2>
                    <h3 class="section-subheading text-muted" style="font-style: normal;margin-bottom: 10px">At
                        NetRoots, we believe in the transformative power of education. We also understand the obstacles
                        that prevent educational institutes from focusing exclusively on teaching and learning.
                        Therefore, we have created a student management system that streamlines, automates, and
                        condenses administrative and operational tasks, empowering administrators, teachers, students,
                        and even parents of minor students to work towards building a better world through
                        education. </h3>                <br></div>
            </div>
        </div>
        <div class="container"><h4 class="section-heading text-uppercase">Portals</h4>
            <h3 class="section-subheading text-muted" style="font-style: normal;margin-bottom: 22px"> NetRoots’
                customizable School management system contains four main portals, each of which has modules relevant to
                the users </h3>
            <div class="row">


                <?php
              // the query
            $wpb_all_query = new WP_Query(array('post_type'=>'post', 'cat'=> 19 , 'post_status'=>'publish', 'posts_per_page'=>20)); ?>

            <?php if ( $wpb_all_query->have_posts() ) : ?>
                <!-- the loop -->
                <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>


                <div class="col-lg-3 hvr-fade py-5">
                    <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                        <div class="features-icons-icon d-flex">
                            <i class="icn fas <?php the_field('sms-icon'); ?>  m-auto text-primary"></i>
                        </div>
                        <h3><?php the_title(); ?></h3>
                        <p class="lead mb-0"><?php echo the_content(); ?></p>
                    </div>
                    <button type="button" class="btn btn-info"><a href="<?php the_permalink(); ?>">Read more</a></button>
                </div>


            <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>


            </div>
        </div>
    </section>

         


<section class="content-section bg-primary text-white text-center" id="Features">
        <div class="container">
            <div class="content-section-heading">
                <h1 class="mb-5">WHY CHOOSE US</h1>
            </div>
           <div class="row">
            
                <?php
                $args=array(
                'post_type' => 'post',
                'post_status' => 'publish',
                'posts_per_page' => 20,
                'cat'=> 10,
                'order'=> 'ASC'
                );
                $my_query = null;
                $my_query = new WP_Query($args);
            if( $my_query->have_posts() ) {
                
            $i = 1;

                   ?> 
                    <?php
            while ($my_query->have_posts()) : $my_query->the_post();
                if($i % 4 == 0) {  
                print_r($my_query->the_post());?>
                <?php
                }
                ?>
                    <div class="col-lg-3 col-md-6 mb-5 py-3 hvr-fades ">

                    <span class="service-icon rounded-circle mx-auto mb-3">
                    <i class="fas <?php the_field('sms-icon-why-choose-us'); ?>"></i>
                    </span>
                    <div>
                    <h4><strong><?php the_title(); ?></strong></h4>
                    </div>
                    <div>
                    <p class="text-faded mb-0"><?php echo the_content(); ?></p>
                    </div>
                        </div>
               
                <?php    
                if($i % 4 == 0) { ?> 
                <?php

                $i++;
                }
            endwhile;
               ?>
               <?php
            }
            wp_reset_query();
            ?>
                </div>
      
    </div>
</section>

 
  

<section class="page-section testimonails" style="background-color: #f2f2f2;">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <div class="section-title text-left" data-animation="fadeInLeft">
                        <!-- Heading --> <h2 class="title">Latest Posts</h2></div>
                    <ul class="latest-posts">

            <?php
            // the query
            $wpb_all_query = new WP_Query(array('post_type'=>'post', 'cat'=> 9 , 'post_status'=>'publish', 'posts_per_page'=>2)); ?>
            <?php if ( $wpb_all_query->have_posts() ) : ?>
                <!-- the loop -->
                <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>

                        <li data-animation="fadeInLeft">
                            <div class="post-thumb">
                             
                             <?php the_post_thumbnail( array(85,85) );?>

                            </div>

                            <style>
                                .description a {text-decoration: underline; }
                                .description a:hover {text-decoration: underline;color: #00094d;}
                            </style>

                            <div class="post-details">
                                <div class="description"><a href="<?php echo get_page_link(242); ?>">
                                <?php the_title();?>

                                </a>
                                    </div>

                                <div class="meta">
                                <span class="time">
                                    <i class="fa fa-calendar"></i>
                                    <?php the_date( 'd-m-Y'); ?>
                                </span></div>
                            </div>
                        </li>


            <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>

                    </ul>
                </div>

                <!-- testemonials start from here -->

                <div class="col-sm-12 col-md-6 testimonails">
                    <div class="section-title text-left" data-animation="fadeInRight">
                        <!-- Heading --> <h2 class="title">Testimonials</h2>
                    </div>
                    <div class="owl-carousel pagination-1 dark-switch" data-effect="backSlide" data-pagination="true"
                         data-autoplay="true" data-navigation="false" data-singleitem="true"
                         data-animation="fadeInRight">

            <?php
            // the query
            $wpb_all_query = new WP_Query(array('post_type'=>'post', 'cat'=> 7 , 'post_status'=>'publish','order'=>'asc', 'posts_per_page'=>3)); ?>
            <?php if ( $wpb_all_query->have_posts() ) : ?>
                <!-- the loop -->
                <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>

                        <div class="item">
                            <div class="desc-border bottom-arrow quote">
                                <blockquote class="small-text">
                                  <?php echo the_content(); ?>
                                </blockquote>
                                <div class="star-rating text-right"><i class="fa fa-star text-color"></i> <i
                                        class="fa fa-star text-color"></i> <i class="fa fa-star text-color"></i> <i
                                        class="fa fa-star text-color"></i> <i
                                        class="fa fa-star-half-o text-color"></i></div>
                            </div>
                            <div class="client-details text-center">
                                <div class="client-image" style="width: 20%;
display: inline;">      
                             <?php the_post_thumbnail( array(80, 80), array('class' => 'img-circle'));?>
                                    </div>

                                <div class="client-details">
                                    <strong class="text-color">
                                        <?php the_title();?>
                                        </strong>
                                </div>
                            </div>
                        </div>

            <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>

                        
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="clients" class="page-section tb-pad-30">
        <div class="container">
            <div class="section-title text-center" data-animation="fadeInUp">
                <h1 class="title">Our Best Clients</h1></div>
            <div class="row">
                <div class="col-md-12 text-center" data-animation="fadeInDown">
                    <div class="owl-carousel navigation-1" data-pagination="false" data-items="6" data-autoplay="true"
                         data-navigation="true">


                         <?php
                // the query
                $wpb_all_query = new WP_Query(array('post_type'=>'post', 'cat'=> 8 , 'post_status'=>'publish', 'posts_per_page'=>15)); ?>
                <?php if ( $wpb_all_query->have_posts() ) : ?>
                    <!-- the loop -->
                    <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
             <a style="margin: 0 10px;">
             <?php the_post_thumbnail( array(150, 150));?>
                           
                 </a> 

          <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?> 


                 </div>
                </div>
            </div>
        </div>
    </section>    <!--testimonials-->


    <div id="get-quote" class="bg-color get-a-quote black text-center">
    <div class="container animated pulse visible" data-animation="pulse">
        <div class="row">
            <div class="col-md-12"><p>Get A Free Quote / Need a Help ? <a class="black" href="<?php echo get_page_link(47);?>">Contact
                        Us</a></p></div>
        </div>
    </div>
</div>


<?php
get_footer();
