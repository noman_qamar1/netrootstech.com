<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_template_part( 'template-parts/header/header', 'product' ); ?>



    <section class="slider">
        <div id="main-slider">
            <div id="carousel-example-generic1" class="carousel slide carousel-fade" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item"><img src="<?php echo get_template_directory_uri(); ?>/assets/crm-assets/img/Boost-Sales.jpg" class="banner_1" alt="" title="" width="100%"
                                           height="587px">
                        <div class="carousel-caption" style="color: #54a7c9;"><h1 align="left"
                                                                                  class="upper animation animated-item-1">
                                Boost Sales</h1>
                            <p align="left" class="description"> NRT CRM lets you analyse the needs of your customers,
                                <br>allowing you to up-sell and cross-sell </p></div>
                    </div>
                    <div class="item active"><img src="<?php echo get_template_directory_uri(); ?>/assets/crm-assets/img/1_hKMNDwaHZuTEIwgmVHKsJA.png" class="banner_1" alt=""
                                                  title="" width="100%" height="587px">
                        <div class="carousel-caption"><h2 class="upper animation animated-item-1"> Increase Customer
                                Satisfaction</h2>
                            <p class="description">Our CRM enhances your ability to provide support services <br>to your
                                consumers, improving customer satisfaction</p></div>
                    </div>
                    <div class="item"><img src="<?php echo get_template_directory_uri(); ?>/assets/crm-assets/img/CRM-Main.jpg" class="banner_1" alt="" title="" width="100%"
                                           height="587px">
                        <div class="carousel-caption"><h2 class="upper animation animated-item-1"
                                                          style="text-align: left;color: #f0cfa5"><span
                                    style="background-color: #f0cfa5;color: black;padding-left: 10px;padding-right: 10px;"
                                    class="text-color">NRT</span><br>Customer Relationship Management</h2>
                            <p style="color: #f0cfa5" align="left" class="description animation animated-item-2">From
                                your first contact with a potential customer, to support services after sale, <br>our
                                customer relationship management system helps you manage it all</p></div>
                    </div>
                </div>                <!-- Controls --> <a class="left carousel-control"
                                                           href="#carousel-example-generic1" role="button"
                                                           data-slide="prev"> <span class="fa fa-angle-left fa-2x"
                                                                                    aria-hidden="true"></span> <span
                        class="sr-only">Previous</span></a> <a class="right carousel-control"
                                                               href="#carousel-example-generic1" role="button"
                                                               data-slide="next"> <span
                        class="fa fa-angle-right fa-2x" aria-hidden="true"></span> <span class="sr-only">Next</span></a>
            </div>
        </div>
    </section>    <!--Slider END-->    <span></span>
    <section id="Modules" class="page-section">
        <div class="container">
            <div class="section-title">                <!-- Heading -->
                <h1 class="title text-center">NetRoots CRM -
                    <small style="color: #077388">Make a customer, not a sale</small>
                </h1>
            </div>
            <p style="padding-bottom: 10px;color: black" class="text-center">Build better relationships, increase
                customer retention, and boost sales with NetRoots CRM. The two modules of our CRM software enable you to
                monitor your interactions with existing or potential clients and resolve their complaints in an
                efficient and timely manner, improving your rapport and consequently your revenues. </>
            <div class="row text-center">

                    
            <?php
              // the query
            $wpb_all_query = new WP_Query(
				array('order' =>'DESC','post_type'=>'page','post_parent'=>'119','post_status'=>'publish', 'posts_per_page'=>2)
			); ?>

            <?php if ( $wpb_all_query->have_posts() ) : ?>
                <!-- the loop -->
                <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>

                <div style="padding-bottom: 10px;" class="col-sm-6 col-md-6 hvr-grow-shadow">
                <i class="<?php the_field('crm-icon'); ?>  medium text-color fa-2x icons-circle border-color"></i>
                

                      <h4><?php the_title(); ?></h4>

                      <p style="margin-right: 15px;color: black"><?php the_field('post-content'); ?> </p>

                      <hr>
                        <button class="btn btn-default"><a href="<?php the_permalink(); ?>">Read More</a></button>
            </div>
                

            <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>


            </div>
        </div>
    </section>


    
    <section class="quote page-section no-pad bg-color">
        <div class="container">
            <div class="row">
                <div class="col-md-12 top-pad-40 bottom-pad-40 text-center"><h3
                        class="text-uppercase white animated fadeInUp visible" data-animation="fadeInUp">For Help Or
                        A Free Quote</h3>                    <a id="Features" href="<?php echo get_page_link(47); ?>"
                                                                class="btn btn-transparent-white btn-lg animated pulse visible"
                                                                data-animation="pulse">Contact us</a></div>
            </div>
        </div>
    </section>    <!--service START-->
    <section class="page-section">
        <div class="container">
            <div class="section-title" data-animation="fadeInUp">                <!-- Heading --> <h1 class="title">Why
                    Choose Us</h1></div>
            <div class="row">
                <div class="col-md-8">
                    <div class="row services icons-circle">
                        <div class="item-box icons-color hover-black col-sm-6 col-md-6 hvr-glow"
                             data-animation="fadeInLeft" style="background-color: #d8d8d852"><a>
                                <!-- Icon --> <i class="fa fa-money i-4x border-color"></i>
                                <!-- Title --> <h5 class="title">Affordable</h5>
                                <!-- Text -->
                                <div>Get more for less <span style="color: #d8d8d852;visibility: hidden">Get more for less Get more for less Get more for less Get more forfor less Get more for</span>
                                </div>
                            </a></div>
                        <div class="item-box icons-color hover-black col-sm-6 col-md-6  hvr-glow"
                             data-animation="fadeInLeft" style="background-color: #d8d8d852"><a>
                                <!-- Icon --> <i class="fa fa-user-plus i-4x border-color"></i>
                                <!-- Title --> <h5 class="title">User-friendly</h5>
                                <!-- Text -->
                                <div>Make your life easy with the simple interface<span
                                        style="color: #d8d8d852;visibility: hidden">Get more for less Get more for less Get more for less Get more forfor less Get more for</span>
                                </div>
                            </a></div>
                        <div class="item-box icons-color hover-black col-sm-6 col-md-6 hvr-glow"
                             data-animation="fadeInUp" style="background-color: #d8d8d852"><a>
                                <!-- Icon --> <i class="fa fa-mobile-phone i-4x border-color"></i>
                                <!-- Title --> <h5 class="title">Customizable</h5>
                                <!-- Text -->
                                <div>Add features to meet your specific business needs<span
                                        style="color: #d8d8d852;visibility: hidden">Get more for less Get more for less Get more for less Get more forfor less Get more for</span>
                                </div>
                            </a></div>
                        <div class="item-box icons-color hover-black col-sm-6 col-md-6 hvr-glow"
                             data-animation="fadeInRight" style="background-color: #d8d8d852"><a>
                                <!-- Icon --> <i class="fa fa-bars i-4x border-color"></i>
                                <!-- Title --> <h5 class="title">Responsive and convenient</h5>
                                <!-- Text -->
                                <div>Use it on your cellphone, tablet, or computer<span
                                        style="color: #d8d8d852;visibility: hidden">Get more for less Get more for less Get more for less Get more forfor less Get more for</span>
                                </div>
                            </a></div>
                        <div class="item-box icons-color hover-black col-sm-6 col-md-6 hvr-glow"
                             data-animation="fadeInUp" style="background-color: #d8d8d852"><a>
                                <!-- Icon --> <i class="fas fa-shapes i-4x border-color"></i>
                                <!-- Title --> <h5 class="title">Custom color themes</h5>
                                <!-- Text -->
                                <div>Choose from among a range of colors to configure your system’s aesthetics according
                                    to your preferences
                                </div>
                            </a></div>
                        <div class="item-box icons-color hover-black col-sm-6 col-md-6 hvr-glow"
                             data-animation="fadeInRight" style="background-color: #d8d8d852"><a>
                                <!-- Icon --> <i class="fa fa-edit i-4x border-color"></i>
                                <!-- Title --> <h5 class="title">Frequent updates</h5>
                                <!-- Text -->
                                <div>Receive frequent updates so that your CRM system keeps running smoothly and
                                    improves consistently to remain its best version possible.
                                </div>
                            </a></div>
                    </div>
                </div>
                <div class="col-md-4 animated fadeInRight visible" data-animation="fadeInRight"><img src="<?php echo get_template_directory_uri(); ?>/assets/crm-assets/img/crm.png"
                                                                                                     alt="" width="330"
                                                                                                     height="415"></div>
            </div>
        </div>
    </section><span id="testimonials"></span>
    <section class="page-section testimonails" style="background-color: #f2f2f2;">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <div class="section-title text-left" data-animation="fadeInLeft">
                        <!-- Heading --> <h2 class="title">Latest Posts</h2></div>
                    <ul class="latest-posts">

            <?php
            // the query
            $wpb_all_query = new WP_Query(array('post_type'=>'post', 'cat'=> 6 , 'post_status'=>'publish', 'posts_per_page'=>3)); ?>
            <?php if ( $wpb_all_query->have_posts() ) : ?>
                <!-- the loop -->
                <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>

                        <li data-animation="fadeInLeft">
                            <div class="post-thumb">
                             
                             <?php the_post_thumbnail( array(85,85) );?>

                            </div>

                            <style>
                                .description a {text-decoration: underline; }
                                .description a:hover {text-decoration: underline;color: #00094d;}
                            </style>

                            <div class="post-details">
                                <div class="description"><a href="<?php echo get_page_link(121);?>">
                                <?php the_title();?>

                                </a>
                                    </div>

                                <div class="meta">
                                <span class="time">
                                    <i class="fa fa-calendar"></i>
                                    <?php the_date( 'd-m-Y'); ?>
                                </span></div>
                            </div>
                        </li>


            <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>

                    </ul>
                </div>

                <!-- testemonials start from here -->

                <div class="col-sm-12 col-md-6 testimonails">
                    <div class="section-title text-left" data-animation="fadeInRight">
                        <!-- Heading --> <h2 class="title">Testimonials</h2>
                    </div>
                    <div class="owl-carousel pagination-1 dark-switch" data-effect="backSlide" data-pagination="true"
                         data-autoplay="true" data-navigation="false" data-singleitem="true"
                         data-animation="fadeInRight">

            <?php
            // the query
            $wpb_all_query = new WP_Query(array('post_type'=>'post', 'cat'=> 7 , 'post_status'=>'publish','order'=>'asc', 'posts_per_page'=>3)); ?>
            <?php if ( $wpb_all_query->have_posts() ) : ?>
                <!-- the loop -->
                <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>

                        <div class="item">
                            <div class="desc-border bottom-arrow quote">
                                <blockquote class="small-text">
                                  <?php echo the_content(); ?>
                                </blockquote>
                                <div class="star-rating text-right"><i class="fa fa-star text-color"></i> <i
                                        class="fa fa-star text-color"></i> <i class="fa fa-star text-color"></i> <i
                                        class="fa fa-star text-color"></i> <i
                                        class="fa fa-star-half-o text-color"></i></div>
                            </div>
                            <div class="client-details text-center">
                                <div class="client-image">      
                             <?php the_post_thumbnail( array(80, 80), array('class' => 'img-circle'));?>
                                    </div>

                                <div class="client-details">
                                    <strong class="text-color">
                                        <?php the_title();?>
                                        </strong>
                                </div>
                            </div>
                        </div>

            <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>

                        
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="clients" class="page-section tb-pad-30">
        <div class="container">
            <div class="section-title text-center" data-animation="fadeInUp">
                <h1 class="title">Our Best Clients</h1></div>
            <div class="row">
                <div class="col-md-12 text-center" data-animation="fadeInDown">
                    <div class="owl-carousel navigation-1" data-pagination="false" data-items="6" data-autoplay="true"
                         data-navigation="true">


                         <?php
                // the query
                $wpb_all_query = new WP_Query(array('post_type'=>'post', 'cat'=> 8 , 'post_status'=>'publish', 'posts_per_page'=>10)); ?>
                <?php if ( $wpb_all_query->have_posts() ) : ?>
                    <!-- the loop -->
                    <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
             <a style="margin:10px;">
             <?php the_post_thumbnail( array(150, 150));?>
                           
                 </a> 

          <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?> 


                 </div>
                </div>
            </div>
        </div>
    </section>    <!--testimonials-->


<div id="get-quote" class="bg-color get-a-quote black text-center">
    <div class="container animated pulse visible" data-animation="pulse">
        <div class="row">
            <div class="col-md-12"><p>Get A Free Quote / Need a Help ? <a class="black" href="<?php echo get_page_link(47);?>">Contact
                        Us</a></p></div>
        </div>
    </div>
</div>


<?php
get_footer();
