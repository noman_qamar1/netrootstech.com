<section class="content-section bg-primary text-white text-center" id="Features">
        <div class="container">
            <div class="content-section-heading">
                <h1 class="mb-5">WHY CHOOSE US</h1>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6 mb-5 mb-lg-0 py-3 hvr-fades">
                    <span class="service-icon rounded-circle mx-auto mb-3">
                    <i class="fas fa-tags"></i></span>

                    <h4><strong>Affordable</strong></h4>
                    <p class="text-faded mb-0">Get more for less</p>
                </div>
                
                <div class="col-lg-3 col-md-6 mb-5 mb-lg-0 py-3 hvr-fades "><span class="service-icon rounded-circle mx-auto mb-3">            <i class="fas fa-history"></i>          </span>
                    <h4><strong>Round-the-clock support</strong></h4>
                    <p class="text-faded mb-0">Call us any time for help with our SMS system.</p></div>
                <div class="col-lg-3 col-md-6 mb-5 mb-md-0 py-3 hvr-fades"><span class="service-icon rounded-circle mx-auto mb-3">            <i class="fas fa-mobile-alt"></i></span>
                    <h4><strong>Responsive</strong></h4>
                    <p class="text-faded mb-0">Use it on your cellphone, tablet, or computer.</p></div>
                <div class="col-lg-3 col-md-6 py-3 hvr-fades"><span class="service-icon rounded-circle mx-auto mb-3">            <i class="fas fa-external-link-alt"></i>          </span>                <h4><strong>Ease
                            of use</strong></h4>
                    <p class="text-faded mb-0">Make your life easy with the system’s simple interface.</p></div>
            </div>
            <div class="row pt-5">
                <div class="col-lg-3 col-md-6 mb-5 mb-lg-0 py-3 hvr-fades"><span class="service-icon rounded-circle mx-auto mb-3">            <i class="fab fa-intercom"></i>          </span>
                    <h4><strong>Customizable</strong></h4>
                    <p class="text-faded mb-0">Add features/modules to meet your specific business needs.</p></div>
                <div class="col-lg-3 col-md-6 mb-5 mb-lg-0 py-3 hvr-fades"><span class="service-icon rounded-circle mx-auto mb-3">            <i class="fas fa-sync-alt"></i>          </span>
                    <h4><strong>updates</strong></h4>
                    <p class="text-faded mb-0">Receive frequent updates so that your HR system keeps running
                        smoothly.</p></div>
                <div class="col-lg-3 col-md-6 mb-5 mb-md-0 py-3 hvr-fades"><span class="service-icon rounded-circle mx-auto mb-3">            <i class="fas fa-tachometer-alt"></i>          </span>                <h4><strong>Increased
                            collaboration</strong></h4>
                    <p class="text-faded mb-0">Collaborate with your team members</p></div>
                <div class="col-lg-3 col-md-6 py-3 hvr-fades"><span class="service-icon rounded-circle mx-auto mb-3">            <i class="fas fa-palette"></i>          </span>                <h4><strong>Multi
                            Dashboard</strong></h4>
                    <p class="text-faded mb-0">Choose from among a range of colors to configure your system’s aesthetics
                        according to your preferences.</p></div>
            </div>
        </div>
    </section>