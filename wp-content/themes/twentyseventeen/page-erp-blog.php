<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_template_part( 'template-parts/header/header', 'erp' ); ?>

<head>
   
    <link href="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/css/responsive.css" rel="stylesheet" type="text/css">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/css/stylesheet.css" rel="stylesheet" type="text/css">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/css/owl.carousel.min.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/css/owl.theme.default.min.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/animate.css-master/animate.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
          integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/js/bootstrap.min.js"></script>
    <style>
        .navbar-default {
            background-color: white;
            border-color: #1c7992;
        }
		.banner{
			padding: 100px 0;
		}
		.banner h1{
			font-size:80px;
		}
		.banner h2{
			font-size:70px;
		}
        .navbar-default .navbar-nav > li > a {
            color: black;
        }

        .navbar-nav > li > a {
            font-size: 16px;
        }

        .navbar {
            min-height: 50px;
            margin-bottom: 0;
            border: 1px solid transparent;
            padding-top: 10px;
            padding-bottom: 10px;
            top: 0;
            position: sticky;
            z-index: 1;
            box-shadow: 0px 1px 10px #999;
            border-radius: 0;
            height: 123px;
            border-bottom: 2px solid #007488;
            width: 100%;
        }

        header {
            width: 100%;
            padding: 0 0;
        }

        .btn-warning {
            color: #fff;
            background-color: #FF9800;
            border-color: #FF9800;
            margin-top: 8px;
        }

        .navbar-default .navbar-nav > li > a:hover {
            color: #007488;
        }    </style>
</head>
<!-- ------------------------------------------------template body start from here -->

<style>    .number li {
            font-weight: bold;
            list-style-type: decimal !important;
        }

        .post-content {
            text-align: justify;
        }</style>
    <div class="page-header">
        <div class="container"><h1 class="title">Blogs</h1></div>
        
           <div class="breadcrumb-box">
            <div class="container">
                <ul class="breadcrumb"><!--                    <li><a href="index.php">Home</a></li>-->
                    <!--                    <li><a href="page-ContactUs.php">Contact us</a></li>-->
                    <!--                    <li><a href="#">Pages</a></li>-->
                    <!--                    <li class="active"> Blog</li>-->                </ul>
            </div>
        </div>
        
    </div>    <!--page-header-->
    <div class="container">

<?php
// the query
$wpb_all_query = new WP_Query(array('post_type'=>'post', 'cat'=> 33 , 'post_status'=>'publish', 'posts_per_page'=>10)); ?>
<?php if ( $wpb_all_query->have_posts() ) : ?>
    <!-- the loop -->
    <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>

        <section class="page-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12"><h1><?php echo the_title(); ?></h1>
                        <div class="post-image opacity"><?php echo the_post_thumbnail('full');?></div>
                        <div class="post-meta">                        <!-- Author  --> <span class="author"><i
                                        class="fa fa-user"></i><?php echo the_author(); ?></span>                        <!-- Meta Date -->
                            <span class="time"><i class="fa fa-calendar"></i> <?php the_date(' F j, Y'); ?></span>
                            <!-- Comments --> <span class="category "><i class="fa fa-heart"></i> Technology, Software, ERP</span>
                            <!-- Category --> <span class="comments pull-right"><i class="fa fa-comments"></i> Comments </span>
                        </div>
                        <div class="post-content top-pad-20">
                            <?php echo the_content(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
    <?php wp_reset_postdata(); ?>
<?php else : ?>
    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>
    </div> 


<!-- ----------------------------------------------template end here -->

<script>
	AOS.init();
	</script>
<?php
get_footer();