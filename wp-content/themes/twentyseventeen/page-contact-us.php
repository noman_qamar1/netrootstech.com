<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<main class="cd-main-content">
<style>
    body{
        background-color: white;

    }


</style>


<div class="cotainer-fluid">
    <div class="contact-banner">
        <img src="<?php echo get_template_directory_uri();?>/assets/images/nrt-contact-banner.jpg" alt="banner-img" class="img-responsive"/>
    </div>
</div>

    <div class="container">
    <div class="col-lg-12 text-center contact" style="margin-top: 100px;">
    </div>
    <div class="container">
        <div class="main-contact">
        <div class="row">
            <div class="col-sm-6">                                <!-- Field 4 -->
                <h3>Head Office</h3>
                <ul class="color-black regular_font__Montserrat contact-details">
                    <li class="office_location" style="margin-top: 5px;"><span><i class="fas fa-map-marker-alt"></i></span>11 Grattan St, Kylekiproe, Portlaoise, Laois, R32 HY59,Ireland                                </li>
                    <li class="office_numder"><span><i class="fas fa-phone-square"></i></span><a href="tel:+353578601255" target="_blank">+353 (57) 8601255</a></li>
                    <li class="office_email"><span><i class="far fa-envelope"></i></span><a href="mailto:info@netrootstech.com">info@netrootstech.com</a>
                    </li>

                </ul>
                <h3>USA Office</h3>
                <ul class="color-black regular_font__Montserrat contact-details">

                    <li class="office_location" style="margin-top: 5px;"><span><i class="fas fa-map-marker-alt"></i></span>1845 Freemansburg Ave, Easton, Pennsylvania 18042,USA                            </li>
                    <li class="office_numder"><span><i class="fas fa-phone-square"></i></span><a href="tel:+13023001742" target="_blank">+1 (302) 300 1742</a></li>
                    <li class="office_numder2"><span><i class="fas fa-phone-square"></i></span><a href="tel:" target="_blank">+1 (610) 905 3046</a></li>
                    <li class="office_email"><span><i class="far fa-envelope"></i></span><a href="mailto:info@netrootstech.com">info@netrootstech.com</a>
                    </li>

                </ul>
                <h3>Canada Office</h3>
                <ul class="color-black regular_font__Montserrat contact-details">

                    <li class="office_location" style="margin-top: 5px;"><span><i class="fas fa-map-marker-alt"></i></span> 1325 Derry Rd East,
                        Unit 3 (2nd Floor)
                        Mississauga, ON, L5T1B6, CANADA                            </li>

                    <li class="office_numder"><span><i class="fas fa-phone-square"></i></span> <a href="tel:+16474906750" target="_blank">+1 (647) 490 6750</a></li>

                    <li class="office_email"><span><i class="far fa-envelope"></i></span><a href="mailto:info@netrootstech.com">info@netrootstech.com</a>
                    </li>

                </ul>
            </div>




            <div class="col-sm-6">
                <h3>Contact us/Request a demo</h3>


                <?php echo do_shortcode('[contact-form-7 id="5" title="Contact form 1"]') ?>
            </div>




        </div>
    </div>
    </div>
</div>
</main>
<?php
get_footer();
