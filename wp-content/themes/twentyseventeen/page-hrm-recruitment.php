<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_template_part( 'template-parts/header/header', 'hrm' ); ?>

<!-- ------------------------------------------------template body start from here -->

<div class="top-banner">
        <div class="container">
            <div class="banner-content">
                <h1>
                   Hiring new employees doesn’t <br>have to be a hassle!
                </h1>
                <p>
                    Fórsa HR software gives you the most efficient, effective,
                </p>
                <p>
                   and simple recruitment management solutions to all
                </p>
                <p>
                	 your hiring problems.
                </p>


            </div>




        </div>



    </div>

    <!--banner ends-->


    <!--module-area start-->

    <div class="module-area">

        <div class="container">
            <h2>
                Sub-modules
            </h2>
            <p>
                The recruitment management software of our HRMS features four customizable sub-modules, that together simplify the process of requesting, advertising for, and selecting new employees.
            </p>
            <div class="module-chart">
                <div class="row">
                    <div class="col-md-4">

                        <div class="branches-Recruitment" data-aos="flip-up">

                            <a href="#">
                                <h4>
                                    Job Requisitions
                                </h4>
                                <p>
                                    The Job Requisitions sub-module allows you to create a job description for a required employee, and to specify the reason for the new requisition, after which the requisition is subject to the approval of relevant personnel.
                                </p>
                            </a>

                        </div>

                        <div class="branches-Recruitment" data-aos="flip-up">
                            <a href="#">

                                <h4>
                                   Job Applications
                                </h4>
                                <p>
                                   The Job Applications sub-module stores the applications received for each position. HR personnel can view applications within the HR software.
                                </p>
                            </a>

                        </div>



                    </div>
                    <div class="col-md-4">

                        <figure class="module-tablet"  data-aos="zoom-in">

                            <a href="#">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/images/module-tab.png" alt="dashboard" width="100%">
                            </a>

                        </figure>

                    </div>
                    <div class="col-md-4">

                        

                        <div class="branches-right-Recruitment" data-aos="flip-up">
                            <a href="#">

                                <h4>
                                   Applicant Status 
                                </h4>
                                <p>
                                    After a decision has been made about an employee’s application, she is either rejected, selected, or put on hold through the Applicant Status sub-module.
                                </p>
                            </a>

                        </div>

                        <div class="branches-right-Recruitment" data-aos="flip-up" style="margin-top: 116px">
                            <a href="#">

                                <h4>
                                    Offer Letter
                                </h4>
                                <p>
                                   Finally, if the applicant is a good fit for your company, the Offer Letter sub-module is used to generate an offer letter for her.
                                </p>
                            </a>

                        </div>




                    </div>


                </div>



            </div>





        </div>

    </div>

    <!--module-area ends-->


    <!--content-wraper start-->

    <div class="content-wraper">

        <div class="container">

            <div class="row">



                <div class="col-md-6">

                    <div class="wraper-text"data-aos="fade-right"
                         data-aos-offset="300"
                         data-aos-easing="ease-in-sine">

                        <h2>
                            Customizable and Integrated 
                        </h2>
                        <p>
                           
After an offer letter is generated through our recruitment management software, the new hires information is added automatically in other modules such as Fórsa’s payroll software, and the time and attendance module. In addition, while our cloud-based recruitment management software is comprehensive, we offer to customize it according to your specific requirements so that you get exactly what you need - nothing more, nothing less! 

                        </p>
                       



                    </div>



                </div>
                <div class="col-md-6">
                    <figure class="wraper-image" data-aos="zoom-in-up">

                        <a href="#">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/images/recruitment-laptop.png" alt="dashboard" width="100%">
                        </a>

                    </figure>

                </div>

            </div>



        </div>

    </div>

    <!--content-wraper end-->

    <!--text area-->

    <div class="text-area" data-aos="zoom-in-down">

        <div class="container">

            <h2>
                Why use Fórsa’s Recruitment Management Solutions?
            </h2>

            <p>

                Fórsa’s simple but comprehensive recruitment management module automates the entire recruitment process to save time and money, streamline data collection, and encourage more informed decision-making. The recruitment process often requires long, tedious hours of paperwork, which Fórsa’s recruitment module cuts down significantly. In addition, it saves overhead costs, and collects applicant data within one database. With all applicant information together in one place, it becomes easier to make smarter hiring decisions.

With the Recruitment Module of Fórsa HR software, you need no other recruitment management software!


            </p>


        </div>



    </div>


<!-- ----------------------------------------------template end here -->

<script>
	AOS.init();
	</script>
<?php
get_footer();