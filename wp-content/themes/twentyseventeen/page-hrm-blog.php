<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */


get_template_part( 'template-parts/header/header', 'hrm' ); ?>
<!-- ------------------------------------------------template body start from here -->

<style>    .number li {
            font-weight: bold;
            list-style-type: decimal !important;
        }

        .post-content {
            text-align: justify;
        }</style>
    <div class="page-header">
        <div class="container"><h1 class="title">Blogs</h1></div>
        <div class="breadcrumb-box">
            <div class="container">
                <ul class="breadcrumb"><!--                    <li><a href="index.php">Home</a></li>-->
                    <!--                    <li><a href="page-ContactUs.php">Contact us</a></li>-->
                    <!--                    <li><a href="#">Pages</a></li>-->
                    <!--                    <li class="active"> Blog</li>-->                </ul>
            </div>
        </div>
    </div>    <!--page-header-->
    <div class="container">

<?php
// the query
$wpb_all_query = new WP_Query(array('post_type'=>'post', 'cat'=> 23 , 'post_status'=>'publish', 'posts_per_page'=>10)); ?>
<?php if ( $wpb_all_query->have_posts() ) : ?>
    <!-- the loop -->
    <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>

        <section class="page-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12"><h1><?php echo the_title(); ?></h1>
                        <div class="post-image opacity"><?php echo the_post_thumbnail('full');?></div>
                        <div class="post-meta">                        <!-- Author  --> <span class="author"><i
                                        class="fa fa-user"></i><?php echo the_author(); ?></span>                        <!-- Meta Date -->
                            <span class="time"><i class="fa fa-calendar"></i> <?php the_date(' F j, Y'); ?></span>
                            <!-- Comments --> <span class="category "><i class="fa fa-heart"></i> Technology, Software, HRM</span>
                            <!-- Category --> <span class="comments pull-right"><i class="fa fa-comments"></i> Comments </span>
                        </div>
                        <div class="post-content top-pad-20">
                            <?php echo the_content(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
    <?php wp_reset_postdata(); ?>
<?php else : ?>
    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>
    </div> 


<!-- ----------------------------------------------template end here -->

<script>
	AOS.init();
	</script>
<?php
get_footer();