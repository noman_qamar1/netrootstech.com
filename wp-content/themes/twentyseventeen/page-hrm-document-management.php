<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_template_part( 'template-parts/header/header', 'hrm' ); ?>
<!-- ------------------------------------------------template body start from here -->


    <div class="top-banner">
        <div class="container">
            <div class="banner-content">
                <h1>
                    Document Management
                </h1>
                <p>
                    Have you been meaning to sort through the enormous stack 
                </p>
                <p>
                    of files in the closet or the piles of paper on your desk?
                </p>


            </div>




        </div>



    </div>

    <!--banner ends-->


    <!--module-area start-->

    <div class="module-area">

        <div class="container">
            <h2>
                Have you been meaning to sort through the enormous stack of files in the closet or the piles of paper on your desk? 
            </h2>
            <p>
                Are you looking for document management solutions to help you out?
Reduce that mess and never lose another document again, thanks to our document management software. The Document Management module of Fórsa HR software stores and retrieves human resources documents, keeping a record of the versions created and modified. 

            </p>
            <div class="module-chart">
                <div class="row">
                    <div class="col-md-4">

                        <div class="branches-documents" data-aos="flip-up">

                            <a href="#">
                                <h4>
                                  Employee Documents
                                </h4>
                                <p>
                                    Our document management software provides a safe storage space to store information such as birth certificates, resumes, and employment contracts, ensuring that they are easily accessible and unlikely to get lost.
                                </p>
                            </a>

                        </div>

                        

                    </div>
                    <div class="col-md-4">

                        <figure class="module-tablet"  data-aos="zoom-in">

                            <a href="#">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/images/documents-tab.png" alt="dashboard" width="100%">
                            </a>

                        </figure>

                    </div>
                    <div class="col-md-4">

                        <div class="branches-right-documents" data-aos="flip-up">
                            <a href="#">

                                <h4>
                                    Project Proposals
                                </h4>
                                <p>
                                   As with employee documents, our document management software allows project proposals to also not only be safely stored but also shared with the relevant people on one convenient platform.
	

                                </p>
                            </a>

                        </div>

                        

                    </div>


                </div>



            </div>



        </div>

    </div>

    <!--module-area ends-->


    <!--content-wraper start-->

    <div class="content-wraper">

        <div class="container">

            <div class="row">



                <div class="col-md-6">

                    <div class="wraper-text"data-aos="fade-right"
                         data-aos-offset="300"
                         data-aos-easing="ease-in-sine">

                        <h2>
                            Comprehensive and Integrated
                        </h2>
                        <p>
                            Fórsa document management software allows you to store different types of documents, ranging from employee documents to project proposals. Our HR software simultaneously stores employee birth certificates, resumes and identification information, MOUs, agreements, project proposals, policy manuals, and many other kinds of documents.
In addition, residing within our comprehensive HR software, the document management software integrates with other modules such as the employee database, payroll software, and training module. As a result, information and documents are shared quickly.


                        </p>



                    </div>



                </div>
                <div class="col-md-6">
                    <figure class="wraper-image" data-aos="zoom-in-up">

                        <a href="#">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/images/documents-laptop.png" alt="dashboard" width="100%">
                        </a>

                    </figure>

                </div>

            </div>



        </div>

    </div>

    <!--content-wraper end-->

    <!--text area-->

    <div class="text-area" data-aos="zoom-in-down">

        <div class="container">

            <h2>
                Why use Fórsa’s Document Management Software? 
            </h2>

            <p>

               Besides the quick sharing of information, cloud-based Fórsa document management software has a host of benefits. It makes data more accessible, and allows you to easily search for and find specific data that you need. It also keeps your data secure, making it immune to theft and loss. Furthermore, the document manager enhances collaboration within your workplace, as documents and information can be shared instantaneously and with ease. In addition to all its benefits to for your workplace, Fórsa HR software enables you to become more environmentally friendly by helping you save tons of paper. 
Opt for Fórsa document management solutions to declutter your desk and simplify your life. nd simplify your life.

            </p>


        </div>



    </div>
    
<!-- ----------------------------------------------template end here -->

<script>
	AOS.init();
	</script>
<?php
get_footer();