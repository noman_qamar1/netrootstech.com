<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>



<?php
if (in_category(7)){ ?>
	
            <section id="projects" class="projects-section bg-light">
                <div class="container">        <!-- Featured Project Row -->                <!-- Project One Row -->
					 <?php
                // the query
                $wpb_all_query = new WP_Query(array('post_type'=>'post', 'cat'=> 7 , 'post_status'=>'publish', 'posts_per_page'=>10)); ?>
                <?php if ( $wpb_all_query->have_posts() ) : ?>
                    <!-- the loop -->
                    <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
                    <div class="row justify-content-center no-gutters mb-5 mb-lg-0">
                        <div class="col-lg-12">
                            <div class="bg-black text-center h-100 project">
                                <div class="d-flex h-100">
									
                                    <div class="project-text w-100 my-auto text-center text-lg-left"><h4
                                                class="text-white" style="font-size: 36px;"><?php the_title(); ?></h4>
                                        <p class="mb-0 text-white-50" style="font-size: 18px;"><?php the_content(); ?></p>
                                        <hr class="d-none d-lg-block mb-0 ml-0">
                                    </div>
									
           
                                </div>
                            </div>
                        </div>
                    </div>
				<?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>   
                </div>
            </section>
	<?php
}
else{
	echo "<h1>THis post have no content</h1>";
}
?>


<?php
get_footer();

