<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_template_part( 'template-parts/header/header', 'erp' ); ?>

<head>
   
    <link href="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/css/responsive.css" rel="stylesheet" type="text/css">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/css/stylesheet.css" rel="stylesheet" type="text/css">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/css/owl.carousel.min.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/css/owl.theme.default.min.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/animate.css-master/animate.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
          integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/css/erp-sales-module.css" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/js/bootstrap.min.js"></script>
    
</head>
	<!--   banner start  -->
<div class="banner">
    <div class="container"><h2 style="color: white" class="wow slideInDown">NetRoots</h2>
        <h1 style="color: white" class="wow slideInRight" data-wow-delay="0.5s">Enterprise Resource Planning</h1>
        <p class="wow slideInUp" data-wow-delay="0.7s">Your business processes automated, integrated, and simplified</p>
    </div>
</div>
<div class="sticky-container">    
    <style>        .sticky-number {
            color: black;
            text-decoration: underline;
        }    </style>
</div>                   <!--   banner end  -->
<!--   about us start  -->
<div class="aboutus wow fadeInUp" data-wow-delay="0.3s" id="about">
    <div class="container"><h1 class="wow slideInDown" data-wow-delay="1s">NetRoots ERP</h1>
        <p class="subheading">Boost productivity, save time, and increase profits</p>
        <p>NetRoots ERP software integrates the core processes of your business into a single, customizable system to
            foster growth, reduce operational costs, and improve efficiency. By automating virtually all business
            operations, our ERP ensures relief from tedious and redundant tasks, allowing you to focus on growing your
            business. In addition, NetRoots ERP stores all customer, employee, and financial data in one place, helping
            strengthen collaboration among departments and employees. With NetRoots ERP, building a more cohesive,
            evolved, and profitable business has never been simpler. </p></div>
</div>                                <!--   about us end  -->
<!--   module start  -->
<div id="modules" class="module">
    <div class="container">
        <div class="heading"><h1>Core Modules</h1>
            <p>NetRoots ERP’s 8 core modules integrate all your key business operations into one system</p></div>
        <div class="row">
			<div class="col-xs-12 col-sm-6 col-md-3 wow shake">
				<a href="http://localhost/netrootstech.com/erp/erp_sales/">
                
                    <div class="moduledata">
                        <div class="imgholder1"></div>
                        <div class="moduleinfo"><h4>Sales</h4>
                            <p>Track and manage all sales activities <span style="visibility: hidden">active</span></p>
                        </div>
                        <i class="fa fa-long-arrow-right"></i>
           				 </div>
			</a></div>
    <div class="col-xs-12 col-sm-6 col-md-3 wow shake" data-wow-delay="3.2s">
		<a href="http://localhost/netrootstech.com/erp/erp_procurement/">
            <div class="moduledata">
                <div class="imgholder2"></div>
                <div class="moduleinfo"><h4>Procurement</h4>
                    <p>Manage and record all procurement tasks </p></div>
                <i class="fa fa-long-arrow-right"></i>
        </div></a>
</div>
<div class="col-xs-12 col-sm-6 col-md-3 wow shake" data-wow-delay="1s">
    <a href="http://localhost/netrootstech.com/erp/erp_inventory/">
        <div class="moduledata">
            <div class="imgholder3"></div>
            <div class="moduleinfo"><h4>Inventory</h4>
                <p>Log stock inventory and goods issues/received</p></div>
            <i class="fa fa-long-arrow-right"></i>
    </div></a>
</div>
<div class="col-xs-12 col-sm-6 col-md-3">
    <a href="http://localhost/netrootstech.com/erp/erp_accounts/">
        <div class="moduledata">
            <div class="imgholder4"></div>
            <div class="moduleinfo"><h4>Accounts</h4>
                <p>Automate accounting and financial tasks</p></div>
            <i class="fa fa-long-arrow-right"></i>
   </div> </a>
</div>                                    </div>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-3">
        <a href="http://localhost/netrootstech.com/erp/erp_production/">
            <div class="moduledata">
                <div class="imgholder5"></div>
                <div class="moduleinfo"><h4>Production</h4>
                    <p>Track and process the production of finished goods</p></div>
                <i class="fa fa-long-arrow-right"></i>
        </div></a>
</div>
<div class="col-xs-12 col-sm-6 col-md-3 wow headShake" data-wow-delay="1.7s">
    <a href="https://localhost/netrootstech.com/erp/erp_hrm/">
        <div class="moduledata">
            <div class="imgholder6"></div>
            <div class="moduleinfo"><h4>HRM</h4>
                <p>Organize and consolidate your HR processes <br> <span style="visibility: hidden"> </span></p></div>
            <i class="fa fa-long-arrow-right"></i>
    </div></a>
</div>
<div class="col-xs-12 col-sm-6 col-md-3">
	<a href="http://localhost/netrootstech.com/erp/erp_crm/">
        <div class="moduledata">
            <div class="imgholder7"></div>
            <div class="moduleinfo"><h4>CRM</h4>
                <p>Record, manage, and analyze interactions with customers <span style="visibility: hidden"> <br></span>
                </p></div>
            <i class="fa fa-long-arrow-right"></i>
    </div></a>
</div>
<div class="col-xs-12 col-sm-6 col-md-3 wow shake" data-wow-delay="2.1s"><a
            href="http://localhost/netrootstech.com/erp_supplychain/">
        <div class="moduledata">
            <div class="imgholder8"></div>
            <div class="moduleinfo"><h4>Supply Chain</h4>
                <p>Manage the flow of goods and services</p></div>
            <i class="fa fa-long-arrow-right"></i>
    </div></a>
</div>                                    
</div>                                                      
</div>                  
</div>

<!--   modules end  -->                                 
<!--   clients start  -->
    <div id="clinet" class="clients">
        <div class="container">
            <div class="clientheading">
                <h1 class="wow fadeInDown">Happy Clients</h1>
                <p class="wow fadeInUp" data-wow-delay="0.6s">We work smart to understand and fulfill the business needs
                    of
                    our clients, and it shows through our work. You don’t have to take our word for it – check out what
                    our
                    clients say </p>
            </div>
            <div class="partners">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 wow fadeInRight" data-wow-delay="1.3s">
                        <div class="owl-carousel owl-theme" style="z-index: -1">
                            <div class="item">
                                <div class="slider">
                                    <div class="clientimg"><img src="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/img/client.png" alt="#"></div>
                                    <div class="client_info"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                                class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                                class="fa fa-star"></i>
                                        <h5>TUV Austria</h5>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="clientdetails">
                                        <p>We acquired a HR system from NetRoots, and have found it
                                            to exceed expectations. NetRoots was able to customize our software down to
                                            a T,
                                            creating for us a solution that addressed every one of our HR needs for an
                                            affordable price. Perhaps the best thing about the company was that their
                                            team
                                            worked closely with us to figure out what we need and then made us exactly
                                            that.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider">
                                    <div class="clientimg">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/img/client1.png" alt="#"></div>
                                    <div class="client_info"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                                class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                                class="fa fa-star"></i>
                                        <h5>Office Automation Group</h5>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="clientdetails">
                                        <p>NetRoots Technologies created an ERP system including HRMS
                                            and CRM for us. They automated all of our main business processes, and
                                            really
                                            simplified our work. Throughout the process, they remained in contact with
                                            us
                                            and took our suggestions and requirements into consideration. What we love
                                            about
                                            this ERP is that anyone can use it – young or old, tech-savvy or not! We are
                                            happy that we can focus on growing our business now that we don’t have to
                                            worry
                                            about doing everything manually.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="slider">
                                    <div class="clientimg"><img src="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/img/client2.png" alt="#"></div>
                                    <div class="client_info"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                                class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                                class="fa fa-star"></i>
                                        <h5>Sufi Flour Mills</h5>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="clientdetails">
                                        <p>Flour mills in Pakistan mostly operate in the traditional
                                            way, using manual records. We are glad that we made the switch. It was
                                            becoming
                                            difficult to keep record of and manually manage production and tracking and
                                            our
                                            ERP solved those problems. Our ERP also includes HR functions such as
                                            payroll
                                            and employee asset management. This makes keeping account of our finances
                                            much
                                            easier!</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 wow fadeInLeft" data-wow-delay="1.3s">
                        <div class="clientholder"><img src="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/img/logo-client.jpg" alt="#"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--   clients end  -->

    <script src="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/js/owl.carousel.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/js/jqueryowl.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/erp-assets/WOW-master/dist/wow.min.js"></script>


    <script> new WOW().init();  </script>

</body>
</html>


<?php
get_footer();
