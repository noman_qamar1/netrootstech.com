<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_template_part( 'template-parts/header/header', 'hrm' ); ?>

<!-- ------------------------------------------------template body start from here -->


<div class="top-banner">  
       <div class="container">     
              <div class="banner-content">      
                        <h1>                  
                          Performance Evaluation </h1>       
                                   <p>Are you deciding whether someone is due for a raise?</p>
                                     </div>
                                 </div>
                             </div>
                              <!--banner ends-->    <!--module-area start--> 
                              <div class="module-area"> 
                              	<div class="container"> 
                              	  <h2>Are you deciding whether someone is due for a raise? Is it time to hand out promotions? Who should be given the title of ‘Employee of the Month’?</h2>
                              	  <p>  By tracking employee performance, our performance management software simplifies these decisions for you. Our performance management solutions permit the management to design appraisal forms that include employee competencies, objectives, Key Performance Indicators (KPIs), and comments, as well as self-appraisal forms.  </p>
                              	  <div class="module-chart"> 
                              	  	<div class="row">
                              	  		<div class="col-md-4"> 
                              	  			<div class="branches-performance" data-aos="flip-up">

                              	  			 <a href="#"> 
                              	  			 	<h4>
                              	  			 	Key Performance Indicators 
                              	  			 </h4> 
                              	  			 <p>
                              	  			 Employee key performance objectives are recorded with the name of employee, the objectives, the target year, and so on so that management can evaluate, during the performance appraisal, whether the employee has met his/her objectives.</p>
                              	  		</a> 
                              	  	</div> 
                              	  	<div class="branches-performance" data-aos="flip-up">
                              	  	 <a href="#"> 
                              	  	 	<h4>Objectives  </h4>
                              	  	 	 <p>This sub-module also allows the management to set objectives and KPIs for the following year. You can set objectives for both the company itself and individual employees.</p>
                              	  	 	</a>
                              	  	 </div>
                              	  	</div>
                              	  	<div class="col-md-4">
       <figure class="module-tablet"  data-aos="zoom-in">
       	<a href="#">
       		<img src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/images/performance-tab.png" alt="dashboard" width="100%">
       	</a>
       	</figure>
       	 </div>
       	 <div class="col-md-4">
       	 	<div class="branches-right-performance" data-aos="flip-up"> 

      <a href="#">
      	<h4>Performance appraisals</h4> 
      	 <p>You can add appraisals along with information including the name of the appraisal, the review date, the name of employee who is to be appraised, the current salary of the employee under review, and the date and amount of the last increment.</p>
      	 </a>
      	</div> 
      	 </div> 
      	</div> 
      </div>
       </div>
   </div>
    <!--module-area ends-->    <!--content-wraper start--> 
    <div class="content-wraper">
    	<div class="container">
    	<div class="row">
    		<div class="col-md-6">
    		 <div class="wraper-text"data-aos="fade-right" 
    		  data-aos-offset="300"
    		  data-aos-easing="ease-in-sine"> 
    		   <h2> 
    		    Efficient and Integrated 
    		     </h2>  
    		      <p>  
    		       By digitizing many tasks involved in performance evaluation, Fórsa provides effective, efficient, and practical performance management solutions. In addition, the Performance Management module of Fórsa HR software communicates with the payroll software to ensure that any increments granted during performance appraisals are updated in the payroll. 
    		        </div> 
    		    </div>
    		         <div class="col-md-6"> 
    		         	<figure class="wraper-image" data-aos="zoom-in-up">
    		         		<a href="#">
    		         			<img src="<?php echo get_template_directory_uri(); ?>/assets/hrm-assets/images/performance-laptop.png" alt="dashboard" width="100%">
    		         		</a>
    		         	</figure> 
    		         </div>
    		     </div> 
    		 </div>    
    		</div>    
    		<!--content-wraper end-->    <!--text area-->    
    		<div class="text-area" data-aos="zoom-in-down">        
    			<div class="container">            
    				<h2>Why use Fórsa’s Performance Management Software?</h2>            
    				<p>A company is only as good as the people who run it. Therefore, keeping track of employees’ performance is crucial in ensuring the progress and success of your business. Our performance management software helps you decide which employees are up for an evaluation, which ones deserve a promotion or performance and incentive, and which ones are not adding any value to your business. Thus, our cloud-based HR software allows you to make smart decisions that keep your workforce in the best shape possible, and encourage good performance.</p>
    				 </div> 
    				</div> 

<!-- ----------------------------------------------template end here -->

<script>
	AOS.init();
	</script>
<?php
get_footer();