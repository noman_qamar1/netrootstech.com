<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */


get_template_part( 'template-parts/header/header', 'hrm' ); ?>
<!-- ------------------------------------------------template body start from here -->

<div class="top-banner">
        <div class="container">
            <div class="banner-content">
                <h1>
                    Careers 
                </h1>
                <p>Explore opportunities to take your career to the next level. <br> Whether you are a student, graduate or an experienced<br> professional, discover the impact you could make with a<br> career at ForsaHR.


                </p>
            </div>




        </div>



    </div>

    <!--banner ends-->


    

    <!--text area-->

    <div style="padding:25px 0; color: black;" class="" data-aos="zoom-in-down">

        <div class="container">

            <h2 class="text-left">
                Careers in Pakistan
            </h2>

           <?php
$response = wp_remote_get( 'http://demo.forsahr.com/api/fetchJobPostPakAjax' );
//print $response ['body'];

$jsondata = '';
$arr = json_decode($response['body'], true);

if($arr['message'] == 'No Job Posting Available Yet.'){
    echo "<P>".$arr['message']."</P>";
}
else{
    foreach ($arr as $k=>$v){
        ?>


        <p><a target="_blank" href="<?php echo $v['route'] ?>" ><?php echo  $v['jobTitle'] ?></a>
            <?php echo $v['city'].",".$v['country'].",".$v['organization'].",".$v['positionDate']; ?>
        </p>

        <?php
    }
}

?>


        </div>


    </div>


<script>
	AOS.init();
	</script>

<!-- ----------------------------------------------template end here -->

<?php
get_footer();